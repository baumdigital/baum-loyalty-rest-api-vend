//arreglo general de la documentacion
const mode = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
const version = require ('../../package.json').version;
const swagger = {
    swagger: "2.0",
    info: {
        title: 'VendWebhooks-rest-api Services',
        version,
        description: 'RESTful Api | Services for Adobe-rest-api | Powered by NodeJS',
        termsOfService: "http://swagger.io/terms/",
        contact: {
            name: "Baum Digital 2017",
            email: "soporte@baumdigital.com",
            url: "https://soporte.baumdigital.com"
        },
        license: {
            name: "Apache 2.0",
            url: "http://www.apache.org/licenses/LICENSE-2.0.htm"
        }
    },
    host: process.env.NODE_URL_DOCS,
    basePath: '/',
    schemes: ['http', 'https'],
    definitions: require('./definitions.json'),
    securityDefinitions: require('./securityDefinitions.json'),
    tags: require('./tags.json'),
    consumes: [
        'application/x-www-form-urlencoded',
        'application/json'
    ],
    produces: [
        'application/json'
    ],
    externalDocs: {
        description: "More information about this RESTful Api",
    }
};
module.exports = swagger;
