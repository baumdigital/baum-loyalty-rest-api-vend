const Sequelize = require('sequelize');
/*
id, id_encuesta, id_usuario, id_sucursal, id_codigo_regalia, json_respuesta, json_encuesta, created_at, updated_at
*/

const init = (sequelize) => {
    const EncuestaRespuesta = sequelize.define('EncuestaRespuesta', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_encuesta: {
            type: Sequelize.INTEGER
        },
        id_usuario: {
            type: Sequelize.INTEGER
        },
        id_sucursal: {
            type: Sequelize.INTEGER
        },
        json_respuesta: {
            type: Sequelize.TEXT
        },
        json_encuesta: {
            type: Sequelize.TEXT
        },
        created_at: {
            type: Sequelize.DATE
        },
        updated_at: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_encuesta_respuesta'
    });

    EncuestaRespuesta.associate = (Encuesta,Usuario) => {
        if (Encuesta)
            EncuestaRespuesta.belongsTo(Encuesta, { as: 'encuesta', foreignKey: 'id_encuesta', targetKey: 'id' });
        if (Usuario)
            EncuestaRespuesta.belongsTo(Usuario, { as: 'usuario', foreignKey: 'id_usuario', targetKey: 'id' });
    };

    EncuestaRespuesta.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return EncuestaRespuesta;

};

module.exports.init = init;