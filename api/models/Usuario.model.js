const Sequelize = require('sequelize');

const init = (sequelize) => {
    const Usuario = sequelize.define('Usuario', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_genero: {
            type: Sequelize.INTEGER,
        },
        sub_cognito: {
            type: Sequelize.STRING
        },
        codigo: {
            type: Sequelize.STRING
        },
        tipo_identificacion: {
            type: Sequelize.INTEGER,
            defaultValue: 1 // 1 =>  nacional, 2 =>  extranjero
        },
        identificacion: {
            type: Sequelize.STRING
        },
        nombre: {
            type: Sequelize.STRING
        },
        apellidos: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING,
            validate: { isEmail: true }
        },
        telefono: {
            type: Sequelize.STRING
        },
        codigo_region:{
            type: Sequelize.STRING,
            defaultValue: 'CR'
        },
        password: {
            type: Sequelize.STRING
        },
        fecha_nacimiento: {
            type: Sequelize.DATE
        },
        imagen_perfil: {
            type: Sequelize.STRING
        },
        acepta_notificaciones: {
            type: Sequelize.BOOLEAN
        },
        estado: {
            type: Sequelize.INTEGER
        },
        fb_user: {
            type: Sequelize.BOOLEAN
        },
        fb_id: {
            type: Sequelize.STRING
        },
        id_idioma: {
            type: Sequelize.STRING
        },
        puntos: {
            type: Sequelize.DOUBLE
        },
        id_nivel: {
            type: Sequelize.INTEGER
        },
        puntos_historicos: {
            type: Sequelize.DOUBLE
        },
        puntos_totales: {
            type: Sequelize.DOUBLE
        },
        saldo_premios: {
            type: Sequelize.DOUBLE
        },
        qr_code: {
            type: Sequelize.STRING
        }
    }, {

        // don't add the timestamp attributes (updatedAt, createdAt)
        //timestamps: false,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_usuario'
    });


    Usuario.associate = (Genero, Canton, Premio, TransaccionSaldo, TransaccionPremio, DonacionAutomatica, SolicitudCanjeo, Nivel) => {
        if (SolicitudCanjeo)
            Usuario.hasMany(SolicitudCanjeo, { as: 'solicitudes_usuario', foreignKey: 'id_usuario', targetKey: 'id' });
        if (Genero)
            Usuario.belongsTo(Genero, { as: 'genero', foreignKey: 'id_genero', targetKey: 'id' });
        if (Canton)
                //   Usuario.belongsTo(Canton, { as: 'canton', foreignKey: 'id_canton', targetKey: 'id' });
        if (Premio)
            Usuario.belongsToMany(Premio, { as: 'usuarioPremios ', through: 'pl_premio_usuario', foreignKey: 'id_usuario ', targetKey: 'id' });
        if (TransaccionSaldo)
            Usuario.hasMany(TransaccionSaldo, { as: 'TransaccionSaldo', foreignKey: 'id_usuario', targetKey: 'id' });
        if (TransaccionPremio)
            Usuario.hasMany(TransaccionPremio, { as: 'TransaccionPremio', foreignKey: 'id_usuario', targetKey: 'id' });
        if (DonacionAutomatica) {
            Usuario.hasMany(DonacionAutomatica, { as: 'donacion_automatica', foreignKey: 'id_usuario', targetKey: 'id' });
        }
        if (Nivel) {
            Usuario.belongsTo(Nivel, { as: 'NivelUsuario', foreignKey: 'id_nivel', targetKey: 'id' });
        }
    };
    
    Usuario.associatePremio = (Premio) => {
        Usuario.belongsToMany(Premio, { as: 'usuarioPremios ', through: 'pl_premio_usuario', foreignKey: 'id_usuario ', targetKey: 'id' });
        Premio.belongsToMany(Usuario, { as: 'premioUsuarios', through: 'pl_premio_usuario', foreignKey: 'id_premio', targetKey: 'id' });
    };

    Usuario.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return Usuario;

}

module.exports.init = init;