const Sequelize = require('sequelize');
/*
id, 
id_factura, 
sku_producto, 
nombre_producto, 
precio_producto, 
cantidad_producto

*/

const init = (sequelize) => {
    const LineaFactura = sequelize.define('LineaFactura', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_factura: {
            type: Sequelize.INTEGER
        },
        sku_producto: {
            type: Sequelize.STRING
        },
        nombre_producto: {
            type: Sequelize.STRING
        },
        precio_producto: {
            type: Sequelize.FLOAT
        },
        cantidad_producto: {
            type: Sequelize.FLOAT
        },
        impuestos_producto:{
             type: Sequelize.FLOAT   
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_linea_factura'
    });
    LineaFactura.associate = (Factura) => {
        if (Factura)
            LineaFactura.belongsTo(Factura, { as: 'factura', foreignKey: 'id_factura', targetKey: 'id' });
    };
    LineaFactura.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return LineaFactura;
};

module.exports.init = init;