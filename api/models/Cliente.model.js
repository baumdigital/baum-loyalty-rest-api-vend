const Sequelize = require('sequelize');
const sequelizeMaster = require('../core/Sequelize.js').master;
/*

.master
*/
const Cliente = sequelizeMaster.define('Cliente', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    api_key:{
         type: Sequelize.STRING
    },
    api_endpoint:{
         type: Sequelize.STRING
    },
    nombre: {
        type: Sequelize.STRING
    },
    nombre_encargado: {
        type: Sequelize.STRING
    },
    apellidos_encargado: {
        type: Sequelize.STRING
    },
    email_encargado: {
        type: Sequelize.STRING,
        validate: { isEmail: true }
    },
    telefono_encargado: {
        type: Sequelize.STRING
    },
    estado: {
        type: Sequelize.INTEGER
    },
    password: {
        type: Sequelize.STRING
    },
    username: {
        type: Sequelize.STRING
    },
    id_plan: {
        type: Sequelize.INTEGER
    },
    id_config_apariencia: {
        type: Sequelize.INTEGER
    },
    msj_estado: {
        type: Sequelize.STRING
    },
    branding: {
        type: Sequelize.INTEGER
    }



}, {
    // don't add the timestamp attributes (updatedAt, createdAt)
    //timestamps: false,

    // don't delete database entries but set the newly added attribute deletedAt
    // to the current DATE (when deletion was done). paranoid will only work if
    // timestamps are enabled
    //paranoid: true,

    // don't use camelcase for automatically added attributes but underscore style
    // so updatedAt will be updated_at
    underscored: true,
    // disable the modification of table names; By default, sequelize will automatically
    // transform all passed model names (first parameter of define) into plural.
    // if you don't want that, set the following
    freezeTableName: true,
    // define the table's name
    tableName: 'cliente'
});

Cliente.query = (query, spread) => {
    if (spread) {
        return sequelize.query(query);
    } else {
        return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    }
};

module.exports = Cliente;