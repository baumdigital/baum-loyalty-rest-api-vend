//id, sku_producto, id_label_nombre_producto, id_label_descripcion_producto, precio_producto, bonus_puntos, limite_uso, limite_uso_x_usuario, vencimiento_codigos, tipo_imagen, imagen, imagen_lq, fecha_inicio, fecha_fin, id_label_terminos_condiciones, estado, created_at, updated_at

const Sequelize = require('sequelize');

const init = (sequelize) => {
    const Cupon = sequelize.define('Cupon', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_label_nombre_producto: {
            type: Sequelize.INTEGER
        },
        id_label_descripcion_producto: {
            type: Sequelize.INTEGER
        },
        id_label_terminos_condiciones: {
            type: Sequelize.INTEGER
        },
        sku_producto: {
            type: Sequelize.STRING
        },
        precio_producto: {
            type: Sequelize.FLOAT
        },
        bonus_puntos: { // bonus
            type: Sequelize.FLOAT
        },
        limite_uso: {
            type: Sequelize.INTEGER
        },
        limite_uso_x_usuario: {
            type: Sequelize.INTEGER
        },
        vencimiento_codigos: {
            type: Sequelize.INTEGER
        },
        tipo_imagen: {
            type: Sequelize.STRING
        },
        imagen: {
            type: Sequelize.STRING
        },
        imagen_lq: {
            type: Sequelize.STRING
        },
        estado: {
            type: Sequelize.INTEGER
        },
        fecha_inicio: {
            type: Sequelize.DATE
        },
        fecha_fin: {
            type: Sequelize.DATE
        },
        created_at: {
            type: Sequelize.DATE
        },
        updated_at: {
            type: Sequelize.DATE
        },
        orden: {
             type: Sequelize.INTEGER
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_cupon'
    });

    Cupon.associate = (DisponiblidadCupon, CodigoCupon) => {
        if (DisponiblidadCupon)
            Cupon.hasMany(DisponiblidadCupon, { as: 'disponilibidad', foreignKey: 'id_cupon', targetKey: 'id' });
        if (CodigoCupon)
            Cupon.hasMany(CodigoCupon, { as: 'codigos', foreignKey: 'id_cupon', targetKey: 'id' });
    };
    Cupon.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return Cupon;
};

module.exports.init = init;