const Sequelize = require('sequelize');
const sequelizeMaster = require('../core/Sequelize.js').master;

/*
token text 
secret varchar(255) 
device_id varchar(255) 
created_at timestamp 
updated_at timestamp
*/

const SysSessionToken = sequelizeMaster.define('SysSessionToken', {
    token: {
        type: Sequelize.TEXT,
        primaryKey: true
    },
    secret: {
        type: Sequelize.STRING,
    },
    device_id: {
        type: Sequelize.STRING,
    },
    created_at: {
        type: Sequelize.DATE,
    },
    updated_at: {
        type: Sequelize.DATE,
    }
}, {
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,

    // don't delete database entries but set the newly added attribute deletedAt
    // to the current DATE (when deletion was done). paranoid will only work if
    // timestamps are enabled
    //paranoid: true,

    // don't use camelcase for automatically added attributes but underscore style
    // so updatedAt will be updated_at
    underscored: true,
    // disable the modification of table names; By default, sequelize will automatically
    // transform all passed model names (first parameter of define) into plural.
    // if you don't want that, set the following
    freezeTableName: true,
    // define the table's name
    tableName: 'sys_session_token'
});

SysSessionToken.query = (query, spread) => {
    if (spread) {
        return sequelize.query(query);
    } else {
        return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    }
};


module.exports = SysSessionToken;