const Sequelize = require('sequelize');
/*
id, INTEGER
id_usuario, INTEGER
monto, FLOAT
saldo_disponible, FLOAT
tipo, INTEGER
id_label_descripcion, INTEGER
id_sucursal, INTEGER
created_at, DATE
*/

const init = (sequelize) => {
    const TransaccionSaldo = sequelize.define('TransaccionSaldo', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        num_factura: {
            allowNull: true,
            defaultValue: null,
            type: Sequelize.STRING
        },
        id_usuario: {
            type: Sequelize.INTEGER
        },
        id_factura: {
            type: Sequelize.INTEGER
        },
        id_admin: {
            type: Sequelize.INTEGER
        },
        monto: {
            type: Sequelize.FLOAT
        },
        monto_real: {
            type: Sequelize.FLOAT
        },
        saldo_disponible: {
            type: Sequelize.FLOAT
        },
        tipo: {
            type: Sequelize.INTEGER
        },
        id_label_descripcion: {
            type: Sequelize.INTEGER
        },
        id_sucursal: {
            type: Sequelize.INTEGER
        },
        created_at: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_transaccion_saldo'
    });
    TransaccionSaldo.associate = (Usuario, Sucursal,Factura) => {
        if (Usuario)
            TransaccionSaldo.belongsTo(Usuario, { as: 'TransaccionSaldoUsuario', foreignKey: 'id_usuario', targetKey: 'id' });
        if (Sucursal)
            TransaccionSaldo.belongsTo(Sucursal, { as: 'sucursal', foreignKey: 'id_sucursal', targetKey: 'id' });
        if(Factura){
            TransaccionSaldo.belongsTo(Factura, { as: 'factura', foreignKey: 'id_factura', targetKey: 'id', onDelete: 'cascade', hooks: true }); //
        }
    };

    TransaccionSaldo.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return TransaccionSaldo;
}



module.exports.init = init;