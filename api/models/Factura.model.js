const Sequelize = require('sequelize');
/*
id, 
id_sucursal, 
numero_factura, 
subtotal, 
impuestos, 
total, 
fecha

*/

const init = (sequelize) => {
    const Factura = sequelize.define('Factura', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_sucursal: {
            type: Sequelize.INTEGER
        },
        numero_factura: {
            type: Sequelize.STRING
        },
        subtotal: {
            type: Sequelize.FLOAT
        },
        impuestos: {
            type: Sequelize.FLOAT
        },
        total: {
            type: Sequelize.FLOAT
        },
        fecha: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_factura'
    });

    Factura.associate = (LineaFactura, Sucursal) => {
        if (LineaFactura)
            Factura.hasMany(LineaFactura, { as: 'lineas', foreignKey: 'id_factura', targetKey: 'id' });
        if (Sucursal)
            Factura.belongsTo(Sucursal, { as: 'sucursal', foreignKey: 'id_sucursal', targetKey: 'id' });
    };

    Factura.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return Factura;

};

module.exports.init = init;