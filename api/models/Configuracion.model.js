const Sequelize = require('sequelize');

const init = (sequelize) => {
    const Configuracion = sequelize.define('Configuracion', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_label_pl_descripcion: { //id_label_descripcion_plan ✓
            type: Sequelize.INTEGER
        },
        pl_simbolo_moneda: { //simbolo_moneda_venta ✓
            type: Sequelize.STRING
        },
        pl_servicio_donaciones: { //servicio_donacion ✓
            type: Sequelize.BOOLEAN
        },
        pl_formato_montos: { //formato_montos ✓
            type: Sequelize.INTEGER
        },
        pl_encuestas_tiempo_delay:{  // minutos
            type: Sequelize.INTEGER
        },
        acum_domicilio_tiempo_vencimiento:{
              type: Sequelize.INTEGER
        },
        acum_nombre_puntos: { //nombre_moneda ✓ * 
            type: Sequelize.STRING
        },
        acum_simbolo_puntos: { //simbolo_moneda ✓ * 
            type: Sequelize.STRING
        },
        acum_piso_redencion: { //piso_redencion ✓ * 
            type: Sequelize.FLOAT
        },
        acum_porc_acumulacion_puntos: { //porc_acumulacion_puntos ✓ * 
            type: Sequelize.FLOAT
        },
        acum_vencimiento: { //vencimiento ✓ * 
            type: Sequelize.BOOLEAN
        },
        acum_tiempo_vencimiento: { //meses_vencimiento ✓ * 
            type: Sequelize.INTEGER
        },
        acum_id_premio: { //id_premio ✓ * 
            type: Sequelize.INTEGER
        },
        acum_factura_requerida: { //factura_requerida ✓ * 
            type: Sequelize.BOOLEAN
        },
        acum_tarjeta_tamano: { //tarjeta_tamano ✓ * 
            type: Sequelize.FLOAT
        },
        acum_puntos_iniciales: { // puntos_inicio ✓ * 
            type: Sequelize.FLOAT
        },
        acum_tarjeta_icono: { //tarjeta_icono ✓ * 
            type: Sequelize.STRING
        },
        invi_servicio_invitaciones: { //servicio_invitaciones ✓ * 
            type: Sequelize.BOOLEAN
        },
        invi_puntos_x_invitar: { //puntos_x_invitar ✓ * 
            type: Sequelize.FLOAT
        },
        invi_puntos_x_aceptar_invitacion: { //puntos_x_aceptar_invitacion ✓ * 
            type: Sequelize.FLOAT
        },
        invi_cant_regalias: { //cant_regalias_invitaciones ✓ * 
            type: Sequelize.INTEGER
        },
        invi_vencimiento: { //vencimiento_invitaciones ✓ * 
            type: Sequelize.BOOLEAN
        },
        invi_tiempo_vencimiento: { //dias_vencimiento_invitaciones ✓ * 
            type: Sequelize.INTEGER
        },
        eval_periodo: { //periodo_evaluacion  ✓ * 
            type: Sequelize.INTEGER
        },
        eval_ultima_fecha: { //fecha_ultima_evaluacion ✓ * 
            type: Sequelize.DATE
        },
        canj_vencimiento_solicitud: { //horas_vencimiento_solicitud_canjeo ✓ * 
            type: Sequelize.INTEGER
        },
        canj_factura_requerida: {
            type: Sequelize.BOOLEAN
        },
        canj_solicitudes:{ // INDICA SI LEALTO TRABAJA CON SOLICITUDES DE CANJEO
             type: Sequelize.BOOLEAN
        },
        canj_solicitudes_tipo:{ // indica el tipo de funcionalidad que tiene
             type: Sequelize.STRING
        },
        conf_cambia_cupones: {
            type: Sequelize.INTEGER
        },
        seg_cant_max_acum_dia: {
            type: Sequelize.INTEGER
        },
        seg_cant_max_acum_mes: {
            type: Sequelize.INTEGER
        },
        seg_cant_max_logins_dia: {
            type: Sequelize.INTEGER
        },
        seg_monto_real_max_acum:{
            type: Sequelize.INTEGER
        }
    }, {
        timestamps: false,
        underscored: true,
        freezeTableName: true,
        tableName: 'pl_configuracion'
    });
    Configuracion.associate = (Premio, Sellos, ReglaBeneficio, CampaniaDonacion) => {
        if (Premio)
            Configuracion.belongsTo(Premio, { as: 'premio', foreignKey: 'acum_id_premio', targetKey: 'id' });
        if (Sellos)
            Configuracion.hasMany(Sellos, { as: 'sellos', foreignKey: 'id_configuracion', targetKey: 'id' });
        if (ReglaBeneficio) {
            Configuracion.hasMany(ReglaBeneficio, { as: 'beneficios', foreignKey: 'id_configuracion', targetKey: 'id' });
            Configuracion.hasMany(ReglaBeneficio, { as: 'reglas', foreignKey: 'id_configuracion', targetKey: 'id' });
        }
        if (CampaniaDonacion)
            Configuracion.hasMany(CampaniaDonacion, { as: 'campaniaDonacion', foreignKey: 'id_configuracion', targetKey: 'id' });
    };
    Configuracion.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, {
                type: sequelize.QueryTypes.SELECT
            });
        }
    };
    return Configuracion;
};


module.exports.init = init;