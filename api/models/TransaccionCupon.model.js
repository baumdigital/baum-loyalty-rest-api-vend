// id, id_codigo_cupon, id_usuario, porcentaje_acumulacion, monto_acumulacion, created_at, updated_at

const Sequelize = require('sequelize');

const init = (sequelize) => {
    const TransaccionCupon = sequelize.define('TransaccionCupon', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_codigo_cupon: {
            type: Sequelize.INTEGER
        },
        id_sucursal:{
              type: Sequelize.INTEGER
        },
        id_usuario: {
            type: Sequelize.INTEGER
        },
        puntos_bonus: {
            type: Sequelize.FLOAT
        },
        monto_acumulacion: {
            type: Sequelize.FLOAT
        },
        created_at: {
            type: Sequelize.DATE
        },
        updated_at: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_transaccion_cupon'
    });

    TransaccionCupon.associate = (Usuario, CodigoCupon) => {
        if (Usuario)
            TransaccionCupon.belongsTo(Usuario, { as: 'usuario', foreignKey: 'id_usuario', targetKey: 'id' });
        if (CodigoCupon)
            TransaccionCupon.belongsTo(CodigoCupon, { as: 'codigoCupon', foreignKey: 'id_codigo_cupon', targetKey: 'id' });
    };
    
    TransaccionCupon.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return TransaccionCupon;
};


module.exports.init = init;