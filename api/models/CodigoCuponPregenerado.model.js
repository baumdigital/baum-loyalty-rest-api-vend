/*

id, 
id_cupon, 
codigo, 
created_at,
updated_at

*/
const Sequelize = require('sequelize');

const init = (sequelize) => {
    const CodigioCuponPregenerado = sequelize.define('CodigioCuponPregenerado', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_cupon: {
            type: Sequelize.INTEGER
        },
        codigo: {
            type: Sequelize.STRING
        },
        created_at: {
            type: Sequelize.DATE
        },
        updated_at: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_codigo_cupon_pregenerado'
    });
    CodigioCuponPregenerado.associate = (Cupon) => {
        if (Cupon)
            CodigioCuponPregenerado.belongsTo(Cupon, { as: 'cupon', foreignKey: 'id_cupon', targetKey: 'id' });
    };
    CodigioCuponPregenerado.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return CodigioCuponPregenerado;
};

module.exports.init = init;