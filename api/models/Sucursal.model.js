const Sequelize = require('sequelize');

/*
    id, int
    id_label_nombre, int
     id_label_direccion, int
     telefono,  string
     id_label_horario, int
    latitud,  string
    longitud, string
    imagen, string
    estado, int
    */

const init = (sequelize) => {
    const Sucursal = sequelize.define('Sucursal', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_label_nombre: {
            type: Sequelize.INTEGER
        },
        id_label_direccion: {
            type: Sequelize.INTEGER
        },
        telefono: {
            type: Sequelize.STRING
        },
        id_label_horario: {
            type: Sequelize.INTEGER
        },
        latitud: {
            type: Sequelize.STRING
        },
        longitud: {
            type: Sequelize.STRING
        },
        imagen: {
            type: Sequelize.STRING
        },
        imagen_lq: {
            type: Sequelize.STRING
        },
        estado: {
            type: Sequelize.INTEGER
        },
        principal: {
            type: Sequelize.INTEGER
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_sucursal'
    });

    Sucursal.associate = (SolicitudCanjeo, Factura) => {
        if (SolicitudCanjeo)
            Sucursal.hasMany(SolicitudCanjeo, { as: 'SolicitudCanjeo', foreignKey: 'id_sucursal', targetKey: 'id' });
        if (Factura)
            Sucursal.hasMany(Factura, { as: 'facturas', foreignKey: 'id_sucursal', targetKey: 'id' });

    };

    Sucursal.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return Sucursal;
};

module.exports.init = init;