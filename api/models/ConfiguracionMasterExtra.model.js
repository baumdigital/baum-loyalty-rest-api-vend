const Sequelize = require('sequelize');
const sequelizeMaster = require('../core/Sequelize.js').master;
/*
id, 
vend_private_token, 
vend_domain_prefix, 
vend_user_id
*/
const ConfiguracionMasterExtra = sequelizeMaster.define('ConfiguracionMasterExtra', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    id_cliente: {
        type: Sequelize.INTEGER,
    }
}, {
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,

    // don't delete database entries but set the newly added attribute deletedAt
    // to the current DATE (when deletion was done). paranoid will only work if
    // timestamps are enabled
    //paranoid: true,

    // don't use camelcase for automatically added attributes but underscore style
    // so updatedAt will be updated_at
    underscored: true,
    // disable the modification of table names; By default, sequelize will automatically
    // transform all passed model names (first parameter of define) into plural.
    // if you don't want that, set the following
    freezeTableName: true,
    // define the table's name
    tableName: 'config_extra'
});

ConfiguracionMasterExtra.query = (query, spread) => {
    if (spread) {
        return sequelize.query(query);
    } else {
        return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    }
};

module.exports = ConfiguracionMasterExtra;