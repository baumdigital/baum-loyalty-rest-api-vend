const Sequelize = require('sequelize');
const init = (sequelize) => {
    const TransaccionEncuesta = sequelize.define('TransaccionEncuesta', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_encuesta_respuesta: {
            type: Sequelize.INTEGER
        },
        id_premio_articulo: { 
            type: Sequelize.INTEGER
        },
        id_transaccion_saldo: {
            type: Sequelize.INTEGER
        },
        id_transaccion_premio: {
            type: Sequelize.INTEGER
        },
        puntos: {
            type: Sequelize.FLOAT
        },
        updated_at: {
            type: Sequelize.DATE
        },
        created_at: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_transaccion_encuesta'
    });
    TransaccionEncuesta.associate = (EncuestaRespuesta, PremioArticulo, TransaccionPremio, TransaccionSaldo) => {
        if(EncuestaRespuesta)
            TransaccionEncuesta.belongsTo(EncuestaRespuesta, { as: 'respuesta', foreignKey: 'id_encuesta_respuesta', targetKey: 'id' });
        if (PremioArticulo)
            TransaccionEncuesta.belongsTo(PremioArticulo, { as: 'premio', foreignKey: 'id_premio_articulo', targetKey: 'id' });
        if (TransaccionPremio)
            TransaccionEncuesta.belongsTo(TransaccionPremio, { as: 'transaccionPremio', foreignKey: 'id_transaccion_premio', targetKey: 'id' });
        if (TransaccionSaldo)
            TransaccionEncuesta.belongsTo(TransaccionSaldo, { as: 'transaccionSaldo', foreignKey: 'id_transaccion_saldo', targetKey: 'id' });
    };

    TransaccionEncuesta.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    
    return TransaccionEncuesta;
}
module.exports.init = init;