// id, id_transaccion_cupon, id_transaccion_premio, id_transaccion_saldo, created_at, updated_at

const Sequelize = require('sequelize');

const init = (sequelize) => {
    const TransaccionCuponTransaccion = sequelize.define('TransaccionCuponTransaccion', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_transaccion_cupon: {
            type: Sequelize.INTEGER
        },
        id_transaccion_premio: {
            type: Sequelize.INTEGER
        },
        id_transaccion_saldo: {
            type: Sequelize.FLOAT
        },
        created_at: {
            type: Sequelize.DATE
        },
        updated_at: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_transaccion_transaccion_cupon'
    });

    TransaccionCuponTransaccion.associate = (TransaccionCupon, TransaccionPremio, TransaccionSaldo) => {
        if (TransaccionCupon)
            TransaccionCuponTransaccion.belongsTo(TransaccionCupon, { as: 'transaccionCupon', foreignKey: 'id_transaccion_cupon', targetKey: 'id' });
        if (TransaccionPremio)
            TransaccionCuponTransaccion.belongsTo(TransaccionPremio, { as: 'transaccionPremio', foreignKey: 'id_transaccion_premio', targetKey: 'id' });
        if (TransaccionSaldo)
            TransaccionCuponTransaccion.belongsTo(TransaccionSaldo, { as: 'transaccionSaldo', foreignKey: 'id_transaccion_saldo', targetKey: 'id' });
    };
    
    TransaccionCuponTransaccion.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return TransaccionCuponTransaccion;
};

module.exports.init = init;