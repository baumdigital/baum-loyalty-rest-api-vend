// id, id_cupon, id_usuario, codigo, estado, vencimiento, created_at, updated_at

const Sequelize = require('sequelize');

const init = (sequelize) => {
    const CodigioCupon = sequelize.define('CodigioCupon', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_cupon: {
            type: Sequelize.INTEGER
        },
        id_usuario: {
            type: Sequelize.INTEGER
        },
        id_sucursal: {
            type: Sequelize.INTEGER
        },
        codigo: {
            type: Sequelize.STRING
        },
        qr_codigo: {
            type: Sequelize.STRING
        },
        estado: {
            type: Sequelize.INTEGER
        },
        vencimiento: {
            type: Sequelize.DATE
        },
        created_at: {
            type: Sequelize.DATE
        },
        updated_at: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_codigo_cupon'
    });
    CodigioCupon.associate = (Usuario, Cupon) => {
        if (Usuario)
            CodigioCupon.belongsTo(Usuario, { as: 'usuario', foreignKey: 'id_usuario', targetKey: 'id' });
        if (Cupon)
            CodigioCupon.belongsTo(Cupon, { as: 'cupon', foreignKey: 'id_cupon', targetKey: 'id' });
    };
    CodigioCupon.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return CodigioCupon;
};

module.exports.init = init;