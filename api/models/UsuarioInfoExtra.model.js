const Sequelize = require('sequelize');

/*
    id,
    id_usuario
    id_vend
    created_at,
    updated_at
    */

const init = (sequelize) => {
    const UsuarioInfoExtra = sequelize.define('UsuarioInfoExtra', {
        id_usuario: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        identificador: {
            type: Sequelize.STRING,
            notNull: true,
            notEmpty: true
        },
        created_at: {
            type: Sequelize.DATE
        },
        updated_at: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_usuario_info_extra'
    });

    UsuarioInfoExtra.associate = (Usuario) => {
        if (Usuario)
            UsuarioInfoExtra.belongsTo(Usuario, { as: 'usuario', foreignKey: 'id_usuario', targetKey: 'id' });
    };

    UsuarioInfoExtra.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return UsuarioInfoExtra;
};

module.exports.init = init;