const Sequelize = require('sequelize');
const sequelizeMaster = require('../core/Sequelize.js').master;
/*

id, id_cliente, usuario, password, nombre, apellidos, id_rol, email, imagen, fecha_registro, estado, id_sucursal, principal
*/


const Administrador = sequelizeMaster.define('Administrador', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    id_cliente: {
        type: Sequelize.STRING
    },
    usuario: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    nombre: {
        type: Sequelize.STRING,
        validate: { isEmail: true }
    },
    apellidos: {
        type: Sequelize.STRING
    },
    id_rol: {
        type: Sequelize.INTEGER
    },
    email: {
        type: Sequelize.STRING
    },
    imagen: {
        type: Sequelize.STRING
    },
    estado: {
        type: Sequelize.INTEGER
    },
    id_sucursal: {
        type: Sequelize.INTEGER
    },
    principal: {
        type: Sequelize.STRING
    },
    acceso_punto_venta: {
        type: Sequelize.BOOLEAN
    }
}, {

    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,

    // don't delete database entries but set the newly added attribute deletedAt
    // to the current DATE (when deletion was done). paranoid will only work if
    // timestamps are enabled
    //paranoid: true,

    // don't use camelcase for automatically added attributes but underscore style
    // so updatedAt will be updated_at
    underscored: true,
    // disable the modification of table names; By default, sequelize will automatically
    // transform all passed model names (first parameter of define) into plural.
    // if you don't want that, set the following
    freezeTableName: true,
    // define the table's name
    tableName: 'administrador'
});

Administrador.query = function(query, spread) {
    if (spread) {
        return sequelize.query(query);
    } else {
        return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    }
}

module.exports = Administrador;