const Sequelize = require('sequelize');
const sequelizeMaster = require('../core/Sequelize.js').master;
/*
id, 
vend_private_token, 
vend_domain_prefix, 
vend_user_id
vend_retailer_id
vend_customer_group_id
vend_producto_regalia_invitacion_sku
vend_producto_regalia_bienvenida_sku
vend_producto_regalia_encuesta_sku
vend_metodo_pago_lealto_id
vend_tienda_lealto_id
vend_cajero_lealto_username
*/
const ConfiguracionVend = sequelizeMaster.define('ConfiguracionVend', {
    id_cliente: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    vend_private_token:{
         type: Sequelize.STRING
    },
    vend_domain_prefix:{
         type: Sequelize.STRING
    },
    vend_user_id:{
         type: Sequelize.STRING
    },
    vend_retailer_id:{
         type: Sequelize.STRING
    },
    vend_customer_group_id:{
        type: Sequelize.STRING
    },
    vend_producto_regalia_invitacion_sku:{
        type: Sequelize.STRING
    },
    vend_producto_regalia_bienvenida_sku:{
        type: Sequelize.STRING
    },
    vend_producto_regalia_encuesta_sku:{
        type: Sequelize.STRING
    },
    vend_metodo_pago_lealto_id:{
        type: Sequelize.STRING
    },
    vend_tienda_lealto_id:{
        type: Sequelize.STRING
    },
    vend_cajero_lealto_username:{
        type: Sequelize.STRING
    }
}, {
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,

    // don't delete database entries but set the newly added attribute deletedAt
    // to the current DATE (when deletion was done). paranoid will only work if
    // timestamps are enabled
    //paranoid: true,

    // don't use camelcase for automatically added attributes but underscore style
    // so updatedAt will be updated_at
    underscored: true,
    // disable the modification of table names; By default, sequelize will automatically
    // transform all passed model names (first parameter of define) into plural.
    // if you don't want that, set the following
    freezeTableName: true,
    // define the table's name
    tableName: 'vend_config'
});

ConfiguracionVend.query = (query, spread) => {
    if (spread) {
        return sequelize.query(query);
    } else {
        return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    }
};

module.exports = ConfiguracionVend;