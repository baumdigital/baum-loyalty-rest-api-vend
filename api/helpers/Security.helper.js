const crypto = require('crypto');
const sequelize = require('sequelize');
const sequelizeMaster = require('../core/Sequelize.js').master;
const jwt = require('jsonwebtoken');

//Config
const Config = require('../../config.js');
const Constants = Config.Constants;
const TIPO_PLAN = Constants.tipoDePlan;
const VEND_CONFIG = Config.Vend;
const CLIENT_SECRET = VEND_CONFIG.vendClientSecret; // VIENE DE ENV
const CLIENT_ID = VEND_CONFIG.vendClientId; // VIENE DE ENV
const REDIRECT_URI = VEND_CONFIG.vendRedirectUri; //// VIENE DE ENV
const ID_CONFIG_WEB_HOOKS_INTERNOS = 1;
//Models
const SysSessionToken = require('../models/SysSessionToken.model.js');
const ConfigWebhookInterno = require('../models/ConfigWebhookInterno.model.js');


const verifyRequestVend = async (req,res,next) => {
	try{
	 	const { headers, body, rawBody } = req;
	 	//console.log({  headers, body  });
	    const signatureHeader = headers['x-signature'];  // EJ: 'signature=f47359cdb49bb9852e47b654b890b27fb313ace0a1eb9e1aa61a966f6023a95e, algorithm=HMAC-SHA256
	    const webHookId = headers['x-vend-webhook-id'];
	    const options = { replacements: { webHookId: webHookId ? webHookId : '' }, type: sequelize.QueryTypes.SELECT }; //id = :webHookId 
	     // buscar el cliente de lealto dueño de ese webhook

	     if(!body || !rawBody || !signatureHeader){
	     	console.log(`VR => 1`)
	     	res.status(403);
        	res.json({ valido: false})
        	return;
	     }
		const arrayStrings1 = signatureHeader.split(','); // -> [ 'signature=f47359cdb49bb9852e47b654b890b27fb313ace0a1eb9e1aa61a966f6023a95e','algorithm=HMAC-SHA256' ]
		if(arrayStrings1.length == 0){
			console.log(`VR => 2`)
        	res.status(403);
        	res.json({ valido: false})
        	return;
		}
		if(arrayStrings1[0].search('signature') == -1){
			console.log(`VR => 3`)
        	res.status(403);
        	res.json({ valido: false})
        	return;
		}
		const finalArray = arrayStrings1[0].split('='); // -> [ 'signature','f47359cdb49bb9852e47b654b890b27fb313ace0a1eb9e1aa61a966f6023a95e' ]
		if(finalArray.length < 2){
			console.log(`VR => 4`)
        	res.status(403);
        	res.json({ valido: false})
        	return;
		}

		const signature = finalArray[1];

        if(!validateSignature(signature,CLIENT_SECRET,rawBody)){
        	res.status(401);
        	res.json({ valido: false})
        	return;
        }

	    const query = 
	    `SELECT cliente.id as idCliente FROM cliente 
	    INNER JOIN config_extra ON config_extra.id_cliente = cliente.id 
	    INNER JOIN webhook_externo ON webhook_externo.id_cliente = cliente.id  AND webhook_externo.id_webhook = :webHookId` 
		const result = await sequelizeMaster.query(query,options).then(r => (r[0])? r[0]: null);

		if(!result){
        	res.status(403);
        	res.json({ valido: false})
        	return;
		}

		const { idCliente } = result;

		if(!idCliente){
        	res.status(403);
        	res.json({ valido: false})
        	return;
		}

        req.idCliente = idCliente;
        return next();
	}catch(error){
		console.log(error);
       	res.status(500);
       	res.json({ valido: false})
       	return;
	}
};
const verifyRequestLealto = async (req,res,next) => {
    const { headers, body, rawBody } = req;
    try{
        const signatureHeader = headers['x-signature'];  // EJ: 'signature=f47359cdb49bb9852e47b654b890b27fb313ace0a1eb9e1aa61a966f6023a95e' // el de lealto no dice el algoritmo
        const webHookId = headers['x-lealto-webhook-id'];

        const configInterna = await ConfigWebhookInterno.findOne({ where: { id: ID_CONFIG_WEB_HOOKS_INTERNOS }});   

        if(!signatureHeader){
            res.status(403);
            res.json({ valido: false, info: { mensaje: 'Invalid request'}})
            return;  
        }

        if(!configInterna){
            res.status(500);
            res.json({ valido: false, info: { mensaje: 'Invalid Lealto webhook configuration'}})
            return;   
        }

        const LEALTO_SECRET = configInterna.secret;

        if(!validateSignature(signatureHeader,LEALTO_SECRET,rawBody)){
            res.status(401);
            res.json({ valido: false, info: { mensaje: 'Invalid request'}})
            return;
        }
    
        next();
    }catch(error){
        console.log(error);
        res.status(500);
        res.json({ valido: false,  info: { mensaje: 'Unexpected error'}})
        return;
    }
};
const verifyLoyaltyPlan = async (req,res,next) => {
        const { idCliente } = req; 
        const query = "SELECT tipo,niveles FROM cliente inner join plan_lealtad on plan_lealtad.id = cliente.id_plan where cliente.id = 2;";           
        const options =  { replacements: [idCliente], type: sequelize.QueryTypes.SELECT };
        const planConfig = await sequelizeMaster.query(query,options).then(r => r[0]? r[0] : null);
        if(!planConfig){
            res.status(500);
            res.json({ valido: false, info: { mensaje: 'Invalid Lealto configuration'}})
            return;   
        }else{
            const { tipo, niveles} = planConfig
            if(niveles){
                res.status(500);
                res.json({ valido: false, info: { mensaje: 'Invalid Lealto configuration for the use this service (has levels)'}})
                return;   
            }
            if(TIPO_PLAN.cashback != tipo){
                res.status(500);
                res.json({ valido: false, info: { mensaje: 'Invalid Lealto configuration for the use this service (not cashback)'}})
                return;
            }
            next();
        }
};
const verifyApiKey = (req,res,next) => {
    const apiKeyRequest = req.headers['x-api-key'];
    const apiKey = process.env.API_KEY;
    if(!apiKey){
        res.status(403);
        res.json({ valido: false, info: { mensaje: 'Missing authentication'}})
        return;    
    }
    if(!apiKeyRequest || !apiKey){
        res.status(403);
        res.json({ valido: false, info: { mensaje: 'Invalid request'}})
        return;         
    }
    if(apiKeyRequest !== apiKey){
        res.status(403);
        res.json({ valido: false, info: { mensaje: 'Invalid request'}})
        return; 
    }
    return next();
};
const saveTokenToDBMaster = (token, secret) => {
    return sequelizeMaster.transaction((transaction_master) => {
        return SysSessionToken.build({ token, secret }).save({ transaction: transaction_master }).then(result => {
            if (result) {
                return token
            } else {
                return '';
            }
        })
    }).catch(err => {
        return null;
    })
};
const deleteTokenDBMaster = (token) => {
	return SysSessionToken.destroy({ where: { token } }).catch(error => console.log(error));
};
const createClienteTokenPOS = (id_empresa, id_admin, id_sucursal) => {
    let secret = id_empresa + '-aplicacion-vend-' + new Date().getTime();
    let token = jwt.sign({
        id_empresa: id_empresa, // esto solo puede cambiar si se cambia en el API DE LEALTO
        id_admin: id_admin,  // esto solo puede cambiar si se cambia en el API DE LEALTO
        id_sucursal,  // esto solo puede cambiar si se cambia en el API DE LEALTO
        tipo: 'aplicacion' // esto solo puede cambiar si se cambia en el API DE LEALTO
    }, secret);
    return saveTokenToDBMaster(token, secret);
};
const createUsuarioToken = (id_usuario, id_empresa, email, name) => {
    let secret = id_usuario + '-aplicacion-vend-usuario' + new Date().getTime();
    let token = jwt.sign({
        id_empresa: id_empresa,  // esto solo puede cambiar si se cambia en el API DE LEALTO
        id: id_usuario,  // esto solo puede cambiar si se cambia en el API DE LEALTO
        email: email,  // esto solo puede cambiar si se cambia en el API DE LEALTO
        name: name,  // esto solo puede cambiar si se cambia en el API DE LEALTO
        tipo: 'aplicacion'  // esto solo puede cambiar si se cambia en el API DE LEALTO
    }, secret);
    return saveTokenToDBMaster(token, secret);
};
const validateSignature = (signature,clientSecret,body) => {
        const hmac = crypto.createHmac('sha256', clientSecret);
        hmac.update(body);
        const hash = hmac.digest('hex');
        //console.log({ signature, hash })
        return signature !== hash ? false : true;
};




module.exports = {
	verifyRequestVend, //Devuelve el id del cliente en el req.idCliente
    verifyRequestLealto, 
    verifyLoyaltyPlan,
    verifyApiKey,
	validateSignature,
	createClienteTokenPOS,
	createUsuarioToken,
	deleteTokenDBMaster
} 
