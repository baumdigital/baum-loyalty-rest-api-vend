const sequelize = require('sequelize');
const sequelizeInit = require('../core/Sequelize.js').init;
const sequelizeMaster = require('../core/Sequelize.js').master;
const Op = sequelize.Op;

const Cliente = require('../models/Cliente.model.js');
const ConfiguracionMasterExtra = require('../models/ConfiguracionMasterExtra.model.js');
const ConfiguracionVend = require('../models/ConfiguracionVend.model.js');

const getConfig = (allowIdInBody = false) => async (req,res,next) => {
	try{
		let { idCliente, method } = req;
		if(!idCliente && !allowIdInBody){
	       	res.status(403);
	       	res.json({ valido: false})
	       	return;
		}
		if(!idCliente){
			idCliente = (method == 'POST' || method == 'PUT' )? req.body.id_cliente_lealto : req.query.id_cliente_lealto;
			req.idCliente = idCliente;
		}

		const query = 
				"select config_db.nombre, config_db.usuario,config_db.password, config_db.host_escritura, config_db.host_lectura, config_db.puerto " +
	            " from cliente  as cliente left join config_db on cliente.id_config_db = config_db.id where cliente.id = ? ";

		const options =  { replacements: [idCliente], type: sequelize.QueryTypes.SELECT };
		const dbConfig = await sequelizeMaster.query(query,options).then(r => r[0]? r[0] : null);

		if(!dbConfig){
	       	res.status(404);
	       	res.json({ valido: false})
	       	return;
		}

		req.sequelizeInstance = sequelizeInit(dbConfig);
		next();
	}catch(error){
		console.log(error);
       	res.status(500);
       	res.json({ valido: false})
       	return;
	}
};
const getVendConfig = async (req,res,next) => {
	try{
		const { idCliente } = req;
		const clienteLealto = await Cliente.findOne({ where: { id: idCliente, estado: 1 } });
		if(!clienteLealto){
			res.status(400);
			res.json({ valido: false, error: { mensaje: 'Invalid Lealto client'} });
			return; 
		}

		const configMasterExtra = await ConfiguracionMasterExtra.findOne({
			where:{
				id_cliente : clienteLealto.id
			}
		});

		const configExtra = await ConfiguracionVend.findOne({ 
			where: { 
				id_cliente: clienteLealto.id, 
				vend_domain_prefix:  { 
						[Op.ne] : null 
				}
			}
		});

		if(!configExtra){
			res.status(403);
			res.json({ valido: false, error: { mensaje: 'Invalid Lealto client'} });
			return; 		
		}


		const configVend = {
			idConfigExtra: ( configMasterExtra ? configMasterExtra.id : null),
			domainPrefix: configExtra.vend_domain_prefix,
			privateToken: configExtra.vend_private_token,
			customerGroupId: configExtra.vend_customer_group_id,
			userId: configExtra.vend_user_id,
			retailerId: configExtra.vend_retailer_id,
			productoRegaliaInvitacionSku : configExtra.vend_producto_regalia_invitacion_sku,
			productoRegaliaBienvenidaSku : configExtra.vend_producto_regalia_bienvenida_sku,
			productoRegaliaEncuestaSku : configExtra.vend_producto_regalia_encuesta_sku,
			metodoPagoLealtoId : configExtra.vend_metodo_pago_lealto_id,
			tiendaLealtoId : configExtra.vend_tienda_lealto_id,
			cajeroLealtoUsername : configExtra.vend_cajero_lealto_username,
		};

		req.vendConfig = configVend;
		next();
		return;
	}catch(error){
		console.log(error);
       	res.status(500);
       	res.json({ valido: false})
       	return;
	}
};


module.exports = {
	getConfig,
	getVendConfig
}