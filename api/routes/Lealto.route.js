/* Routes de Notificaciones */

const express = require('express');
const AppRouter = express.Router();
const ctrlVend = require('../controllers/Vend.controller.js');
const SecurityHelper = require('../helpers/Security.helper.js');
const ConfigHelper = require('../helpers/Config.helper.js');
const Config = require('../../config.js');
const Constants = Config.Constants;
const API_SERVICES = Constants.API_SERVICES.lealto;

const ctrlDocs = require('../controllers/Doc.controller.js');


//
/**
 * @swagger
 * /lealto/export-swagger-api:
 *   post:
 *     tags:
 *       - Lealto
 *     summary:  Crea documentacion de swagger para ser exporta a apigateway
 *     description: Crea documentacion de swagger para ser exporta a apigateway
 *     operationId: Crea documentacion de swagger para ser exporta a apigateway
 *     parameters:
 *      - name: backend_endpoint
 *        in: formData
 *        description: url del backend al que va a apuntar en apigateway
 *        type: string
 *        format: string
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post(API_SERVICES.exportSwaggerConfig, [SecurityHelper.verifyApiKey], (req, res) => new ctrlDocs().AppGetApiGatewayDocsImport(req, res));

//
/**
 * @swagger
 * /lealto/customer-update-or-create:
 *   post:
 *     tags:
 *       - Lealto
 *     summary:  Crea o actualiza un customer(cliente) en vend respecto a un usuario de lealto (Webhook)
 *     description: Crea o actualiza un customer(cliente) en vend respecto a un usuario de lealto
 *     operationId: lealto-create-update-customer-vend
 *     parameters:
 *      - name: id_cliente_lealto
 *        in: formData
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *      - name: id_usuario
 *        in: formData
 *        description: id del usuario en lealto
 *        type: string
 *        format: string
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post(API_SERVICES.customerCreateOrUpdate, [SecurityHelper.verifyRequestLealto, ConfigHelper.getConfig(true), ConfigHelper.getVendConfig], ctrlVend.AppVendCustomerCreateOrUpdate);

//
/**
 * @swagger
 * /lealto/sale-update-or-create:
 *   post:
 *     tags:
 *       - Lealto
 *     summary:  Hace un sale en vend otorgando una cantidad de puntos de loyalty definida
 *     description: Hace un sale en vend otorgando una cantidad de puntos de loyalty definida
 *     operationId: lealto-sale-update-customer-vend
 *     parameters:
 *      - name: id_cliente_lealto
 *        in: formData
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *      - name: id_usuario
 *        in: formData
 *        description: id del usuario en lealto
 *        type: string
 *        format: string
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post(API_SERVICES.saleCreateOrUpdate, [SecurityHelper.verifyRequestLealto, ConfigHelper.getConfig(true), ConfigHelper.getVendConfig], ctrlVend.AppVendEarnLoyaltyPoints);
// ,,

module.exports.App = AppRouter;
