/* Routes de Notificaciones */

const express = require('express');
const AppRouter = express.Router();
const ctrlVend = require('../controllers/Vend.controller.js');
const SecurityHelper = require('../helpers/Security.helper.js');
const ConfigHelper = require('../helpers/Config.helper.js');
const Config = require('../../config.js');
const Constants = Config.Constants;
const API_SERVICES = Constants.API_SERVICES.vend;
const R = require('ramda');


const async = require('async');

const isValidNumber = R.both(R.is(Number), R.complement(R.equals(NaN)));

const lengthQueueSaleUdapte =  isValidNumber(process.env.length_queue_sale_update) ? process.env.length_queue_sale_update : 29;
const lengthQueueCustomerUpdate = isValidNumber(process.env.length_queue_customer_update) ?  process.env.length_queue_customer_update : 18;

let qCustomerUpdate = async.queue( async (task) => ctrlVend.AppCustomerUpdate(task.req,task.res), lengthQueueCustomerUpdate);
let qSaleUpdate = async.queue( async (task) => ctrlVend.AppSaleUpdate(task.req,task.res), lengthQueueSaleUdapte);

//routes del app
//
/**
 * @swagger
 * /vend/customer-update:
 *   post:
 *     tags:
 *       - Vend
 *     summary: Handler de un customer-update de vend (Webhook)
 *     description: Handler de un customer-update de vend
 *     operationId: vend-customer-update
 *     parameters: 
 *      - name: name_cliente
 *        in: formData
 *        description: Nombre de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post(API_SERVICES.customerUpdate,[SecurityHelper.verifyRequestVend, ConfigHelper.getConfig(false), SecurityHelper.verifyLoyaltyPlan], (req,res) => qCustomerUpdate.push({ req, res }) );

//routes del app
//
/**
 * @swagger
 * /vend/sale-update:
 *   post:
 *     tags:
 *       - Vend
 *     summary: Handler de un sale-update de vend (Webhook)
 *     description: Handler de un sale-update de vend
 *     operationId: vend-sale-update
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post(API_SERVICES.saleUpdate,[SecurityHelper.verifyRequestVend, ConfigHelper.getConfig(false), SecurityHelper.verifyLoyaltyPlan, ConfigHelper.getVendConfig],  (req,res) => qSaleUpdate.push({ req, res }));



//
/**
 * @swagger
 * /vend/oauth-authorize:
 *   get:
 *     tags:
 *       - Vend
 *     summary: Handler de autorizacion(oauth)para vend 
 *     description: Handler de autorizacion(oauth)para vend
 *     operationId: vend-oauth-authorize
 *     parameters: 
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.get(API_SERVICES.oauthAuthorize, ctrlVend.AppOuthAuthorize);


//
/**
 * @swagger
 * /vend/link-authorize:
 *   get:
 *     tags:
 *       - Vend
 *     summary:  Genera un link para dar autorizacion a la aplicacion de lealto de utilizar un API de vend
 *     description: Genera un link para dar autorizacion a la aplicacion de lealto de utilizar un API de vend
 *     operationId: vend-link-authorize
 *     parameters: 
 *      - name: id_cliente_lealto
 *        in: query
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.get(API_SERVICES.authorizationLink,  SecurityHelper.verifyApiKey, ctrlVend.AppcreateAuthUrl);


//
/**
 * @swagger
 * /vend/webhook:
 *   get:
 *     tags:
 *       - Vend
 *     summary: Lista todos los webhooks registrados
 *     description: Lista todos los webhooks registrados
 *     operationId: vend-list-webhooks
 *     parameters: 
 *      - name: id_cliente_lealto
 *        in: query
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.get(API_SERVICES.listWebhook,  SecurityHelper.verifyApiKey, ConfigHelper.getConfig(true), ConfigHelper.getVendConfig, ctrlVend.AppListarWebhooks );


//
/**
 * @swagger
 * /vend/webhook:
 *   post:
 *     tags:
 *       - Vend
 *     summary: Crea un webhook en vend
 *     description: Crea un webhook en vend
 *     operationId: vend-create-webhook
 *     parameters: 
 *      - name: id_cliente_lealto
 *        in: formData
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *      - name: host
 *        in: formData
 *        description: host(url) del servicio que va a responder al webhook. http://baumprojects.com:8083/vend/sale-update
 *        type: string
 *        format: string
 *      - name: type
 *        in: formData
 *        description: el tipo de webhook que va a hacer sale.update o customer.update
 *        type: string
 *        format: string
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post(API_SERVICES.createWebhook,  SecurityHelper.verifyApiKey, ConfigHelper.getConfig(true), ConfigHelper.getVendConfig, ctrlVend.AppCreateWebhook);

//
/**
 * @swagger
 * /vend/webhook:
 *   put:
 *     tags:
 *       - Vend
 *     summary:  Actualiza un webhook en vend
 *     description: Actualiza un webhook en vend
 *     operationId: vend-update-webhook
 *     parameters: 
 *      - name: id_cliente_lealto
 *        in: formData
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *      - name: id_webhook
 *        in: formData
 *        description: id del webhook
 *        type: string
 *        format: string
 *      - name: host
 *        in: formData
 *        description: host(url) del servicio que va a responder al webhook. Ej. http://baumprojects.com:8083/vend/sale-update
 *        type: string
 *        format: string
 *      - name: type
 *        in: formData
 *        description: el tipo de webhook que va a hacer sale.update o customer.update
 *        type: string
 *        format: string
 *      - name: active
 *        in: formData
 *        description: si esta activo o no el webhook
 *        type: boolean
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.put(API_SERVICES.updateWebhook,  SecurityHelper.verifyApiKey, ConfigHelper.getConfig(true), ConfigHelper.getVendConfig, ctrlVend.AppUpdateVendWebhook);

//
/**
 * @swagger
 * /vend/webhook:
 *   delete:
 *     tags:
 *       - Vend
 *     summary:  Elimina un webhook de vend
 *     description: Elimina un webhook de vend
 *     operationId: vend-delete-webhook
 *     parameters: 
 *      - name: id_cliente_lealto
 *        in: query
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *      - name: id_webhook
 *        in: query
 *        description: id del webhook a eliminar
 *        type: string
 *        format: string
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.delete(API_SERVICES.deleteWebhook, SecurityHelper.verifyApiKey, ConfigHelper.getConfig(true), ConfigHelper.getVendConfig, ctrlVend.AppDeleteVendWebhook);


//
/**
 * @swagger
 * /vend/synchronize-customers:
 *   post:
 *     tags:
 *       - Vend
 *     summary: Sincroniza usuarios de vend con usuarios de lealto
 *     description: Sincroniza usuarios de vend con usuarios de lealto para pre registro. No crea ni acutaliza usuarios en lealto ni viceversa.
 *     operationId: vend-synchronize-customers
 *     parameters: 
 *      - name: id_cliente_lealto
 *        in: formData
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post(API_SERVICES.synchronizeCustomers, SecurityHelper.verifyApiKey, ConfigHelper.getConfig(true), ConfigHelper.getVendConfig, ctrlVend.AppSincronizarUsuarios);



//
/**
 * @swagger
 * /vend/synchronize-customers-state:
 *   post:
 *     tags:
 *       - Vend
 *     summary: Da el estado de una Sincronizacion de usuarios de vend con usuarios de lealto
 *     description: Da el estado de una Sincronizacion de usuarios de vend con usuarios de lealto
 *     operationId: vend-synchronize-customers-state
 *     parameters: 
 *      - name: id_cliente_lealto
 *        in: formData
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post(API_SERVICES.synchronizeCustomersState, SecurityHelper.verifyApiKey, ConfigHelper.getConfig(true), ConfigHelper.getVendConfig, ctrlVend.AppEstatusSincronizacionUsuario);


//
/**
 * @swagger
 * /vend/synchronize-sales:
 *   post:
 *     tags:
 *       - Vend
 *     summary: Sincroniza las ventas de vend con las de lealto(solo para usuarios registrados en lealto)
 *     description:  Sincroniza las ventas de vend con las de lealto(solo para usuarios registrados en lealto)
 *     operationId: vend-synchronize-sales
 *     parameters: 
 *      - name: id_cliente_lealto
 *        in: formData
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *      - name: dateFrom
 *        in: formData
 *        description: apartir de que fecha se quieren sincronizar las ventas
 *        type: string
 *        format: date
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post(API_SERVICES.synchronizeSales, SecurityHelper.verifyApiKey, ConfigHelper.getConfig(true), ConfigHelper.getVendConfig, ctrlVend.AppSincronizarPuntos);



//
/**
 * @swagger
 * /vend/synchronize-sales-state:
 *   post:
 *     tags:
 *       - Vend
 *     summary: Da el estado de una Sincronizacion de ventas
 *     description: Da el estado de una Sincronizacion de ventas
 *     operationId: vend-synchronize-sales-state
 *     parameters: 
 *      - name: id_cliente_lealto
 *        in: formData
 *        description: id de la empresa(cliente de baum)
 *        type: string
 *        format: string
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Manejo exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorMessage'
 */
AppRouter.post(API_SERVICES.synchronizeSalesState, SecurityHelper.verifyApiKey, ConfigHelper.getConfig(true), ConfigHelper.getVendConfig, ctrlVend.AppEstatusSincronizacionPuntosUsuario);

module.exports.App = AppRouter;
