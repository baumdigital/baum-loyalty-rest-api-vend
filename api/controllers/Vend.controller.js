
let TOTAL_ACUM = 0;
//packages
const Joi = require('joi');
const Promise = require('bluebird');
const path = require('path');
const fs = require('fs-extra');
const R = require('ramda');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Request = require('request-promise-native');
const moment = require('moment');
const PhoneNumberUtil = require('google-libphonenumber').PhoneNumberUtil;
const phoneUtil = PhoneNumberUtil.getInstance();
const async = require('async');
const DataLoader = require('dataloader');
const schedule = require('node-schedule');
const sequelizeMaster = require('../core/Sequelize.js').master;
const sequelizeInit = require('../core/Sequelize.js').init;
const sequelize = require('sequelize');

//models
const Cliente = require('../models/Cliente.model.js');
const ConfiguracionMasterExtra = require('../models/ConfiguracionMasterExtra.model.js');
const ConfiguracionVend = require('../models/ConfiguracionVend.model.js');

const UsuariosModel = require('../models/Usuario.model.js');
const UsuariosInfoExtraModel = require('../models/UsuarioInfoExtra.model.js');
const SucursalModel = require('../models/Sucursal.model.js');
const SucursalInfoExtraModel = require('../models/SucursalInfoExtra.model.js');
const AdministradorModel = require('../models/Administrador.model.js');
const AdministradorInfoExtraModel = require('../models/AdministradorInfoExtra.model.js');
const PreRegistroModel = require('../models/PreRegistro.model.js');
const TransaccionSaldoModel = require('../models/TransaccionSaldo.model.js');
const SaldoModel = require('../models/Saldo.model.js');
const ConfiguracionModel = require('../models/Configuracion.model.js');
const SysColaAccionesModel = require('../models/SysColaAcciones.model.js');

const TransaccionCuponModel = require('../models/TransaccionCupon.model.js');
const TransaccionCuponTransaccionModel = require('../models/TransaccionCuponTransaccion.model.js');
const transaccionEncuestaModel = require('../models/TransaccionEncuesta.model.js');
const CodigoCuponModel = require('../models/CodigoCupon.model.js');

const EncuestaRespuestaModel = require('../models/EncuestaRespuesta.model.js');

const ClienteModel = require('../models/Cliente.model.js');
const WebHooksModel = require('../models/WebHooks.model.js');
const ConfiguracionMasterExtraModel = require('../models/ConfiguracionMasterExtra.model.js');
const ConfiguracionVendModel = require('../models/ConfiguracionVend.model.js');
const CrendencialesMasterExtraModel = require('../models/CredencialesMasterExtra.model.js');
const CodigoCuponPregeneradoModel = require('../models/CodigoCuponPregenerado.model.js');

const FacturaModel = require('../models/Factura.model.js');
const LineaFacturaModel = require('../models/LineaFactura.model.js');
//Config
const Config = require('../../config.js');
const ESTADO_ACCIONES = Config.ConfigWebhooks.estadoAccionesCola;
const VEND_CONFIG = Config.Vend;
const CLIENT_SECRET = VEND_CONFIG.vendClientSecret; // VIENE DE ENV
const CLIENT_ID = VEND_CONFIG.vendClientId; // VIENE DE ENV
const REDIRECT_URI = VEND_CONFIG.vendRedirectUri; //// VIENE DE ENV
//testing json
//const JSON_TEST = path.resolve(__dirname + '/../../config','test.json');

//Constants
const Constants = Config.Constants;
const CRONJOBS = Constants.CRONJOBS;
const NODE_URL = Constants.url;
const USER_AGENT = 'baum-lealto-vend'
const VEND_WEBHOOK_TYPES = Constants.VEND_WEBHOOK_TYPES;
const API_SERVICES = Constants.API_SERVICES.vend;
const URL_API_LEALTO = Constants.URL_API_LEALTO;
const LEALTO_SERVICES = Constants.LEALTO_SERVICES;
const PAYMENTS_IDS = Constants.PAYMENTS_IDS;
const PAYMENTS_STATUS = Constants.PAYMENTS_STATUS;
const ESTADO_PRE_REGISTRO = Constants.estadoPreRegistro;
const TIPO_REGALIA = Constants.tipoRegalia;
const TRANSACCIONES_LEALTO = Constants.tipoTransaccion;
const TRANSACCIONES_LEALTO_OBJ = Constants.tipoTransaccionObj;
const PAGE_SIZE_SEARCH_SALES = 100;

const SYNC_CUSTOMERS_QUEUE = 3;  //pasar a variables de entorno
const SYNC_TRANSACTIONS_QUEUE = 4; //pasar a variables de entorno
const SYNC_PENDIENTES_QUEUE = 6; //pasar a variables de entorno

const SYNC_RECEIPT_NUMBER  = (id_cliente,id_usuario) => `SYNC-${id_cliente}-${id_usuario}`;
const SHORT_CODE_ENCUESTA = (idCliente,idEncuesta,idRespuesta,puntos) => `${idCliente}-${idEncuesta}-${idRespuesta}-${puntos}`;
const GET_INFO_SHORT_CODE_ENCUESTA = (shortCode) => {
	const data = shortCode.split('-');
	return {
		idCliente: data[0],
		idEncuesta: data[1],
		idRespuesta: data[2],
		puntos: data[3],
	} 
}


//Helper
const SecurityHelper = require('../helpers/Security.helper.js');
//state

const AppSincronizarUsuariosEstado = {};
const AppSincronizarPuntosUsuarioEstado = {};

const STATE = (state,progress) => ({
	state,
	progress
})

const INPROGRESS = 'in-progress';
const AVAILABLE = 'available'
const DONE = 'done';

const ESTADO_CUPON_CODIGO = {
  disponible: 1,
  utilizado: 2,
}

const TIPO_ALIAS = { // PARA COLA DE ACCIONES
    CANJE_PENDIENTE: 'CANJE-PENDIENTE',
    ACUM_PENDIENTE:'ACUMLACION-PENDIENTE'
}

/*
 * Schemas de validacion
 */
const syncUsuariosPuntos = Joi.object().keys({
	id_cliente_lealto: Joi.number().integer().positive().max(255).required(),
	dateFrom: Joi.date()
});
const syncUsuariosPuntosState = Joi.object().keys({
	id_cliente_lealto: Joi.number().integer().positive().max(255).required()
});
const createLinkAuthSchemaValidator = Joi.object().keys({
  id_cliente_lealto: Joi.number().integer().positive().max(255).required(),
});
const listAllWebhooksSchemaValidator = Joi.object().keys({
  id_cliente_lealto: Joi.number().integer().positive().max(255).required(),
});
const createVendWebhookSchemaValidator = Joi.object().keys({
  id_cliente_lealto: Joi.number().integer().positive().max(255).required(),
  host: Joi.string().required().uri(),
  type: Joi.string().required()
});
const updateVendWebhookSchemaValidator = Joi.object().keys({
  id_cliente_lealto: Joi.number().integer().positive().max(255).required(),
  id_webhook: Joi.string().required().guid(),
  host: Joi.string().required().uri(),
  type: Joi.string().required(),
  active: Joi.boolean().falsy(0).truthy(1).required(),
});
const deleteVendWebhookSchemaValidator = Joi.object().keys({
  id_cliente_lealto: Joi.number().integer().positive().max(255).required(),
  id_webhook: Joi.string().required().guid(),
});
const deleteAllWebhooksSchemaValidator = Joi.object().keys({
  id_cliente_lealto: Joi.number().integer().positive().max(255).required(),
});
const sincronizarUsuariosSchemaValidator = Joi.object().keys({
  id_cliente_lealto: Joi.number().integer().positive().max(255).required(),
});
//Controller
class VendController {
	constructor(req,res){
		try{
			const sequelizeInstance = req.sequelizeInstance;
			if(sequelizeInstance){
				this.sequelizeInstance = this.sequelizeInstance;
				this.sequelizeInstance = sequelizeInstance;
				this.Usuario = UsuariosModel.init(sequelizeInstance);
				this.Sucursal = SucursalModel.init(sequelizeInstance);	
				this.UsuariosInfoExtra = UsuariosInfoExtraModel.init(sequelizeInstance);	
				this.SucursalInfoExtra = SucursalInfoExtraModel.init(sequelizeInstance);	
				this.PreRegistro = PreRegistroModel.init(sequelizeInstance);
				this.TransaccionSaldo = TransaccionSaldoModel.init(sequelizeInstance);
				this.Saldo = SaldoModel.init(sequelizeInstance);
				this.SysColaAcciones = SysColaAccionesModel.init(sequelizeInstance);
				this.Configuracion= ConfiguracionModel.init(sequelizeInstance);

				this.TransaccionEncuesta = transaccionEncuestaModel.init(sequelizeInstance);

				this.TransaccionCupon =  TransaccionCuponModel.init(sequelizeInstance);
				this.TransaccionCuponTransaccion =  TransaccionCuponTransaccionModel.init(sequelizeInstance);
				this.CodigoCupon =  CodigoCuponModel.init(sequelizeInstance);
				this.CodigoCuponPregenerado = CodigoCuponPregeneradoModel.init(sequelizeInstance);
				this.EncuestaRespuesta = EncuestaRespuestaModel.init(sequelizeInstance);

				this.Factura = FacturaModel.init(sequelizeInstance);
				this.LineaFactura = LineaFacturaModel.init(sequelizeInstance);

			}
			this.createDataLoaders = this.createDataLoaders.bind(this);
			this.syncCustomers = this.syncCustomers.bind(this);
			this.syncTransactions = this.syncTransactions.bind(this);

			//Request & response
			this.req = req;
			this.res = res;
			//Joi
			this.optionsJoi = { abortEarly: false, stripUnknown : false } ;
			//master models
			this.Administrador = AdministradorModel;	
			this.AdministradorInfoExtra = AdministradorInfoExtraModel;	
			this.Cliente = ClienteModel;
			this.WebHooks = WebHooksModel;	
			this.ConfiguracionMasterExtra = ConfiguracionMasterExtraModel;
			this.CrendencialesMasterExtra = CrendencialesMasterExtraModel;
			this.ConfiguracionVend = ConfiguracionVendModel;
			//Lealto funcs
			this.generarTokenAuthorizacionLealto = this.generarTokenAuthorizacionLealto.bind(this);
			this.enviarAcumulacion = this.enviarAcumulacion.bind(this);
			this.enviarCanje = this.enviarCanje.bind(this);
			this.enviarUpdateUsuario = this.enviarUpdateUsuario.bind(this);
			//Vend funcs
			this.createVendWebhook = this.createVendWebhook.bind(this);
			this.listVendWebhook = this.listVendWebhook.bind(this);
			this.updateVendWebhook = this.updateVendWebhook.bind(this);
			this.deleteVendWebhook = this.deleteVendWebhook.bind(this);
			this.createCustomer = this.createCustomer.bind(this);
			this.updateCustomer = this.updateCustomer.bind(this);
			this.getVendAccessToken = this.getVendAccessToken.bind(this);
			this.getVendValidAccessToken= this.getVendValidAccessToken.bind(this);
			this.getVendAccessTokenFromRefreshToken = this.getVendAccessTokenFromRefreshToken.bind(this);
			this.getVendConfig = this.getVendConfig.bind(this);
			this.listCustomers = this.listCustomers.bind(this);
			this.getCustomer = this.getCustomer.bind(this);
			this.searchProducts = this.searchProducts.bind(this);
			this.registerSale = this.registerSale.bind(this);
			this.getProduct = this.getProduct.bind(this);
			this.getSale = this.getSale.bind(this);
			this.searchSale = this.searchSale.bind(this);
			this.sincronizarPuntos = this.sincronizarPuntos.bind(this);
			this.reportarAccionPendiente = this.reportarAccionPendiente.bind(this);
			this.registrarTransaccionesCupon = this.registrarTransaccionesCupon.bind(this);
			this.registrarFacturaObj = this.registrarFacturaObj.bind(this);
			this.saldoDisponibleUsuarioLealto = this.saldoDisponibleUsuarioLealto.bind(this);
			this.procesarVenta = this.procesarVenta.bind(this);
			this.realizarAcumulacion = this.realizarAcumulacion.bind(this);
			this.realizarCanje = this.realizarCanje.bind(this);
			//Hanlders Vend
			this.AppSaleUpdate = this.AppSaleUpdate.bind(this);
			this.AppCustomerUpdate = this.AppCustomerUpdate.bind(this);
			this.AppOuthAuthorize = this.AppOuthAuthorize.bind(this);
			this.AppcreateAuthUrl = this.AppcreateAuthUrl.bind(this);
			this.AppListarWebhooks = this.AppListarWebhooks.bind(this);
			this.AppCreateWebhook = this.AppCreateWebhook.bind(this);
			this.AppUpdateVendWebhook = this.AppUpdateVendWebhook.bind(this);
			this.AppDeleteVendWebhook = this.AppDeleteVendWebhook.bind(this);
			this.syncPendientes = this.syncPendientes.bind(this);
			//Hanlders Lealto
			this.AppVendCustomerCreateOrUpdate = this.AppVendCustomerCreateOrUpdate.bind(this);
			//utility

		}catch(error){
			console.log(error);
			res.status(500);
			res.json({ valido: false });
		}
	};

	createDataLoaders({ token, idCliente, domainPrefix, date_from }){
		const { AdministradorInfoExtra, UsuariosInfoExtra, SucursalInfoExtra, Administrador, Sucursal } = this;
		const { searchProducts, getProduct } = this;
		const { searchSale, getSale } = this;
		const getSalesByCustomer = async (cutomerIds = []) => {
			console.log(`[getSalesByCustomer]`)
			return Promise.reduce(cutomerIds, async (r,customerId) => {
				const saletResult = await searchSale(domainPrefix,customerId, 0, date_from, token);
				return R.prop('data',saletResult) ? r.concat(saletResult.data) : r.concat(null);
			},[]);
		};
		const getSales = async (ids = []) => {
			console.log(`[getSales]`)
			return Promise.reduce(ids, async (r,id) => {
				const saletResult = await getSale(domainPrefix, id, token);
				return R.prop('data',saletResult) ? r.concat(saletResult.data) : r.concat(null);
			},[]);
		};
		const getProductsBySku = async (skus = []) => {
			console.log(`[getProductsBySku]`)
			return Promise.reduce(skus, async (r,sku) => {
				const productsResult = await searchProducts(domainPrefix, sku, token);
				return R.prop('data',productsResult) ? r.concat(productsResult.data) : r.concat(null);
			},[]);		
		};
		const getProducts = async (ids = []) => {
			console.log(`[getProducts]`)
			return Promise.reduce(ids, async (r,id) => {
				const productResult = await getProduct(domainPrefix, id, token);
				return R.prop('data',productResult) ? r.concat(productResult.data) : r.concat(null);
			},[]);
		};
		const getUsersLealto = async (customerIds) => {
			console.log(`[getUsersLealto]`)
			return Promise.reduce(customerIds, async(r,id) => {
				const data = await UsuariosInfoExtra.findOne({ where: { identificador: id } });
			 return data ? r.concat(data.toJSON()) : r.concat(null);
			},[])
		};
		const getAdminsLealto = async (adminsId) => {
			 console.log(`[getAdminsLealto]`)
			 return Promise.reduce(adminsId, async (r,id) => {
				 const data = await AdministradorInfoExtra.findOne({ where: { identificador: id } });
				return data ? r.concat(data.toJSON()) : r.concat(await getAdminLealto());
			},[])
		};
		const getRetailsLealto = async (retailsId = []) => {
			console.log(`[getRetailsLealto]`)
			return Promise.reduce(retailsId, async (r,id) => {
				const data = await SucursalInfoExtra.findOne({ where: { identificador: id } });
			 return data ? r.concat(data.toJSON()) : r.concat(await getSucursalLealto());
		 },[])
		};
		const getSucursalLealto = async (ids = []) => {
			console.log(`[getSucursalLealto]`)
			let sucursal = Administrador.findOne({ where: 		{
					principal: 1
				}
			});
			
			if(sucursal){
				sucursal = sucursal.toJSON();
				sucursal.id_sucursal = sucursal.id;
			}
			return sucursal ? [sucursal] : [null];
		};
		const getAdminLealto = async (ids = []) => {
			console.log(`[getAdminLealto]`)
			let admin = Administrador.findOne({ where: {
				id_cliente: idCliente,
				principal: 1,
				baum_admin: 0
			}});
			if(admin){
				admin = admin.toJSON();
				admin.id_admin = admin.id;
			}
			return admin ? [admin] : [null];
		};
		return {
			saleVend:  new DataLoader(ids => getSales(ids),{ cache: true }),
			saleVendByCustomer: new DataLoader(ids => getSalesByCustomer(ids),{ cache: true }),
			productsVend: new DataLoader( ids => getProducts(ids),{ cache: true }),
			productsVendBySku: new DataLoader(skus => getProductsBySku(skus),{ cache: true }),
			adminsLealtoByVendId: new DataLoader(ids => getAdminsLealto(ids),{ cache: true }),
			sucuralesLealtoByVendId: new DataLoader(ids => getRetailsLealto(ids),{ cache: true }),
			usuarioLealtoByVendId: new DataLoader(ids => getUsersLealto(ids),{ cache: true }),
			adminLealto: new DataLoader( _ => getAdminLealto(),{ cache: true }),
			sucursalLealto: new DataLoader( _ => getSucursalLealto(),{ cache: true })
		};
	};
	//Vend config request
	async getVendConfig(domainPrefix, token){
		const url = `https://${domainPrefix}.vendhq.com/api/config`;
		const headers = { Authorization: token , 'user-agent': USER_AGENT }
		const options = {
				method: 'GET', 
				uri: url, 
				resolveWithFullResponse: false, 
				simple: false,
				headers,
				json: true			
		};
		const response = await Request(options);

		//console.log({ response });

		return response;
	};
	//Vend webhooks request senders
	async createVendWebhook(domainPrefix, type, host, token){
		const url = `https://${domainPrefix}.vendhq.com/api/webhooks`;

		const data = {
			url: host,
			active: true,
			type
		}
		const headers = { Authorization: token , 'user-agent': USER_AGENT }

		const form = { data: JSON.stringify(data) };

		const options = {
				method: 'POST', 
				uri: url, 
				resolveWithFullResponse: false, 
				form,
				simple: false,
				headers,
				json: true			
		};

		const response = await Request(options);

		console.log({ response });

		return response;
	};
	async deleteVendWebhook(domainPrefix, idWebhook, token){
		const url = `https://${domainPrefix}.vendhq.com/api/webhooks/${idWebhook}`;

		const headers = { Authorization: token , 'user-agent': USER_AGENT }

		const options = {
				method: 'DELETE', 
				uri: url, 
				resolveWithFullResponse: false, 
				simple: false,
				headers,
				json: true			
		};

		const deleteResponse = await Request(options);

		console.log({ [idWebhook]: deleteResponse });

		return deleteResponse;
	};
	async updateVendWebhook(domainPrefix, idWebhook, type, host, active = true, token){
		const url = `https://${domainPrefix}.vendhq.com/api/webhooks/${idWebhook}`;

		const data = {
			url: host,
			active,
			type
		}
		const headers = { Authorization: token , 'user-agent': USER_AGENT }

		const form = { data: JSON.stringify(data) };

		const options = {
				method: 'PUT', 
				uri: url, 
				resolveWithFullResponse: false, 
				form,
				simple: false,
				headers,
				json: true			
		};

		const response = await Request(options);

		console.log({ response });

		return response;
	};
	async listVendWebhook(domainPrefix, token){
		const url = `https://${domainPrefix}.vendhq.com/api/webhooks`;

		const headers = { Authorization: token , 'user-agent': USER_AGENT }

		const options = {
				method: 'GET', 
				uri: url, 
				resolveWithFullResponse: false, 
				simple: false,
				headers,
				json: true			
		};
		const response = await Request(options);

		console.log({ response });

		return response;
	};
	//Vend customer request senders
	async createCustomer(domainPrefix, usuario, token){
		const url = `https://${domainPrefix}.vendhq.com/api/2.0/customers`

		const headers = { Authorization: token , 'user-agent': USER_AGENT }


		const options = {
				method: 'POST', 
				uri: url, 
				resolveWithFullResponse: false, 
				body: {
					...usuario
				},
				simple: false,
				headers,
				json: true			
		};

		const response = await Request(options);

		console.log({ createCustomer: JSON.stringify(response) });

		return response;
	};
	async updateCustomer(domainPrefix, idCustomer, usuario, token){
		const url = `https://${domainPrefix}.vendhq.com/api/2.0/customers/${idCustomer}`

		const headers = { Authorization: token , 'user-agent': USER_AGENT }

		const options = {
				method: 'PUT', 
				uri: url, 
				resolveWithFullResponse: false, 
				body: {
					...usuario
				},
				simple: false,
				headers,
				json: true			
		};

		const response = await Request(options);

		console.log({ updateCustomer: JSON.stringify(response) });

		return response;
	};
	async listCustomers(domainPrefix, after = 0, before, page_size, deleted = false, token){
		const url = `https://${domainPrefix}.vendhq.com/api/2.0/customers`;

		const headers = { Authorization: token , 'user-agent': USER_AGENT }

		const options = {
				method: 'GET',
				qs:{
					after,
					before,
					page_size,
					deleted
				},
				uri: url, 
				resolveWithFullResponse: false, 
				simple: false,
				headers,
				json: true			
		};
		const response = await Request(options);

		/* 
		Requests limits = 300 x <number of registers> + 50
		The best way to deal with rate limiting is to process your Vend API requests in a queuing system (deferred job)
		This should be repeated until an empty collection is returned. This will mean that all items of the collection have been returned.
		{
		    "data": [
		        {
		            id: "0800273d-7d7f-11e5-e1c0-0aaccee64985",
		            ...
		        },
		        ...
		    ]
		    "version": {
		        "min": 11234566790, // =
		        "max": 173456345676 // es el after para la siguiente consulta
		    }
		}

		*/
		return response;
	};
	async getCustomer(domainPrefix, idCustomer, token){
		const url = `https://${domainPrefix}.vendhq.com/api/2.0/customers/${idCustomer}`;
		const headers = { Authorization: token , 'user-agent': USER_AGENT }

		const options = {
				method: 'GET', 
				uri: url, 
				resolveWithFullResponse: false, 
				simple: false,
				headers,
				json: true			
		};
		const response = await Request(options);

		//console.log({ response });

		return response;
	};
	//Vend Products
	async searchProducts(domainPrefix, sku, token){
		const url = `https://${domainPrefix}.vendhq.com/api/2.0/search`;
		const headers = { Authorization: token , 'user-agent': USER_AGENT }

		const options = {
				method: 'GET', 
				uri: url, 
				qs: {
					type: 'products',
					sku
				},
				resolveWithFullResponse: false, 
				simple: false,
				headers,
				json: true			
		};
		const response = await Request(options);

		console.log({ response });

		return response;
	};
	async getProduct(domainPrefix, idProducto, token){
		const url = `https://${domainPrefix}.vendhq.com/api/2.0/products/${idProducto}`;
		const headers = { Authorization: token , 'user-agent': USER_AGENT }

		const options = {
				method: 'GET', 
				uri: url, 
				resolveWithFullResponse: false, 
				simple: false,
				headers,
				json: true			
		};
		const response = await Request(options);

		//console.log({ response });

		return response;
	};
	//Vend  sale
	async registerSale(domainPrefix, source_id, register_id, customer_id, user_id, sale_date, note, status = 'CLOSED', short_code, invoice_number, register_sale_products, register_sale_payments, token){
		//status =>  SAVED, CLOSED, ONACCOUNT, LAYBY, ONACCOUNT_CLOSED, LAYBY_CLOSED, VOIDED.

		const url = `https://${domainPrefix}.vendhq.com/api/register_sales`
		const headers = { Authorization: token , 'user-agent': USER_AGENT }
		const options = {
				method: 'POST', 
				uri: url, 
				resolveWithFullResponse: false, 
				body: {
					source_id,
					register_id,
					customer_id,
					user_id,
					sale_date,
					note,
					status,
					short_code,
					invoice_number,
					register_sale_products,
					register_sale_payments,
				},
				simple: false,
				headers,
				json: true			
		};

		const response = await Request(options);

		console.log({ registerSale: JSON.stringify(response) });

		return response;
	};
	async getSale(domainPrefix, id_sale, token){
		const url = `https://${domainPrefix}.vendhq.com/api/2.0/sales/${id_sale}`
		const headers = { Authorization: token , 'user-agent': USER_AGENT }
		const options = {
				method: 'GET', 
				uri: url, 
				resolveWithFullResponse: false, 
				simple: false,
				headers,
				json: true			
		};

		const response = await Request(options);

		console.log({ getSale: JSON.stringify(response) });

		return response;
	};
	async searchSale(domainPrefix,customer_id, offset = 0, date_from, token){
		const url = `https://${domainPrefix}.vendhq.com/api/2.0/search`;
		const headers = { Authorization: token , 'user-agent': USER_AGENT }

		const qs = {
			type: 'sales',
			customer_id,
			page_size: PAGE_SIZE_SEARCH_SALES,
			offset,
			order_by: 'sale_date',
			order_direction: 'ASC',
		}

		if(!R.isNil(date_from) && !R.isEmpty(date_from)){
			qs.date_from = date_from;
		}

		const options = {
				method: 'GET', 
				uri: url, 
				qs,
				resolveWithFullResponse: false, 
				simple: false,
				headers,
				json: true			
		};
		const response = await Request(options);
		return response;
	};
	//Vend tokens
	async getVendAccessToken(domainPrefix, code) {
	  const url = `https://${domainPrefix}.vendhq.com/api/1.0/token`;
	  const formData = {
	    code,
	    client_id: CLIENT_ID,
	    client_secret: CLIENT_SECRET,
	    grant_type: 'authorization_code',
	    redirect_uri: REDIRECT_URI // tiene que ser el redirect que sale el dashboard de desarrollador de vend
	  }

	  const options = {
	    method: 'POST',
	    uri: url,
	    resolveWithFullResponse: false,
	    formData,
	    simple: false,
	    headers: { 'user-agent': USER_AGENT },
	    json: true
	  };

	  const response = await Request(options);
	  return response;
	};
	async getVendAccessTokenFromRefreshToken(domainPrefix, refreshToken) {
		  const url = `https://${domainPrefix}.vendhq.com/api/1.0/token`;
		  const formData = {
		    refresh_token: refreshToken,
		    client_id: CLIENT_ID,
		    client_secret: CLIENT_SECRET,
		    grant_type: 'refresh_token',
		  };
		  const optionsRefresh = {
		    method: 'POST',
		    uri: url,
		    resolveWithFullResponse: false,
		    formData,
		    simple: false,
		    headers: { 'user-agent': USER_AGENT },
		    json: true
		  };
		  return Request(optionsRefresh);
	};
	async getVendValidAccessToken(idCliente,domainPrefix){
			const { CrendencialesMasterExtra, getVendAccessTokenFromRefreshToken, ConfiguracionMasterExtra } = this;
			const configMasterExtra = await ConfiguracionMasterExtra.findOne({ where: { id_cliente: idCliente } });
			if(!configMasterExtra){
				return null;
			}
			const where = { id_config_extra: configMasterExtra.id };
			const credenciales = await CrendencialesMasterExtra.findOne({ where });

			if(!credenciales){
				return null;
			}

			const { token, refresh_token: refreshToken, expire_date: expireDate } = credenciales; 
			const now = moment().subtract(3, 'minute');
			const expired = moment(expireDate);

			if(expired.isSameOrBefore(now) || !token){
				const result = await getVendAccessTokenFromRefreshToken(domainPrefix, refreshToken);
				console.log({ result });
				if(result.error == 'invalid_request'){
					return 'invalid_token_request';
				}
				const expireDate = moment.unix(result.expires);
				const newtoken = `${result.token_type} ${result.access_token}`;
				const updateParams = { token: newtoken, expire_date: expireDate.toDate(), expires_in: result.expires_in }
				if(result.refresh_token){
					updateParams.refresh_token = result.refresh_token
				};
				await CrendencialesMasterExtra.update(updateParams,{ where });
				return newtoken;
			}else{
				return token;
			}
	};
	//Lealto token
	async generarTokenAuthorizacionLealto(idCliente, idAdmin = null, idSucursal = 1){
		if(!idAdmin){
			return '';
		}		
		const token = await SecurityHelper.createClienteTokenPOS(idCliente, idAdmin, idSucursal);
		return token;
	};
	//Lealto requests senders
	async enviarAcumulacion(idCliente, idAdmin, idUsuario, idSucursal = null, montoVenta, puntosLoyalty, idSale, facturaObj){
		const { generarTokenAuthorizacionLealto, reportarAccionPendiente } = this;
		const url = URL_API_LEALTO + LEALTO_SERVICES.acumular.url; 
		const token = await generarTokenAuthorizacionLealto(idCliente, idAdmin, idSucursal);
		const body = {
			id_usuario: idUsuario,
			id_sucursal: idSucursal? idSucursal : 1,
			monto_real: montoVenta,
			puntos: puntosLoyalty,
			numero_factura: idSale,
			factura_obj: facturaObj
		}
		const headers = {
			Authorization: token,
			'user-agent': USER_AGENT
		};
		const options = {
			method: LEALTO_SERVICES.acumular.method, 
			uri: url, 
			resolveWithFullResponse: false, 
			body,
			json: true,
			headers
		};

		console.log({  enviarAcumulacion : body})
		const response = await Request(options).catch(error => {
			// si la respuesta es diferente a 200 || 201 etc viene aqui
			console.log(JSON.stringify(error))
			return reportarAccionPendiente({ ...body, id_admin: idAdmin, tipo_acumulacion: 'ESPECIFICA' }, TIPO_ALIAS.ACUM_PENDIENTE, 1, error);
		});


		await SecurityHelper.deleteTokenDBMaster(token); // Tokens are meant for single use in this case
		return true;
	};
	async enviarCanje(idCliente, idAdmin, idUsuario, idSucursal, montoVenta, puntosLoyalty, idSale, facturaObj){
		// EN EL API DE LEALTO ESTE ENVIAR CANJE DEBE PERMITIR EL CANJE DE UNA CANTIDAD MAYOR A LA DISPONIBLE
		const { generarTokenAuthorizacionLealto, reportarAccionPendiente } = this;
		const url = URL_API_LEALTO + LEALTO_SERVICES.canjear.url; 
		const token = await generarTokenAuthorizacionLealto(idCliente, idAdmin, idSucursal);

		const body = {
			id_usuario: idUsuario,
			id_sucursal: idSucursal,
			monto_real: montoVenta,
			monto: puntosLoyalty,
			numero_factura: idSale,
			factura_obj: facturaObj
		}
		const headers = {
			Authorization: token,
			'user-agent': USER_AGENT
		};
		const options = {
			method: LEALTO_SERVICES.canjear.method, 
			uri: url, 
			resolveWithFullResponse: false, 
			body,
			json: true,
			headers
		};
		console.log({  enviarCanje : body })
		const response = await Request(options).catch(error => {
			console.log(JSON.stringify(error))
			return reportarAccionPendiente({ ...body, id_admin: idAdmin }, TIPO_ALIAS.CANJE_PENDIENTE, 1, error);
		});


		await SecurityHelper.deleteTokenDBMaster(token); // Tokens are meant for single use in this case
		return true;
	};
	async enviarUpdateUsuario(idCliente, usuario){
		const { generarTokenAuthorizacionLealto } = this;
		const url = URL_API_LEALTO + LEALTO_SERVICES.updateUser.url; 

		const token = await SecurityHelper.createUsuarioToken(usuario.id_usuario, idCliente, usuario.email, usuario.nombre);
		const body = {
			...usuario
		}
		const headers = {
			Authorization: token,
			'user-agent': USER_AGENT
		};
		const options = {
			method: LEALTO_SERVICES.updateUser.method, 
			uri: url, 
			resolveWithFullResponse: true, 
			body,
			json: true,
			headers
		};

		const response = await Request(options).catch(error => console.log(JSON.stringify(error)));
		await SecurityHelper.deleteTokenDBMaster(token); // Tokens are meant for single use in this case
		return true;
	};
	async sincronizarPuntos({ idCliente, idUsuario, vendConfig, idCustomer }){
		const { syncTransactions, PreRegistro, UsuariosInfoExtra } = this;
		console.log(`Starting sync`);
		if(R.isNil(idUsuario) || R.isEmpty(idUsuario)){
			return { valido: false, error: 'No idUsuario' }
		}
		const key = `${idCliente}-sincronizar-puntos-${idUsuario}`;
		if(!AppSincronizarPuntosUsuarioEstado[key]){
			AppSincronizarPuntosUsuarioEstado[key] = STATE(AVAILABLE,0);
		}
		if(AppSincronizarPuntosUsuarioEstado[key].state == INPROGRESS){
			return { valido: true, msj: 'Already in-progress' }
		}
		AppSincronizarPuntosUsuarioEstado[key] = STATE(INPROGRESS,0);
		const infoExtra = await UsuariosInfoExtra.findOne({ where: { identificador: idCustomer }});
		if(!infoExtra){
			await UsuariosInfoExtra.build({ id_usuario: idUsuario, identificador: idCustomer }).save();
		}else{
			const id = R.prop('id_usuario',infoExtra);
			if(!R.isNil(id) && !R.isEmpty(id)){
				if(id != idUsuario){
					await infoExtra.update({ id_usuario: idUsuario });
				}
			}
		}
		const result = await syncTransactions({ key, idCliente, dateFrom: null, vendConfig, idUsuario });
		AppSincronizarPuntosUsuarioEstado[key].state == STATE(AVAILABLE,0);
		await PreRegistro.update({  id_usuario: idUsuario, estado: ESTADO_PRE_REGISTRO.asociado },{ where: { id_externo: idCustomer }});
		return { valido: true, transaccion: result, msj: 'done' };
	};
	//Lealto funcionalidad 
	async registrarTransaccionesCupon(idCliente, domainPrefix, id_sale, id_usuario, id_sucursal = 1, receiptNumber,  saleParams, dataLoaders ){
		const { getVendValidAccessToken, getSale } = this;
		const { TransaccionCupon, TransaccionCuponTransaccion, CodigoCupon, TransaccionSaldo, CodigoCuponPregenerado } = this;
		try{
			const token = await getVendValidAccessToken(idCliente, domainPrefix);
			let sale = null;
			if(!R.isNil(saleParams) && !R.isEmpty(saleParams)){
				sale = saleParams;
			}else{
				sale = (!R.isNil(dataLoaders) && !R.isEmpty(dataLoaders))? await dataLoaders.saleVend.load(id_sale) : await getSale(domainPrefix, id_sale, token);
			} 
			if(!R.has('data')(sale)){
				return false;
			}
			const saleDate = R.path(['data', 'line_items'], sale);
			const lineItems = R.path(['data', 'line_items'], sale);

			if(R.isNil(lineItems) || R.isEmpty(lineItems)){
				return false;
			}

			const transaccionSaldoObj = await TransaccionSaldo.findOne({ where: { num_factura: receiptNumber }});

			const defaultTransaccion = R.defaultTo(null);

			const procesaPromocion = (line) => async (promocion = {}) => {
				try{
					const promoCode = R.prop('promo_code',promocion);
					const amount = R.prop('amount',promocion);
					const name = R.prop('name',promocion);
					const quantity = R.defaultTo(1)(R.prop('quantity',line));

					let codigoCupon = await CodigoCupon.findOne({ where: { codigo: promoCode , id_usuario, estado: ESTADO_CUPON_CODIGO.disponible } });
					if(R.isNil(codigoCupon)){
						let codigoPregenerado = await CodigoCuponPregenerado.findOne({ where: { codigo: promoCode }});
						if(R.isNil(codigoPregenerado)){
							return;
						}
						codigoCupon = await CodigoCupon.build({
							id_cupon: codigoPregenerado.id_cupon,
							codigo: promoCode,
							id_usuario,
							id_sucursal,
							estado: ESTADO_CUPON_CODIGO.utilizado
						}).save();
					}
					const puntos_bonus = amount  * quantity ;
					const params = {
				            id_codigo_cupon: codigoCupon.id,
				            id_usuario,
				            id_sucursal,
				            puntos_bonus: R.lt(puntos_bonus,0)? R.negate(puntos_bonus) : puntos_bonus,
							monto_acumulacion: 0,
							created_at: moment(saleDate).toDate()
				    }
				    const transaccion = await TransaccionCupon.build(params).save();
				    const paramsTransaccionCuponTransaccion = {
				                id_transaccion_cupon: transaccion.id,
				                id_transaccion_saldo: defaultTransaccion(R.prop('id',transaccionSaldoObj)) // esto se hace asi porque siempre es cashback
				    }
				    await TransaccionCuponTransaccion.build(paramsTransaccionCuponTransaccion).save();
				    await CodigoCupon.update({ estado: ESTADO_CUPON_CODIGO.utilizado, id_sucursal },{ where: { id: codigoCupon.id }});
				    return;
				}catch(error){
					console.log(error);
					return;
				}
			};
			const procesarLineItems = (line = {}) => {
					const promotions = R.path(['promotions'],line);
					if(R.isNil(promotions) || R.isEmpty(promotions)){
						return;
					}else{
						return R.forEach(procesaPromocion(line), promotions);
					}
			};
			return R.forEach(procesarLineItems, lineItems);
		}catch(error){
			console.log(error);
			return;
		}
	};
	//Request handlers for Vend webhooks
	async AppSaleUpdate(){
		const { req, res } = this;
		const { getVendConfig, getVendValidAccessToken, getProduct } = this;
		const { UsuariosInfoExtra, SucursalInfoExtra, AdministradorInfoExtra, Administrador, Sucursal, TransaccionSaldo } = this;
		try{
			const { body, idCliente, vendConfig } = req;
			const { metodoPagoLealtoId } = vendConfig;
			const { payload, type, environment, retailer_id: idRetailer,user_id: idAdminVend, domain_prefix: domainPrefix } = body; // info del cliente de vend
			const data = JSON.parse(payload);
			//console.log({ data });
			const { 
				register_id: idVendSucursal,
				sale_date: saleDate,
				register_sale_payments: registerSalePayments, // es un array con los pagos que se hicieron normalmente es solo un tipo de pago, Pero puede cash y loyalty (pago pacial).
				customer, 
				register_sale_products: registerSaleProducts,
				id: idSale,
				invoice_number,
				status,
				totals,
				created_at,
			} = data;	
			const {
			  total_loyalty: totalLoyalty,
			  total_payment: totalPayment,
			  total_price: totalPrice,
			  total_tax: totalTax,
			  total_to_pay: totalToPay,
			} = (totals)? totals : {};
			//console.log({ payload });

			const receiptNumber = idSale;

			if(!customer){
				res.status(404);
				res.json({ valido: false, error: { mensaje: 'Unknown customer'}});
				return;
			}
			const alreadySync = await TransaccionSaldo.findOne({ where: { num_factura: receiptNumber } });
			if(alreadySync){
				console.log(`==alreadySync=====> ${true} ==========>`);
				res.status(200);
				res.json({ valido: true });
				return true;
			}
			const token = await getVendValidAccessToken(idCliente, domainPrefix);

			const { config: configVend} = await getVendConfig(domainPrefix,token);

			const { loyalty_ratio : loyaltyRatio, enable_loyalty: enableLoyalty } = configVend;

			console.log({ loyaltyRatio, enableLoyalty});
			if(!enableLoyalty){
				res.statusMessage = 'Loyalty is not enable, Lealto wont work without it';
				res.status(400);
				res.json({ valido: false, error: { mensaje: 'Loyalty is not enable, Lealto wont work without it'}});
				return;
			}

			const sucursaLealto = await SucursalInfoExtra.findOne({ where: { identificador: idVendSucursal }});
			const usuarioLealto = await UsuariosInfoExtra.findOne({ where: { identificador: customer.id }});
			const adminLealto = await AdministradorInfoExtra.findOne({ where: { identificador: idAdminVend }});

			if(!usuarioLealto){
				res.statusMessage = 'No lealto user associated to this Vend customer';
				res.status(404);
				res.json({ valido: false, error: { mensaje: 'No lealto user associated to this Vend customer'}});
				return;	
			}

			const idSucursal = (sucursaLealto)? sucursaLealto.id_sucursal : await Sucursal.findOne({ where: {
				principal: 1
			}}).then(r => r? r.id : null);
			
			const idAdminLealto = (adminLealto)? adminLealto.id_admin : await Administrador.findOne({ where: {
					id_cliente: idCliente,
					principal: 1,
					baum_admin: 0
				}
			}).then(r => r? r.id : null);


			const { lineasFactura,fallado } = await Promise.reduce( registerSaleProducts, async (re, producto, i) => {
				let { lineasFactura ,objsProductos, fallado } = re;
				if(fallado){
					return re;
				}
				const productoInfo = (objsProductos[producto.product_id])?  objsProductos[producto.product_id]
																	     : await getProduct(domainPrefix, producto.product_id, token).then(r => r? r.data : null).catch(error => { console.log(error); fallado = true; });
				if(!productoInfo){
					fallado = true;
				}

				const linea = {
						sku_producto: productoInfo.sku,
						nombre_producto: productoInfo.name,
						impuestos_producto: producto.tax,
						precio_producto: producto.price,
						cantidad_producto:	producto.quantity	
				}
				lineasFactura = lineasFactura.concat(linea);
				if(!objsProductos[producto.product_id]){
					objsProductos[producto.product_id] = productoInfo;
				}	
				return { lineasFactura, objsProductos,fallado: re.fallado }			
			},{ lineasFactura: [], objsProductos: { }, fallado: false }).catch(error => { console.log(error); return {} });

			const facturaObj =  (lineasFactura && !fallado)? {
				numero_factura: receiptNumber,
				subtotal: totalPrice,
				impuestos: totalTax,
				total: totalPrice,
				fecha: created_at,
				lineas_factura: lineasFactura
			} : null;
			

			const pagoParcialConLoyalty = registerSalePayments.filter((payment) => payment.payment_type_id != PAYMENTS_IDS.loyalty).length >= 1 && registerSalePayments.length >= 2;

			//** CANJE **
			//Vamos realizar el canje de puntos respectivos, no importa el status que tenga el sale, si hay un payment por puntos hay que descontarlo
			// siempres es solo payment de cada tipo, por ejemplo no van a venir 2 payments de loyalty 
			let loyaltyCanjeado = 0;
			await Promise.each(registerSalePayments,(payment,i) => { 
				const { id, amount, payment_type: paymentType } = payment;
				if(paymentType.id == PAYMENTS_IDS.loyalty){
					loyaltyCanjeado += parseFloat(amount);
					// se puede hacer que registre la factura y las lineas de la factura dado que el objeto register_sale_products tiene los productos que fueron canjeados
					return this.enviarCanje(idCliente, idAdminLealto, usuarioLealto.id_usuario , idSucursal, totalPrice, amount, receiptNumber, facturaObj);
				}
				return;
			});

			//funciona con todo menos con compras parciales
			let loyaltyPoints = registerSaleProducts.reduce((result,product,i) =>  (product.loyalty_value)? result + (parseFloat(product.loyalty_value) * parseFloat(product.quantity)) : result,0);


			const porcentajeReduccionPorCompraParcial = ((loyaltyCanjeado * 100) / totalPrice) /100;
			loyaltyPoints = loyaltyPoints - ( loyaltyCanjeado * porcentajeReduccionPorCompraParcial );
			 
			const regalia = registerSalePayments.reduce((r,e) => e.retailer_payment_type_id == metodoPagoLealtoId? r + e.amount : r,0);

			loyaltyPoints = loyaltyPoints - regalia;

			//** ACUMULACION ** 

			// No se hace terminado la compra, no se realiza acumulacion
			// El usuario no tiene loyalty activado
			// El usuario no existe
			if(status != PAYMENTS_STATUS.ONACCOUNT && status != PAYMENTS_STATUS.LAYBY  && customer.enable_loyalty == true && usuarioLealto && loyaltyPoints > 0){
				//se puede hacer que registre la factura y las lineas de la factura dado que el objeto register_sale_products tiene los productos que fueron comprados
				await this.enviarAcumulacion(idCliente, idAdminLealto, usuarioLealto.id_usuario, idSucursal, totalPrice, parseFloat(loyaltyPoints), receiptNumber, facturaObj).catch(er => console.log(er) );
			};
			
			await this.registrarTransaccionesCupon(idCliente, domainPrefix, idSale, usuarioLealto.id_usuario, idSucursal, receiptNumber);

			res.status(200);
			res.json({ valido: true});
			return;
		}catch(error){
			console.log(error);
			res.status(500);
			res.json({ valido: false });
			return;		
		}
	};
	async AppCustomerUpdate(){
		const { req, res } = this; // request y response
		const { Usuario, UsuariosInfoExtra, enviarUpdateUsuario, PreRegistro, sequelizeInstance } = this; // models
		const transaction = await sequelizeInstance.transaction();

		try{
			const { body, idCliente } = req;
			const { payload } = body;
			const data = JSON.parse(payload);

			const { 
				id,
				customer_code,
				first_name,
				last_name,
				do_not_email,
				email,
				phone,
				mobile,
				balance,
				loyalty_balance,
				enable_loyalty,
				sex,
				date_of_birth,
				updated_at,
				created_at
			} = data;

			/*
			cusntom attributes 
				idioma 
				identificacion
				id_lelto

			*/

			//const telefono = (phoneUtil.isValidNumber(mobile))? phoneUtil.format(phoneUtil.parse(mobile), PNF.E164) : null;
			//const codigoRegion =  (phoneUtil.isValidNumber(mobile))? phoneUtil.getRegionCodeForNumber(phoneUtil.parse(mobile)) : null; 

			const usuarioInfoExtraLealto = await UsuariosInfoExtra.findOne({ where: { identificador: id }});

			if(!usuarioInfoExtraLealto){
				//como el usuario no existe lo agregamos a la lista de pre registros o le actualizamos sus datos que tiene en el preregistro
				const params = {
							id_externo: id,
							email: email,
							telefono: mobile,
							codigo: customer_code,
							apellido: last_name,
							nombre: first_name,
							estado: 1
				};
				const aux = await PreRegistro.findOne({ where: { id_externo: id } });
				if(aux){
					await aux.update(params,{ transaction });
				}else{
					await PreRegistro.build(params).save({ transaction });
				}
				await transaction.commit();	
				res.status(200);
				res.json({ valido: false , state: 'pre-registro'});
				return;
			}

			const idUsuario = usuarioInfoExtraLealto.id_usuario;

			const usuario = {
				id_usuario: idUsuario,
				nombre: first_name,
				apellidos: last_name,
				email,
				//identificacion,
				//telefono, 
				//codigo_region: codigoRegion,
			}

			if(sex){
				usuario.id_genero = (sex == 'M')? 1 : 2;// genero del usuario. 1 => masculino, 2 => feminio, 3 => otro
			}
			if(date_of_birth){
				usuario.fecha_nacimiento = (date_of_birth)? moment(date_of_birth).format('YYYY-MM-DD') : null;
			}

			//await enviarUpdateUsuario(idCliente, usuario); // esto provoca que se encicle porque esto manda un request al update de lealto y lealto despues manda un request aqui al AppVendCustomerCreateOrUpdate por haber hecho un update

			await Usuario.update(usuario,{ where: { id: idUsuario }, transaction });
			await transaction.commit();
			res.status(200);
			res.json({ valido: true, state: 'update-exitoso'});
			return;
		}catch(error){
			await transaction.commit();
			console.log(error);
			res.status(500);
			res.json({ valido: false, state: 'fallido' });
			return;	
		};
	};
	//Request handlers for lealto webhooks
	async AppVendCustomerCreateOrUpdate(){
		const { req, res } = this; // request y response
		const { createCustomer, updateCustomer, getVendValidAccessToken, getCustomer, enviarAcumulacion, sincronizarPuntos } = this;
		const { Usuario, UsuariosInfoExtra, PreRegistro, Administrador } = this; // models
		// este mae lo que tiene que hacer es crear el usuario en vend y agregar el id del usuario a la base de datos de lealto.
		try{
			const { body, vendConfig } = req;

			const { payload, id_cliente_lealto: idCliente, type } = body;
			const { domainPrefix, customerGroupId } = vendConfig;

			const { id: idUsuario } = payload;

			if(type == 'user.login' || type == 'user.loginFacebook' || type == 'user.updateNotificaciones'){
				res.status(200);
				res.json({valido: false})
				return;	
			}

			const usuario = await Usuario.findOne({ where: { id: idUsuario } });

			if(!usuario){
				res.statusMessage = 'No user on request';
				res.status(404);
				res.json({valido: false})
				return;
			}

			const token = await getVendValidAccessToken(idCliente, domainPrefix);

			const preRegistroData = await PreRegistro.findOne({ where: { id_usuario: idUsuario, estado: ESTADO_PRE_REGISTRO.pendiente }});
		
			if(preRegistroData){
				const idCustomer = preRegistroData.id_externo;
				const usuarioVendActual =  await getCustomer(domainPrefix,idCustomer,token);
				if(usuarioVendActual){
					await UsuariosInfoExtra.upsert({ id_usuario: idUsuario, identificador: id });
					await sincronizarPuntos({ idCliente, idUsuario, vendConfig, idCustomer });
				}
			}

			const gender = (!usuario.id_genero)? null : ( usuario.id_genero == 1? 'M' : 'F');

			const usuarioObj = {
				first_name: usuario.nombre,
				last_name: usuario.apellidos,
				customer_code: usuario.codigo,
				enable_loyalty: (usuario.estado != 1)? false: true,
				email: usuario.email,
				gender,
				date_of_birth: ( usuario.fecha_nacimiento? moment(usuario.fecha_nacimiento).format('YYYY-MM-DD') : null ),
				do_not_email: false,
				phone: usuario.telefono,
				mobile: usuario.telefono,
				physical_country_id: getCountryCode(usuario.telefono, usuario.codigo_region),
				postal_country_id: getCountryCode(usuario.telefono, usuario.codigo_region),
				custom_field_1: usuario.identificacion,
				custom_field_2: usuario.tipo_identificacion == 1 ? 'nacional' : ( usuario.tipo_identificacion == 2 ? 'extranjero' : null )
			};

			if(!R.isNil(customerGroupId) && !R.isEmpty(customerGroupId)){
				usuarioObj.customer_group_id = customerGroupId;
			}


			let customerId = null;
			const usuarioInfo = await UsuariosInfoExtra.findOne({ where: { id_usuario: idUsuario }}).catch(error => { console.log(error); return null });

			if(usuarioInfo){
				customerId = usuarioInfo.identificador;
			}

			const response =  customerId ?  await updateCustomer(domainPrefix,customerId,usuarioObj,token) : await createCustomer(domainPrefix, usuarioObj, token);

			const { data,errors, } = response;

			if(errors){
				res.statusMessage = 'Request failed';
				res.status(400);
				res.json({ valido: true, error: errors })
				return;
			}

			const { id: idVend } = data;

			if(usuarioInfo){
				await usuarioInfo.update({ identificador: idVend });
			}else{
				await UsuariosInfoExtra.upsert({ id_usuario: idUsuario, identificador: idVend });	
			}

			res.status(200);
			res.json({ valido: true, data })
			return data;

		}catch(error){
			console.log(error);
			res.statusMessage = 'Unexpected error';
			res.status(500);
			res.json({valido: false})
			return;
		}
	};
	async AppVendEarnLoyaltyPoints(){
		const { req, res } = this; // request y response
		const { createCustomer, updateCustomer, getVendValidAccessToken, getCustomer, enviarAcumulacion, registerSale, sincronizarPuntos } = this;
		const { Usuario, UsuariosInfoExtra, PreRegistro, Administrador, TransaccionSaldo, TransaccionEncuesta, EncuestaRespuesta  } = this; // mod
		const { searchProducts } = this;

		try{

			const { body, vendConfig } = req;

			const { payload, id_cliente_lealto: idCliente, type } = body;
			console.log({ body: JSON.stringify(body) });
			const { 
				domainPrefix, 
				customerGroupId, 
				userId, 
				retailerId,
				productoRegaliaInvitacionSku, 
				productoRegaliaBienvenidaSku, 
				productoRegaliaEncuestaSku, 
				metodoPagoLealtoId, 
				tiendaLealtoId, 
				cajeroLealtoUsername 
			} = vendConfig;
			const { usuario, regalia } = payload;

			if(!usuario || !regalia){
				res.statusMessage = 'No user or reward given';
				res.status(400);
				res.json({ valido: false })
				return;		
			}

			const { id: idUsuario } = usuario;

			const { puntos, tipo, numero_transaccion } = regalia;

			if(!puntos || !tipo){
				res.statusMessage = 'No points or type given';
				res.status(400);
				res.json({ valido: false })
				return;	
			}

			const token = await getVendValidAccessToken(idCliente, domainPrefix);
			const preRegistroData = await PreRegistro.findOne({ where: { id_usuario: idUsuario, estado: ESTADO_PRE_REGISTRO.pendiente }});
		
			if(preRegistroData){
				const idCustomer = preRegistroData.id_externo;
				const usuarioVendActual =  await getCustomer(domainPrefix,idCustomer,token);
				if(usuarioVendActual){
					await UsuariosInfoExtra.upsert({ id_usuario: idUsuario, identificador: id });
					await sincronizarPuntos({ idCliente, idCustomer, idUsuario: usuario.id, vendConfig });
				}
			}

			let sku = null;
			switch(tipo){
				case TIPO_REGALIA.bievenida:
						sku = productoRegaliaBienvenidaSku
				break;
				case TIPO_REGALIA.invitacion:
						sku = productoRegaliaInvitacionSku
				break;
				case TIPO_REGALIA.encuesta:
						sku = productoRegaliaEncuestaSku
				break;
				default:
						sku = productoRegaliaBienvenidaSku
				break;
			}

			const producto = await searchProducts(domainPrefix, sku, token);

			if(sku == null || !producto.data[0]){
				res.statusMessage = 'Invalid product sku or no product at all'
				res.status(400);
				res.json({ valido: false, error: 'Invalid product sku or no product at all' })
				return;	
			}

			const usuarioLealto = await UsuariosInfoExtra.findOne({ where: { id_usuario: idUsuario }});

			const customerId = usuarioLealto.identificador;
			const now = moment();

			 const registerSaleProducts = [{
			      "product_id": producto.data[0].id,
			      "quantity": 1,
			      "price": puntos,
                  //"tax": "0.00000",
                  //"tax_id": "0af7b240-abc5-11e9-eddc-19a1098f57ce",
                  //"tax_total": "0.00000"
			      "loyalty_value": puntos
			}];

			const registerSalePayments = [{
			        "register_id": tiendaLealtoId,              
			        "retailer_payment_type_id": metodoPagoLealtoId, 
			        "payment_date": now,                              
			        "amount": puntos  
            }];

            const payloadVend = {
			    "register_id": tiendaLealtoId,
			    "user_id": userId,
			    "status": "CLOSED",
			    "register_sale_products": registerSaleProducts,
			    "register_sale_payments": registerSalePayments
			}

			let shortCode = numero_transaccion;
			if(tipo == TIPO_REGALIA.encuesta){
				const transaccion = await TransaccionEncuesta.findOne({ where: { id_transaccion_saldo: numero_transaccion }});
				if(!R.isNil(transaccion)){
					const encuestaRespuesta = await EncuestaRespuesta.findOne({ where: { id: transaccion.id_encuesta_respuesta }});
					if(encuestaRespuesta){
						shortCode = SHORT_CODE_ENCUESTA(idCliente,encuestaRespuesta.id_encuesta,encuestaRespuesta.id,transaccion.puntos);
					}
				}
			}

			const data = await registerSale(domainPrefix, USER_AGENT, tiendaLealtoId, customerId, userId, now, `Lealto loyalty - ${tipo}`, 'CLOSED', shortCode, null, registerSaleProducts, registerSalePayments, token)

			// se puede hacer que despues del registerSale se agarra el numero_transaccion de lealto y se actualizce el numero_factura en lealto con el invoice de vend

			if(data.hasOwnProperty('register_sale')){
				const { register_sale: registerSale } = data;
				const { id } = registerSale;
				// esto lo hacemos directamente con el TransaccionSaldo xq sabemos que soo cashback funciona con vend, por lo que no tenemos que preguntar si premios o sellos
				await TransaccionSaldo.update({ num_factura: id} , { where: { id: numero_transaccion }}).catch(error => {
					console.log(error);
					return;
				})

			}
			
			res.status(200);
			res.json({ valido: true, data })
			return data;

		}catch(error){
			console.log(error);
			res.statusMessage = 'Unexpected Error';
			res.status(500);
			res.json({valido: false})
			return;		
		}
	};
	//authorization handlers for Vend Oauth
	async AppOuthAuthorize(){
		const { req,res } = this;
		const { Cliente, WebHooks, ConfiguracionMasterExtra, CrendencialesMasterExtra } = this;
		const { createVendWebhook, getVendAccessToken } = this;
		let idCustomerUpdateWebhook = null;
		let idsaleUpdateWebhook = null;
		let domainPrefix = null;
		let tokenVend = null;

		try{
			const { body,query,headers, _parsedUrl } = req;
			const { code, domain_prefix, user_id: userId, state, signature, error } = query;
			domainPrefix = domain_prefix;
			if(error){
				res.status(403);
				res.json({ valido: false, error });
				return;  	
			}
			const stateObj = isJSON(state)? JSON.parse(state) : {};

			const idClienteLealto = stateObj.idLealto;

			if(!idClienteLealto){
				res.status(400);
				res.json({ valido: false, error: { mensaje: 'Invalid state'} });
				return; 
			}

			const clienteLealto = await Cliente.findOne({ where: { id: idClienteLealto, estado: 1 } });

			if(!clienteLealto){
				res.status(400);
				res.json({ valido: false, error: { mensaje: 'Invalid Lealto client'} });
				return; 
			}

		    //if(!SecurityHelper.validateSignature(signature,CLIENT_SECRET,query)){ // hay que averiguar de que esta hecho el signature para asi poder crear bien el hash y hacer match
			//	res.status(400);
			//	res.json({ valido: false });
			//	return;    	
		    //}
		    

			const response = await getVendAccessToken(domainPrefix,code);

			const { token_type, access_token, refresh_token, expires, expires_in } = response;

			if(!access_token || !refresh_token || !token_type){
				res.status(404);
				res.json({ valido: false, error: { mensaje: 'Expired'} });
				return; 
			}

			const token = `${token_type} ${access_token}`;
			tokenVend = token;

			let configMasterExtra = await ConfiguracionMasterExtra.findOne({ where: { id_cliente: clienteLealto.id }});

			const paramsConfigExtra= { id_cliente:  clienteLealto.id };
			if(!configMasterExtra){
				configMasterExtra = await ConfiguracionMasterExtra.build(paramsConfigExtra).save();
			}else{
				await configMasterExtra.update(paramsConfigExtra);
			}

			let configVend = await ConfiguracionVendModel.findOne({ where: { id_cliente: clienteLealto.id }});


			const paramsConfigVend = { vend_domain_prefix: domainPrefix, vend_user_id: userId };
			if(!configVend){
				configVend = await ConfiguracionVendModel.build(paramsConfigVend).save();
			}else{
				await configVend.update(paramsConfigVend);
			}

			const idConfigExtra = configMasterExtra.id;
			const expireDate = moment.unix(expires);

			const paramsCredenciales = {
					id_config_extra: idConfigExtra,
					token,
					refresh_token,
					expires_in,
					expire_date: expireDate.toDate()
			}
			if(await CrendencialesMasterExtra.findOne({ where: { id_config_extra: idConfigExtra}}) ){
				await CrendencialesMasterExtra.update(paramsCredenciales,{ where: { id_config_extra: idConfigExtra }});
			}else{
				await CrendencialesMasterExtra.build(paramsCredenciales).save();	
			}

			const base = `${NODE_URL}/vend`;
			const customerUpdateEndpoint = `${base}${API_SERVICES.customerUpdate}`;
			const saleUpdateEndpoint = `${base}${API_SERVICES.saleUpdate}`
			const customerUpdateWebhook = await createVendWebhook(domainPrefix, VEND_WEBHOOK_TYPES.customerUpdate, customerUpdateEndpoint, token);
			const saleUpdateWebhook = await createVendWebhook(domainPrefix, VEND_WEBHOOK_TYPES.saleUpdate, saleUpdateEndpoint, token);


			idCustomerUpdateWebhook = customerUpdateWebhook.id;
			idsaleUpdateWebhook = saleUpdateWebhook.id;

			const retailerId = (customerUpdateWebhook.retailer_id)? customerUpdateWebhook.retailer_id: null; 
			if(retailerId){
				await configVend.update({  vend_retailer_id: retailerId});
			}



			await WebHooks.build({ 
				id_webhook: customerUpdateWebhook.id,
				id_cliente: idClienteLealto,
				endpoint: customerUpdateEndpoint,
				type: VEND_WEBHOOK_TYPES.customerUpdate
			}).save();

			await WebHooks.build({ 
				id_webhook: saleUpdateWebhook.id,
				id_cliente: idClienteLealto,
				endpoint: saleUpdateEndpoint,
				type: VEND_WEBHOOK_TYPES.saleUpdate
			}).save();



			res.status(200);
			res.json({ valido: true, customerUpdateWebhook, saleUpdateWebhook });
			return;
		}catch(error){
			if(idCustomerUpdateWebhook) deleteVendWebhook(domainPrefix, idCustomerUpdateWebhook, tokenVend);
			if(idsaleUpdateWebhook) deleteVendWebhook(domainPrefix, idsaleUpdateWebhook, tokenVend);
			console.log(error);
			res.status(500);
			res.json({ valido: false});
			return;		
		}
	};
	async AppcreateAuthUrl(){
		const { req, res, optionsJoi } = this;
		try{
			const CLIENT_ID = VEND_CONFIG.vendClientId; // VIENE DE ENV
			const REDIRECT_URI = VEND_CONFIG.vendRedirectUri; //// VIENE DE ENV

			const resultValidate = Joi.validate(req.query, createLinkAuthSchemaValidator, optionsJoi);  
			 if(resultValidate.error){
			 	resultValidate.errorJoi = resultValidate.error;
			 	resultValidate.error = formatJoiError(resultValidate.error);
			 	res.status(400);
			 	res.json(resultValidate.error);
			 	return resultValidate.error;
			 }
			 const { id_cliente_lealto } = resultValidate.value;
			 const state = JSON.stringify({ idLealto: id_cliente_lealto })
			 const url = `https://secure.vendhq.com/connect?response_type=code&client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&state=${state}`

			 res.status(200);
			 res.json({
			  valido: true, 
			  url, 
			  info: {
			  	mensaje: "Este es el link para dar authorizacion a lealto de utilizar su App de Vend"
			  }
			 });
			 return;
		}catch(error){
			console.log(error);
			res.status(500);
			res.json({ valido: false, url: null });
			return;					
		}
	};
	//hanblers for manipulation of Vend webhooks  
	async AppListarWebhooks(){
		const { req, res, optionsJoi } = this; 
		const { vendConfig } = req;
		const { getVendValidAccessToken, listVendWebhook } = this;
		try{
			const resultValidate = Joi.validate(req.query, listAllWebhooksSchemaValidator, optionsJoi);  
			 if(resultValidate.error){
			 	resultValidate.errorJoi = resultValidate.error;
			 	resultValidate.error = formatJoiError(resultValidate.error);
			 	res.status(400);
			 	res.json(resultValidate.error);
			 	return resultValidate.error;
			 }
			const { id_cliente_lealto: idCliente } = resultValidate.value;
			const { domainPrefix } = vendConfig;

			const token = await getVendValidAccessToken(idCliente, domainPrefix);

			const data = await listVendWebhook(domainPrefix, token);

			const re = { valido: true, data };

			res.status(200);
			res.json(re);
			return re;
		}catch(error){
			console.log(error);
			res.status(500);
			res.json({ valido: false, error: { mensaje: "Unexpected error "} });
			return;	
		}		
	};
	async AppCreateWebhook(){
		const { req, res, optionsJoi, WebHooks } = this; 
		const { vendConfig } = req;
		const { getVendValidAccessToken, createVendWebhook } = this;
		try{
			const resultValidate = Joi.validate(req.body, createVendWebhookSchemaValidator, optionsJoi);  
			 if(resultValidate.error){
			 	resultValidate.errorJoi = resultValidate.error;
			 	resultValidate.error = formatJoiError(resultValidate.error);
			 	res.status(400);
			 	res.json(resultValidate.error);
			 	return resultValidate.error;
			 }
			const { id_cliente_lealto: idCliente, host, type } = resultValidate.value;
			const { domainPrefix, idConfigExtra } = vendConfig;

			const token = await getVendValidAccessToken(idCliente, domainPrefix);

			const data = await createVendWebhook(domainPrefix, type, host, token);

			if(!data.error && data.id){
				await WebHooks.build({ 
					id_webhook: data.id,
					id_cliente: idCliente,
					endpoint: data.url,
					type: data.type
				}).save();
			}

			const re = { valido: true, data };

			res.status(200);
			res.json(re);
			return re;
		}catch(error){
			console.log(error);
			res.status(500);
			res.json({ valido: false, error: { mensaje: "Unexpected error "} });
			return;	
		}
	};
	async AppUpdateVendWebhook(){
		const { req, res, optionsJoi } = this; 
		const { WebHooks } = this;
		const { vendConfig } = req;
		const { getVendValidAccessToken, updateVendWebhook } = this;
		try{
			const resultValidate = Joi.validate(req.body, updateVendWebhookSchemaValidator, optionsJoi);  
			 if(resultValidate.error){
			 	resultValidate.errorJoi = resultValidate.error;
			 	resultValidate.error = formatJoiError(resultValidate.error);
			 	res.status(400);
			 	res.json(resultValidate.error);
			 	return resultValidate.error;
			 }
			const { id_cliente_lealto: idCliente, host, type, id_webhook: idWebhook, active } = resultValidate.value;
			const { domainPrefix, idConfigExtra } = vendConfig;

			const token = await getVendValidAccessToken(idCliente, domainPrefix);

			const data = await updateVendWebhook(domainPrefix, idWebhook, type, host, active, token);

			if(!data.error  && data.id){
				await WebHooks.update({ 
					id_webhook: data.id,
					id_cliente: idCliente,
					endpoint: data.url,
					type: data.type
				},{ where: { id_webhook:  data.id  } });
			}

			const re = { valido: true, data };

			res.status(200);
			res.json(re);
			return re;
		}catch(error){
			console.log(error);
			res.status(500);
			res.json({ valido: false, error: { mensaje: "Unexpected error "} });
			return;	
		}
	};
	async AppDeleteVendWebhook(){
		const { req, res, optionsJoi,WebHooks } = this; 
		const { vendConfig  } = req;
		const { getVendValidAccessToken, deleteVendWebhook } = this;
		try{
			const resultValidate = Joi.validate(req.query, deleteVendWebhookSchemaValidator, optionsJoi);  
			 if(resultValidate.error){
			 	resultValidate.errorJoi = resultValidate.error;
			 	resultValidate.error = formatJoiError(resultValidate.error);
			 	res.status(400);
			 	res.json(resultValidate.error);
			 	return resultValidate.error;
			 }
			const { id_cliente_lealto: idCliente, id_webhook: idWebhook } = resultValidate.value;
			const { domainPrefix } = vendConfig;

			const token = await getVendValidAccessToken(idCliente, domainPrefix);

			const data = await deleteVendWebhook(domainPrefix, idWebhook, token);

			if(data.status == 'success'){
				await WebHooks.destroy({ where: { id_webhook:  idWebhook  }});
			}

			const re = { valido: true, data };

			res.status(200);
			res.json(re);
			return re;
		}catch(error){
			console.log(error);
			res.status(500);
			res.json({ valido: false, error: { mensaje: "Unexpected error "} });
			return;	
		}
	};
	async AppDeleteAllWebhooks(){
	};
	//Procesar sale de vend
	async procesarVenta({ sale, idCliente, domainPrefix, metodoPagoLealtoId, token, dataLoaders, vendConfig }) {
		const { SucursalInfoExtra ,UsuariosInfoExtra ,AdministradorInfoExtra, Sucursal, Administrador } = this;
		const { getProduct } = this;
		const { realizarCanje, realizarAcumulacion, registrarTransaccionesCupon, TransaccionSaldo,  TransaccionEncuesta, EncuestaRespuesta  } = this;
		try{
				const { 
					saleVend,
					productsVend,
					productsVendBySku,
					adminsLealtoByVendId,
					sucuralesLealtoByVendId,
					usuarioLealtoByVendId,
					adminLealto,
					sucursalLealto 
				} = dataLoaders;
				
				if (R.isEmpty(sale) || R.isNil(sale)) {
					return false;
				}
				const {
					user_id: idAdminVend,
					register_id: idVendSucursal,
					sale_date: saleDate,
					payments: registerSalePayments, // es un array con los pagos que se hicieron normalmente es solo un tipo de pago, Pero puede cash y loyalty (pago pacial).
					customer_id,
					line_items: registerSaleProducts,
					id: idSale,
					invoice_number,
					status,
					created_at,
					short_code,
					total_loyalty: totalLoyalty,
					total_payment: totalPayment,
					total_price: totalPrice,
					total_tax: totalTax,
					total_to_pay: totalToPay,
					total_loyalty_value
				} = sale;
				console.log(`SaleId ===> ${idSale} ${status}`);
				if(status == PAYMENTS_STATUS.ONACCOUNT || status == PAYMENTS_STATUS.LAYBY ||  status == PAYMENTS_STATUS.VOIDED){
					console.log(`No valid status [${status}]`);
					return false;
				}
				const receiptNumber = idSale;// no se puede usar el invoice xq no se asegura que sean unicos
				const alreadySync = await TransaccionSaldo.findOne({ where: { num_factura: receiptNumber } });
				if(alreadySync){
					console.log(`==alreadySync=====> ${true} ==========>`)
					return true;
				}
		
				const sucursaLealto = await sucuralesLealtoByVendId.load(idVendSucursal).catch(err => { console.log(err); return null }); 
				const usuarioLealto = await usuarioLealtoByVendId.load(customer_id).catch(err => { console.log(err); return null }); 
				const admin = await adminsLealtoByVendId.load(idAdminVend).catch(err => { console.log(err); return null }); 

				if(R.isEmpty(usuarioLealto) || R.isNil(usuarioLealto)){
					console.log(`No usuarioLealto`);
					return false;
				}		
	
				const idSucursal = (sucursaLealto) ? sucursaLealto.id_sucursal : await sucursalLealto.load('1');
		
				const idAdminLealto = (admin) ? admin.id_admin : await adminLealto.load('1');
		
		
				const { lineasFactura, fallado } = await Promise.reduce(registerSaleProducts, async (re, producto, i) => {
					let { lineasFactura, objsProductos, fallado } = re;
					if (fallado) {
						return re;
					}
					const productoInfo = (objsProductos[producto.product_id]) ? objsProductos[producto.product_id] :
						await productsVend.load(producto.product_id).catch(error => {
							console.log(error);
							fallado = true;
						});
					if (!productoInfo) {
						fallado = true;
						return { lineasFactura, objsProductos, fallado: fallado  };
					}
		
					const linea = {
						sku_producto: productoInfo.sku,
						nombre_producto: productoInfo.name,
						impuestos_producto: producto.tax,
						precio_producto: producto.price,
						cantidad_producto: producto.quantity
					}
					lineasFactura = lineasFactura.concat(linea);
					if (!objsProductos[producto.product_id]) {
						objsProductos[producto.product_id] = productoInfo;
					}
					return { lineasFactura, objsProductos, fallado: re.fallado }
				}, { lineasFactura: [], objsProductos: {}, fallado: false }).catch(error => { console.log(error); return {} });
		
		
				const facturaObj = (lineasFactura && !fallado) ? {
					numero_factura: receiptNumber,
					subtotal: totalPrice,
					impuestos: totalTax,
					total: totalPrice,
					fecha: created_at,
					lineas_factura: lineasFactura
				} : null;
		
		
		
				//** CANJE **
				//Vamos realizar el canje de puntos respectivos, no importa el status que tenga el sale, si hay un payment por puntos hay que descontarlo
				// siempres es solo payment de cada tipo, por ejemplo no van a venir 2 payments de loyalty 
				let loyaltyCanjeado = 0;
				
				await Promise.each(registerSalePayments, (payment, i) => {
					const { id, amount, payment_type_id: paymentType } = payment;
					if (paymentType == PAYMENTS_IDS.loyalty) {
						console.log(`============> CANJE DE PUNTOS`);
						loyaltyCanjeado += parseFloat(amount);
						if(usuarioLealto && loyaltyCanjeado > 0);
						return realizarCanje(idCliente, idAdminLealto, usuarioLealto.id_usuario, idSucursal, totalPrice, amount, receiptNumber, facturaObj, saleDate);
					}
					return;
				});
				
		

				//funciona con todo menos con compras parciales
				/*
				let loyaltyPoints = registerSaleProducts.reduce((result, product, i) =>
					(product.total_loyalty_value) ? result + product.total_loyalty_value : result, 0);

				const isCompraParcial = registerSalePayments.some((p) => p.payment_type_id == PAYMENTS_IDS.loyalty) && registerSalePayments.length >= 2;
				console.log(`[isCompraParcial] => ${isCompraParcial}`)
		
				const porcentajeReduccionPorCompraParcial = ((loyaltyCanjeado * 100) / totalPrice) / 100;
				loyaltyPoints = isCompraParcial? (loyaltyPoints - (loyaltyCanjeado * porcentajeReduccionPorCompraParcial)) : loyaltyPoints // isCompraParcial ? ;
		
				*/

				let loyaltyPoints = totalLoyalty;
				TOTAL_ACUM += loyaltyPoints;

				const regalia = registerSalePayments.reduce((r, e) => e.retailer_payment_type_id == metodoPagoLealtoId ? r + e.amount : r, 0);

				loyaltyPoints = loyaltyPoints - regalia;

				//** ACUMULACION REGALIA BIEVENIDA/ENCUESTA** 
				if(usuarioLealto && regalia > 0){
					const productoInfo = await dataLoaders.productsVend.load(R.prop('product_id',registerSaleProducts[0]));
					const {
						productoRegaliaInvitacionSku, 
						productoRegaliaBienvenidaSku, 
						productoRegaliaEncuestaSku, 
					} = vendConfig;
					const esEncuesta = R.propEq('sku',productoRegaliaEncuestaSku);
					let tipo = 'AcreditacionBonus';
					if(R.propEq('sku',productoRegaliaInvitacionSku)(productoInfo)){
						console.log(`============> ACUMULACION BONUS`);
						tipo = 'AcreditacionBonus'
					}
					else if(R.propEq('sku',productoRegaliaBienvenidaSku)(productoInfo)){
						console.log(`============> ACUMULACION BONUS`);
						tipo = 'AcreditacionBonus'
					}
					else if(esEncuesta(productoInfo)){
						console.log(`============> ACUMULACION ENCUESTA`);
						tipo = 'AcreditacionEncuesta';
					}
					const transaccionAcum = await realizarAcumulacion(tipo, idCliente, idAdminLealto, usuarioLealto.id_usuario, idSucursal, 0, parseFloat(regalia), receiptNumber, facturaObj, saleDate).catch(er => console.log(er));
					if(esEncuesta(productoInfo)){
						const { idRespuesta, puntos } = GET_INFO_SHORT_CODE_ENCUESTA(short_code);
						const idTransaccion = R.prop('id',transaccionAcum);
						const respuestaValida = await EncuestaRespuesta.findOne({ where : { id: idRespuesta }});
						if(!R.isNil(respuestaValida) && !R.isNil(idTransaccion)){
							await TransaccionEncuesta.build({ 
								id_encuesta_respuesta: idRespuesta,
								id_transaccion_saldo: idTransaccion,
								puntos
							}).save();
						}
					}
				}
				//** ACUMULACION NORMAL** 
				// No se hace terminado la compra, no se realiza acumulacion
				// El usuario no tiene loyalty activado
				// El usuario no existe
				if (usuarioLealto && loyaltyPoints > 0) {
					console.log(`============> ACUMULACION NORMAL`);
					await realizarAcumulacion('Acreditacion',idCliente, idAdminLealto, usuarioLealto.id_usuario, idSucursal, totalPrice, parseFloat(loyaltyPoints), receiptNumber, facturaObj, saleDate).catch(er => console.log(er));
				};
		
				await registrarTransaccionesCupon(idCliente, domainPrefix, idSale, usuarioLealto.id_usuario, idSucursal, receiptNumber, sale, dataLoaders);
		
				return true;
		}catch(error){
			console.log(error);
			return false;
		}
	};
	async registrarFacturaObj(id_sucursal,facturaObj){
		if(!facturaObj){
			return null;
		}
		const {  Factura, LineaFactura } = this;
		const params = {
			id_sucursal,
			numero_factura: facturaObj.numero_factura,
			subtotal: facturaObj.subtotal,
			impuestos: facturaObj.impuestos,
			total: facturaObj.total,
			fecha: facturaObj.fecha
		}

		const factura = await Factura.build(params).save().catch(error => { console.log(error); return null; });

		if (factura && Array.isArray(facturaObj.lineas_factura)) {
			await Promise.each(facturaObj.lineas_factura, (linea, i) => {
				const paramsLinea = {
					id_factura: factura.id,
					sku_producto: linea.sku_producto,
					nombre_producto: linea.nombre_producto,
					precio_producto: linea.precio_producto,
					impuestos_producto: linea.impuestos_producto,
					cantidad_producto: linea.cantidad_producto
				}
				return LineaFactura.build(paramsLinea).save();
			});
			return factura;
		}
		return factura;
	};
	async saldoDisponibleUsuarioLealto(id_usuario){
		const { sequelizeInstance } = this;
		const result = await sequelizeInstance.query('SELECT  ifnull(SUM(monto),0) as saldo_disponible from pl_puntos where id_usuario = ? ', { replacements: [id_usuario], type: Sequelize.QueryTypes.SELECT })
		if(result[0]){
			const number = result[0].saldo_disponible;
			return (number !== null && typeof number != 'undefined' && !isNaN(number))? parseFloat(number):  0;
		}else{
			return 0;
		}
	};
	async realizarAcumulacion(tipo, idCliente, idAdminLealto, id_usuario, idSucursal, totalPrice, loyaltyPoints, receiptNumber, facturaObj, saleDate = new Date()) {
		const { Usuario, UsuariosInfoExtra, TransaccionSaldo, Saldo, registrarFacturaObj, saldoDisponibleUsuarioLealto } = this;
		saleDate = moment(saleDate);
		const start = saleDate.clone().startOf('month');
		const end = saleDate.clone().endOf('month');

		let saldo = await Saldo.findOne({
			where: {
				[Op.and]: [
					{ id_usuario },
					Sequelize.where(Sequelize.fn('MONTH', Sequelize.col('created_at')), start.format('MM')),
					Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('created_at')), end.format('YYYY'))
				]
			}
		});
		if (R.isNil(saldo) || R.isEmpty(saldo)) {
			saldo = await Saldo.build({ monto: loyaltyPoints, id_usuario, vencimiento: null, created_at: saleDate.toDate() }).save(); // En vend no vencen los puntos entonces aqui tampoco 
		} else {
			await Saldo.increment('monto', { by: loyaltyPoints, where: { id: saldo.id } });
		}

		const factura = await registrarFacturaObj(idSucursal,facturaObj);

		const paramsTransaccion = {
			id_factura: ((factura) ? factura.id : null),
			num_factura: (receiptNumber) ? receiptNumber : null,
			id_usuario,
			id_admin: idAdminLealto,
			monto: loyaltyPoints,
			monto_real: totalPrice,
			saldo_disponible: await saldoDisponibleUsuarioLealto(id_usuario),
			tipo: TRANSACCIONES_LEALTO_OBJ[tipo].tipo,
			id_label_titulo: TRANSACCIONES_LEALTO_OBJ[tipo].id_label_titulo,
			id_label_descripcion: TRANSACCIONES_LEALTO_OBJ[tipo].id_label_descripcion,
			id_sucursal: idSucursal,
			created_at: saleDate.toDate()//cambiar                        
		};

		const trans = await TransaccionSaldo.build(paramsTransaccion).save();
		await Usuario.increment(['puntos', 'puntos_historicos', 'puntos_totales'], { by: loyaltyPoints, where: { id: id_usuario } });
		return trans.toJSON();
	};
	async realizarCanje(idCliente, idAdminLealto, id_usuario, idSucursal, totalPrice, amount, receiptNumber, facturaObj, saleDate = new Date()){
		const { sequelizeInstance, Usuario, UsuariosInfoExtra, TransaccionSaldo, Saldo, registrarFacturaObj, saldoDisponibleUsuarioLealto } = this;
		saleDate = moment(saleDate);
		const saldos = await Saldo.findAll({
			where: { id_usuario },
			order: [
				['created_at', 'ASC']
			]
		});

		//restar el saldo al usuario
		await Promise.reduce(saldos,async (restante,saldo,i) => {
			if (restante == 0) return 0
			if (saldo.monto <= restante){
				const r = restante - saldo.monto
				await saldo.destroy();
				return r;
			}
			if (saldo.monto > restante){
				await Saldo.decrement('monto', { by: restante, where: { id: saldo.id } });
				return 0;
			}
			return 0;
		},amount);
		const saldo_disponible = await saldoDisponibleUsuarioLealto(id_usuario)
		await Usuario.update({ puntos: saldo_disponible }, { where: { id: id_usuario } });

		const factura = await registrarFacturaObj(idSucursal,facturaObj);

		let params_transaccion = {
      id_factura: ((factura) ? factura.id : null),
      num_factura: ((receiptNumber) ? receiptNumber : null),
      id_admin: idAdminLealto,
      id_usuario,
      monto: amount,
      monto_real: totalPrice,
      saldo_disponible,
      tipo: TRANSACCIONES_LEALTO_OBJ.Redencion.tipo,
      id_label_titulo: TRANSACCIONES_LEALTO_OBJ.Redencion.id_label_titulo,
      id_label_descripcion: TRANSACCIONES_LEALTO_OBJ.Redencion.id_label_descripcion,
      id_sucursal: idSucursal,
      created_at: saleDate.toDate() //cambiar                        
    }
    return TransaccionSaldo.build(params_transaccion).save().then(result => { return result; });
	};
	//sync functions
	async syncCustomers({ key, idCliente, domainPrefix }){
		const { getVendValidAccessToken, listCustomers } = this;
		const { UsuariosInfoExtra, PreRegistro, sequelizeInstance } = this;

		const lengthQueueUsuarios = SYNC_CUSTOMERS_QUEUE;
		const syncUsuariosQueue = async.queue( async (task) => process(task), lengthQueueUsuarios);
		const process = async (customer) => { 
			try{
				//console.log({ code: customer.customer_code })
				const usuario = await UsuariosInfoExtra.findOne({ where: { identificador: customer.id  } });
				if(usuario){
					return;
				}
				const params = {
					id_externo: customer.id,
					email: customer.email,
					telefono: customer.mobile,
					codigo: customer.customer_code,
					apellido: customer.last_name,
					nombre: customer.first_name,
				};
				const aux = await PreRegistro.findOne({ where: { id_externo: customer.id }, attributes: ['id'] });
				if(aux){
					console.log('==============> UPDATE -1 ===========================>')
					await aux.update(params);
				}else{
					console.log('==============> INSERT -1 ===========================>');
					params.estado = ESTADO_PRE_REGISTRO.activo;
					await PreRegistro.build(params).save();
				}
			}catch(error){
				console.log(error);
			}
		};
		const func = async (after,contador = 0) => {
			const token = await getVendValidAccessToken(idCliente, domainPrefix);
			const { data, version } = await listCustomers(domainPrefix, after, null, 200, false, token);
			AppSincronizarUsuariosEstado[key] = STATE(INPROGRESS,0);
			if(R.isEmpty(data) || R.isNil(data)){
				AppSincronizarUsuariosEstado[key].state = DONE;
				return true;
			}
			if(Array.isArray(data)){
				data.forEach((c) => syncUsuariosQueue.push(c) );
				await syncUsuariosQueue.drain();
				contador = contador + data.length;
				AppSincronizarUsuariosEstado[key] = STATE(INPROGRESS,contador);
			};
			if(R.isNil(version)){
				AppSincronizarUsuariosEstado[key].state = DONE;
				return true;
			}
			return func(version.max,contador);
		};
		return func(0,0);
	};
	async syncTransactions({ key, idCliente, dateFrom, idUsuario ,vendConfig }){
				const { UsuariosInfoExtra } = this;
				const { getVendValidAccessToken, createDataLoaders } = this;
				const { 
					searchSale,
					procesarVenta,
					getCustomer,
					saldoDisponibleUsuarioLealto,
					realizarAcumulacion
				} = this;
				const singleUser = (!R.isNil(idUsuario) && !R.isEmpty(idUsuario));
				const { domainPrefix, metodoPagoLealtoId } = vendConfig
				//Queue usuarios
				const lengthQueueUsuarios = SYNC_TRANSACTIONS_QUEUE;
				const syncUsuariosQueue = async.queue( async (task) => syncUsuariosTransacciones(task), lengthQueueUsuarios);
				const maxUsuario = 500;
				const getBatch = async (idInicial = 0) =>{
					const where = singleUser ? { id_usuario: idUsuario } : {  id_usuario : { [Op.gt]: idInicial } }
					return await UsuariosInfoExtra.findAll({ 
						where,
						limit: maxUsuario,
						order: [[Sequelize.literal(`id_usuario ASC`)]]
					})
				}
				/*
					sacar las transacciones de vend del usuario mediante el customer id
					unas vez sacadas las transacciones hay que ver cuales son de acumulacion y cuales son de redencion de puntos
					se tienen que acumular y canjear las transacciones sin dar una notificacion, solo a las que no han sido procesadas por baum
					el servicio de Vend para consultar las transacciones seria https://{domain_prefix}.vendhq.com/api/2.0/search mediante Sales y por customer_id
					en lealto las transacciones se guardan con el invoicde de Vend entonces se podrian comparar para ver si ya fueron procesadas o no
					el procesado de cada usuario se tiene que ser mediante async queue para no sobrecargar la base de datos
					todo tiene que quedar registrado en lealto y ser trasparente
				*/
				const token = await getVendValidAccessToken(idCliente, domainPrefix);
				const dataLoaders = createDataLoaders({ token, idCliente, domainPrefix, dateFrom });

				let total = 0;
				const syncUsuariosTransacciones = async(task) => {
					try{
							const { usuario, token  } = task;
							const customer_id = usuario.identificador;
							const date = dateFrom ? moment(dateFrom).format('YYYY-MM-DD') : undefined;
							const process = async (offset, continuar) => {
								if(!continuar){
									return true;
								}
								let sales = await searchSale(domainPrefix, customer_id, offset, date, token);
								if(!R.prop('data',sales)){
									return false;
								}
								sales = sales.data;
								if(sales.length <= 0){
									return await process(0,false);
								}
								await Promise.each(sales,(sale) => procesarVenta({ sale, idCliente, domainPrefix, token, metodoPagoLealtoId, dataLoaders, vendConfig }));
								offset += PAGE_SIZE_SEARCH_SALES; // el limit en el servicio esta definido como 50
								return await process(offset,true);
							}
							await process(0,true)

							//ahora se dan los puntos que hagan falta
							const customer = R.prop('data',await getCustomer(domainPrefix, customer_id, token));
							if(!customer){
								console.log(`No customer ${customer_id}`);
								return false;
							}
							const { loyalty_balance, created_at } = customer;
							const saldoLealto = await saldoDisponibleUsuarioLealto(usuario.id_usuario);
							if(loyalty_balance > saldoLealto){
								const loyaltyPoints = loyalty_balance - saldoLealto;
								const receiptNumber = SYNC_RECEIPT_NUMBER(idCliente,usuario.id_usuario);
								await realizarAcumulacion('AcreditacionBonus', idCliente, null, usuario.id_usuario, null, 0, loyaltyPoints, receiptNumber, null, new Date());
							}
							return true;
					}catch(error){
						console.log(error);
						return false;
					}
				};
				const processarUsuarios = async (arrayUsuarios = []) => {
						if(arrayUsuarios.length == 0){
							return Promise.resolve(true);
						}else{
							total += arrayUsuarios.length;
							AppSincronizarPuntosUsuarioEstado[key] = STATE(INPROGRESS,total);
							arrayUsuarios.forEach((usuario) => syncUsuariosQueue.push({ usuario, token }));
							await syncUsuariosQueue.drain();
							const idLast = R.takeLast(1,arrayUsuarios)[0].id_usuario; 
							const nuevoArrayUsuarios = singleUser ? [] : await getBatch(idLast);
							return await processarUsuarios(nuevoArrayUsuarios); // aqui va nuevoArrayUsuarios, se pone arraya para nada mas hacer una corrida de prueba
						}
				};
				const firstBatch = await getBatch();
				AppSincronizarPuntosUsuarioEstado[key] = STATE(INPROGRESS,firstBatch.length);
				return processarUsuarios(firstBatch);
	};
	async syncPendientes({ idCliente, vendConfig }){
		const {  PreRegistro } = this;
		const { 
			sincronizarPuntos
		} = this;
		//Queue usuarios
		const lengthQueueUsuarios = SYNC_PENDIENTES_QUEUE;
		const syncUsuariosQueue = async.queue( async (task) => sincronizarPuntos(task), lengthQueueUsuarios);
		const maxUsuario = 20;

		const data = await PreRegistro.findAll({ where: { estado: ESTADO_PRE_REGISTRO.pendiente }, limit: maxUsuario });

		data.forEach((e) => {
			const task = {
				idCliente,
				idUsuario: e.id_usuario,
				vendConfig,
				idCustomer: e.id_externo
			};
			syncUsuariosQueue.push(task);
		})
		await syncUsuariosQueue.drain();
		return true;
	}
	//handlers de sincronizar usuarios
	async AppSincronizarUsuarios(){
		const { req, res, optionsJoi, WebHooks } = this; 
		const { vendConfig  } = req;
		const { syncCustomers } = this;
		try{
			const resultValidate = Joi.validate(req.body, syncUsuariosPuntos, optionsJoi);  
			 if(resultValidate.error){
			 	resultValidate.errorJoi = resultValidate.error;
			 	resultValidate.error = formatJoiError(resultValidate.error);
			 	res.status(400);
			 	res.json(resultValidate.error);
			 	return resultValidate.error;
			 }

			const { id_cliente_lealto: idCliente } = resultValidate.value;
			const { domainPrefix, idConfigExtra } = vendConfig;

			let key = `${idCliente}-sincronizar-usuarios`

			console.log(`==========================================>`)
			console.log({ AppSincronizarUsuariosEstado })
			console.log(`==========================================>`)

			if(!AppSincronizarUsuariosEstado.hasOwnProperty(key)){
				AppSincronizarUsuariosEstado[key] = STATE(AVAILABLE,0);
			}
			if(AppSincronizarUsuariosEstado[key].state == INPROGRESS){
					res.status(200);
					res.json({ valido: true, estado: AppSincronizarUsuariosEstado[key] });
					return;
			}
			if(AppSincronizarUsuariosEstado[key].state == DONE || AppSincronizarUsuariosEstado[key].state == AVAILABLE){
				AppSincronizarUsuariosEstado[key] = STATE(INPROGRESS,0);
			}
			let starting = true;
			const params = {
				key,
				idCliente,
				domainPrefix
			}
			syncCustomers(params).then(r => AppSincronizarUsuariosEstado[key].state = DONE ).catch(error => console.log(error)); // esta funcion deberia enviar un correo o algo cuando termina

			if(starting){
				AppSincronizarUsuariosEstado[key].state = INPROGRESS;
			}

			res.status(200);
			res.json({ valido: true, estado: AppSincronizarUsuariosEstado[key] });
			return;
		}catch(error){
			console.log(error);
			res.status(500);
			res.json({ valido: false, error: { mensaje: "Unexpected error "} });
			return;	
		}
	};
	async AppEstatusSincronizacionUsuario(){
		const { req, res, optionsJoi } = this; 
		try{
			const resultValidate = Joi.validate(req.body, sincronizarUsuariosSchemaValidator, optionsJoi);  
			 if(resultValidate.error){
			 	resultValidate.errorJoi = resultValidate.error;
			 	resultValidate.error = formatJoiError(resultValidate.error);
			 	res.status(400);
			 	res.json(resultValidate.error);
			 	return resultValidate.error;
			 }	
			const { id_cliente_lealto: idCliente } = resultValidate.value;

			let key = `${idCliente}-sincronizar-usuarios`;

			if(!AppSincronizarUsuariosEstado.hasOwnProperty(key)){
				AppSincronizarUsuariosEstado[key] = STATE(AVAILABLE,0);
			}

			res.status(200);
			res.json({ valido: true, estado: AppSincronizarUsuariosEstado[key] });
			return;

		}catch(error){
			console.log(error);
			res.status(500);
			res.json({ valido: false, error: { mensaje: "Unexpected error "} });
			return;	
		}
	};
	// sincronizar puntos de usuarios
	async AppSincronizarPuntos(){
		const { req, res, optionsJoi, WebHooks } = this; 
		const { vendConfig  } = req;
		const { syncTransactions } = this;
		const resultValidate = Joi.validate(req.body, syncUsuariosPuntos, optionsJoi);  
		if(resultValidate.error){
			resultValidate.errorJoi = resultValidate.error;
			resultValidate.error = formatJoiError(resultValidate.error);
			res.status(400);
			res.json(resultValidate.error);
			return resultValidate.error;
		}

		let { id_cliente_lealto: idCliente, dateFrom } = resultValidate.value;
		const { domainPrefix } = vendConfig;
		const key = `${idCliente}-sincronizar-puntos`
			try{

				if(!AppSincronizarPuntosUsuarioEstado.hasOwnProperty(key)){
					AppSincronizarPuntosUsuarioEstado[key] = STATE(AVAILABLE,0);
				}
				if(AppSincronizarPuntosUsuarioEstado[key].state == INPROGRESS){
						res.status(200);
						res.json({ valido: true, estado: AppSincronizarPuntosUsuarioEstado[key] });
						return;
				}
				if(AppSincronizarPuntosUsuarioEstado[key].state == DONE || AppSincronizarPuntosUsuarioEstado[key].state == AVAILABLE){
					AppSincronizarPuntosUsuarioEstado[key] = STATE(INPROGRESS,0);
				}

				res.status(200);
				res.json({ valido: true, estado: AppSincronizarUsuariosEstado[key] });
				await syncTransactions({ key, idCliente, dateFrom, vendConfig, idUsuario: null });
				AppSincronizarPuntosUsuarioEstado[key].state = DONE;
				return;
			}catch(error){
				AppSincronizarPuntosUsuarioEstado[key] = STATE(AVAILABLE,0);
				console.log(error);
				res.status(500);
				res.json({ valido: false, error: { mensaje: "Unexpected error "} });
				return;	
			}
	};
	async AppEstatusSincronizacionPuntosUsuario(){
		const { req, res, optionsJoi } = this; 
		try{
			const resultValidate = Joi.validate(req.body, syncUsuariosPuntosState, optionsJoi);  
			 if(resultValidate.error){
			 	resultValidate.errorJoi = resultValidate.error;
			 	resultValidate.error = formatJoiError(resultValidate.error);
			 	res.status(400);
			 	res.json(resultValidate.error);
			 	return resultValidate.error;
			 }	
			const { id_cliente_lealto: idCliente } = resultValidate.value;

			const key = `${idCliente}-sincronizar-puntos`

			if(!AppSincronizarPuntosUsuarioEstado.hasOwnProperty(key)){
				AppSincronizarPuntosUsuarioEstado[key] = STATE(AVAILABLE,0);
			}

			res.status(200);
			res.json({ valido: true, estado: AppSincronizarPuntosUsuarioEstado[key] });
			return;

		}catch(error){
			console.log(error);
			res.status(500);
			res.json({ valido: false, error: { mensaje: "Unexpected error "} });
			return;	
		}
	};
	//Cola de acciones lealto
	async reportarAccionPendiente(data = {}, alias = '', retries = 1, error = {}) {
	    const { SysColaAcciones } = this; // las acciones pendientes son procesados por el servidor de cronjobs. Las acciones pendientes son a nivel de cliente de lealto x lo que no se ocupa un id de cliente un su data o como columna
	    const params = {
	        alias,
	        data: JSON.stringify(data),
	        intentos: retries,
	        error: JSON.stringify(error),
	        estado: ESTADO_ACCIONES.error,
	        completed_at: null
	    };
	    return SysColaAcciones.build(params).save().catch(error => logger.alert(error));
	}
}

//Vend
module.exports.AppCustomerUpdate = (req,res) => new VendController(req,res).AppCustomerUpdate();
module.exports.AppSaleUpdate = (req,res) => new VendController(req,res).AppSaleUpdate();
module.exports.AppOuthAuthorize = (req,res) => new VendController(req,res).AppOuthAuthorize();
module.exports.AppcreateAuthUrl = (req,res) => new VendController(req,res).AppcreateAuthUrl();

module.exports.AppListarWebhooks = (req,res) => new VendController(req,res).AppListarWebhooks();
module.exports.AppCreateWebhook = (req,res) => new VendController(req,res).AppCreateWebhook();
module.exports.AppUpdateVendWebhook = (req,res) => new VendController(req,res).AppUpdateVendWebhook();
module.exports.AppDeleteVendWebhook = (req,res) => new VendController(req,res).AppDeleteVendWebhook();

module.exports.AppSincronizarUsuarios = (req,res) => new VendController(req,res).AppSincronizarUsuarios();
module.exports.AppEstatusSincronizacionUsuario = (req,res) => new VendController(req,res).AppEstatusSincronizacionUsuario();


module.exports.AppSincronizarPuntos = (req,res) => new VendController(req,res).AppSincronizarPuntos();
module.exports.AppEstatusSincronizacionPuntosUsuario = (req,res) => new VendController(req,res).AppEstatusSincronizacionPuntosUsuario();


//Lealto
module.exports.AppVendCustomerCreateOrUpdate = (req,res) => new VendController(req,res).AppVendCustomerCreateOrUpdate();
module.exports.AppVendEarnLoyaltyPoints = (req,res) => new VendController(req,res).AppVendEarnLoyaltyPoints();


//Cronjobs
const getConfigs = async () => {
	const query = `
			SELECT 
			  cliente.id as idCliente,
				vend_domain_prefix as domainPrefix,
				vend_private_token as privateToken,
				vend_customer_group_id as customerGroupId,
				vend_user_id as userId,
				vend_retailer_id as retailerId,
				vend_producto_regalia_invitacion_sku as productoRegaliaInvitacionSku,
				vend_producto_regalia_bienvenida_sku as productoRegaliaBienvenidaSku,
				vend_producto_regalia_encuesta_sku as productoRegaliaEncuestaSku,
				vend_metodo_pago_lealto_id as metodoPagoLealtoId,
				vend_tienda_lealto_id as tiendaLealtoId,
				vend_cajero_lealto_username as cajeroLealtoUsername,
				config_db.nombre, 
				config_db.usuario,
				config_db.password,
				config_db.host_escritura, 
				config_db.host_lectura, 
				config_db.puerto 
			FROM 
				cliente 
			INNER JOIN 
				vend_config on vend_config.id_cliente = cliente.id
			INNER JOIN
				config_db on cliente.id_config_db = config_db.id
			WHERE 
				estado = 1
	`

	const options =  { type: sequelize.QueryTypes.SELECT };
	return await sequelizeMaster.query(query,options);
};
const cronjobSyncTrasactions = async () => {
	try{
		const clientes = await getConfigs();
		const lengthQueueClientes = 3;
		const syncClientesQueue = async.queue( async (task) => taskFunc(task), lengthQueueClientes);
		const taskFunc = async ({ config }) => {
				const idCliente = config.idCliente;
				const req = {}, res = {}
				const sequelizeInstance =  sequelizeInit(config);
				req.sequelizeInstance = sequelizeInstance
				const key = `${idCliente}-sincronizar-puntos`
				const c = new VendController(req,res);
				await c.syncTransactions({ key, idCliente, dateFrom: null, vendConfig: config, idUsuario: null }).catch(error => console.log(error));
				AppSincronizarPuntosUsuarioEstado[key].state = DONE
				return true;
		}
		clientes.forEach((config) => syncClientesQueue.push({ config }));
		return;
	}catch(error){
		console.log(error);
		return;
	}
};
const cronjobSyncPendientes = async () => {
	try{
		const clientes = await getConfigs();
		const lengthQueueClientes = 3;
		const syncClientesQueue = async.queue( async (task) => taskFunc(task), lengthQueueClientes);
		const taskFunc = async ({ config }) => {
				const idCliente = config.idCliente;
				const req = {}, res = {}
				const sequelizeInstance =  sequelizeInit(config);
				req.sequelizeInstance = sequelizeInstance
				const key = `${idCliente}-sincronizar-puntos`
				const c = new VendController(req,res);
				await c.syncPendientes({ idCliente, vendConfig: config }).catch(error => console.log(error));
				AppSincronizarPuntosUsuarioEstado[key].state = DONE
				return true;
		}
		clientes.forEach((config) => syncClientesQueue.push({ config }));
		return;
	}catch(error){
		console.log(error);
		return;
	}
};
const cronjobSyncCustomers = async () => {
	try{
		const clientes = await getConfigs();
		const lengthQueueClientes = 3;
		const syncClientesQueue = async.queue( async (task) => taskFunc(task), lengthQueueClientes);
		const taskFunc = async ({ config }) => {
				const idCliente = config.idCliente;
				const req = {}, res = {}
				const sequelizeInstance =  sequelizeInit(config);
				req.sequelizeInstance = sequelizeInstance
				const key = `${idCliente}-sincronizar-usuarios`;
				const c = new VendController(req,res);
				await c.syncCustomers({ key, idCliente, domainPrefix: config.domainPrefix }).catch(error => console.log(error));
				AppSincronizarUsuariosEstado[key].state = DONE
				return true;
		}
		clientes.forEach((config) => syncClientesQueue.push({ config }));
		return;
	}catch(error){
		console.log(error);
		return;
	}
};
//test

/// Cronjob sync Pendientes
schedule.scheduleJob(CRONJOBS.syncPendientes,cronjobSyncPendientes);
/// Cronjob sync transactions
schedule.scheduleJob(CRONJOBS.syncTransactions,cronjobSyncTrasactions);
/// Cronbjos sync customers
schedule.scheduleJob(CRONJOBS.syncCustomers,cronjobSyncCustomers);

//Utility
const isJSON = (j) => {
	try{	
		let a = JSON.parse(j);
		return true;
	}catch(err){
		console.log({err,j})
		return false;
	}
};
const formatJoiError = (joiError,objName) => {
        if (!joiError.isJoi) {
            return { mensaje: 'Something' };
        }

        if (!Array.isArray(joiError.details)) {
            return { mensaje: 'Something' };
        }
        const parametros = joiError.details.reduce((report, error, i) => {
            let path;
            if(error.path == 0){
            	if(Array.isArray(error.context.present)){
            		path = error.context.present.reduce((r,e,i) => {
            				return {...r, [e]: error.message }
            		},{});
            		return  Object.assign({}, path, report);
            	}
                return error.message;
            }else{
                path =  error.path.reduceRight((r, e, i, array) => ({
                    [e]: ((i == array.length - 1) ? error.message : r)
                }), {});
                return Object.assign({}, path, report);
            }
        }, {});

        return { valido: false, error: { parametros: ((objName )? { [objName] : parametros } : parametros )  } };
};
const getCountryCode = (tel,region) => {
	if(!tel || !region){
		return null
	}
	const number = phoneUtil.parseAndKeepRawInput(tel, region);
	return 	phoneUtil.isValidNumber(number)? phoneUtil.getRegionCodeForNumber(number) : null;
};
