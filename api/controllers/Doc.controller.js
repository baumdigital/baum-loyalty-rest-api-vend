

//UTILIY
const R = require('ramda');

const recursivoFirstObject = (obj,keyName) => {
	if(obj == null){
		return null;
	}
	const keys = Object.keys(obj);

	return keys.reduce((final,k,i) => {
		if(final.hasOwnProperty(keyName)){
			return final;
		}
		if(k == keyName){
			return {tags: obj[k]};
		}
		if(keys.length == i +1){
			return recursivoFirstObject(null,keyName);
		}
		const obj02 = obj[k];
		return recursivoFirstObject(obj02,keyName);
	},{});
}

const firstObject = (obj = {},keyName) => {
	const result = recursivoFirstObject(obj,keyName);

	return (result && result[keyName])? result[keyName] : null;
}

function uniqTags(a) {
    let seen = {};
    return a.filter(function(item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}


class DocsController {

	constructor(){
		this.getDocsForApiGateway = this.getDocsForApiGateway.bind(this);
		this.AppGetApiGatewayDocsImport = this.AppGetApiGatewayDocsImport.bind(this);
	}

	getDocsForApiGateway(swaggerJSDoc = {},url_backend){ // para el api gateway no puede haber  securityDefinitions  ni tags, y armamor los paths para la documentacion en apigateway
		// hay que contruir un objeto spec de swagger con las varas de amazon para que se mapee automatico https://docs.aws.amazon.com/es_es/apigateway/latest/developerguide/api-gateway-swagger-extensions.html
			   const doc =  Object.assign({}, swaggerJSDoc); // clonamos
			   const paths = Object.keys(doc.paths);
			   const pathsToShow = paths;
			   const docsToShowObj =  pathsToShow.reduce((result,path,i) => {
						const pathAux = doc.paths[path];
						const method = Object.keys(pathAux)[0];
						pathAux[method]['security'] = [{'api_key': []}];
						pathAux[method]['produces'] = ['application/json'];
						pathAux[method]['x-amazon-apigateway-integration'] = {
							"responses":
			                    {
			                        "default":
			                        {
			                            "statusCode": "200",
			                            "responseParameters":
			                            {
			                                //"method.response.header.Content-Type": "'application/json'"
			                            },
			                            "responseTemplates":
			                            {
			                                //"application/json": "''"
			                            }
			                        }
			                    },
			                    "uri": `${url_backend}${path}`,
			                    "httpMethod": method.toUpperCase(),
			                    "type": "http_proxy",
			                    "passthroughBehavior": "when_no_match"
						}
						if(pathAux[method]['tags']){
							delete pathAux[method]['tags']; 
						}
						const pathFinal = { [path] : pathAux };

						return Object.assign(result, pathFinal);
			   },{})

			   const finalExportPaths = Object.assign(docsToShowObj,paths_docs_api_gateway(url_backend));

				if(doc['tags']){
					delete doc['tags'];
				}

				doc['securityDefinitions'] = {
				  api_key: {
				    type: "apiKey",
				    name: "x-api-key",
				    in: "header"
				  }
				}
				doc['paths'] = finalExportPaths;

				return doc;
		
	}

	AppSaveAllPathstoDatabase(swaggerJSDoc){
		const paths = Object.keys(swaggerJSDoc.paths);
		paths.forEach((p,i) => {
				ApiService.build({path: p}).save();
		})
	}

	async AppGetApiGatewayDocsImport(req, res) {
	    const { getDocsForApiGateway } = this;
	    try {
	        const endpoint = (!req.body.backend_endpoint)? process.env.NODE_URL : req.body.backend_endpoint; 
            const swaggerSpec = Object.assign({}, global.swaggerSpec); // clonamos
            if(R.isEmpty(swaggerSpec) || R.isNil(swaggerSpec)){
                res.status(500);
                res.json({ msj: 'No swaggerSpec'});
                return;     
            }
	        const doc = await getDocsForApiGateway(swaggerSpec,endpoint);
	        res.status(200);
	        res.json(doc);
	        return;
	    } catch (err) {
            console.log(err);
	        res.status(500);
	        res.json({ msj: 'Unkown error'});
	    }
	}
}
const paths_docs_api_gateway = (url_backend) => {

  const docsApi = {
	    "/api-docs": {
	      "get": {

	        "summary": "Da el json del swaggerspec",
	        "description": "Da el json del swaggerspec",
	        "operationId": "docs_api",
	        "consumes": [
	          "application/json"
	        ],
	        "x-amazon-apigateway-integration": {
	          "uri": `${url_backend}/docs-api`,
	          "httpMethod": "GET",
	          "type": "http_proxy",
	          "passthroughBehavior": "when_no_match"
	        }
	      }
	    }
  }

  const docsProxy = {
	    "/docs/{proxy+}": {
	      "x-amazon-apigateway-any-method": {
	        "parameters": [{
	          "name": "proxy",
	          "in": "path",
	          "required": true,
	          "type": "string"
	        }],
	        "responses": {},
	        "x-amazon-apigateway-integration": {
	          "uri": `${url_backend}/docs/{proxy}`,
	          "passthroughBehavior": "when_no_match",
	          "httpMethod": "ANY",
	          "type": "http"
	        }
	      }
	    }
  }

  const docs = {
	    "/docs": {
	      "get": {

	        "summary": "static html",
	        "description": "static html",
	        "operationId": "static_content_html",
	        "consumes": [
	          "application/json"
	        ],
	        "produces": [
	          "text/html"
	        ],
	        "responses": {
	          "200": {
	            "description": "Successful operation",
	            "headers": {
	              "Content-Type": {
	                "type": "string",
	                "description": "Media type of request"
	              }
	            }
	          }
	        },
	        "x-amazon-apigateway-integration": {
	          "responses": {
	            "default": {
	              "statusCode": "200",
	              "responseParameters": {
	                "method.response.header.Content-Type": "'text/html'"
	              },
	              "responseTemplates": {
	                "text/html": "<html><body>Your HTML doc</body></html>"
	              }
	            }
	          },
	          "uri": `${url_backend}/docs/`,
	          "httpMethod": "GET",
	          "type": "http_proxy",
	          "passthroughBehavior": "when_no_match"
	        }
	      }

	    }
  }

  return  Object.assign({},docsApi,docs)//docsProxy quitamos el proxy xq son llamadas inncesarias al apigateway

};




const init = () => {
	return new DocsController();
};


module.exports = DocsController;