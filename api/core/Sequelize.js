const Sequelize = require('sequelize');
const namespace =  require('cls-hooked').createNamespace('db-sequelize-cliente');
const Config = require('../../config.js');
const ConfigDataBaseMaster = Config.DatabaseMaster;
let array_connections = [];// pool de conexioes

const clsBluebird = require('cls-bluebird'); // esto es para que se comporta el contexto con las promesas de bluebird

clsBluebird(namespace);
Sequelize.useCLS(namespace);

const sequelizeMaster = new Sequelize(ConfigDataBaseMaster.database, ConfigDataBaseMaster.username, ConfigDataBaseMaster.password, {
  dialect: 'mysql',
  logging: false,
  dialectOptions: {decimalNumbers: true},
  timezone: Config.System.timezone_sequelize,
  port: ConfigDataBaseMaster.port,
  pool: {
      min: 1,
      max: 1000,
      idle: 420,
      acquire: 4500, //  The maximum time, in milliseconds, that pool will try to get connection before throwing error,
      evict: 410, // The time interval, in milliseconds, for evicting stale connections. Set it to 0 to disable this feature.
    },
  replication: {
    write: {
      host: ConfigDataBaseMaster.replicas.write.host,
    },
    read: [{
      host: ConfigDataBaseMaster.replicas.read.host,
    }],
  },
});

const init = (config) => {
  const ConfigDB = (!config) ? {nombre: '', usuario: '', password: '', puerto: '', host_escritura: '', host_lectura: '', montos_enteros: false} : config;
  let sequelize = new Sequelize(ConfigDB.nombre, ConfigDB.usuario, ConfigDB.password, {
    dialect: 'mysql',
    logging: true,
    dialectOptions: {decimalNumbers: true},
    timezone: Config.System.timezone_sequelize,
    port: ConfigDB.puerto,
    pool: {
      min: 1,
      max: 1000,
      idle: 420,
      acquire: 8000, //  The maximum time, in milliseconds, that pool will try to get connection before throwing error,
      evict: 410, // The time interval, in milliseconds, for evicting stale connections. Set it to 0 to disable this feature.
    },
    replication: {
      write: {
        host: ConfigDB.host_escritura,
      },
      read: [{
        host: ConfigDB.host_lectura,
      }],
    },
    define: {
      hooks: {
        beforeCreate: (instancia, options) => {
          return roundToInstance(instancia, options, config.montos_enteros);
        },
        beforeUpdate: (instancia, options) => {
          return roundToInstance(instancia, options, config.montos_enteros);
        },
        beforeUpsert: (instancia, options) => {
          return roundToInstance(instancia, options, config.montos_enteros);
        },
        beforeSave: (instancia, options) => {
          return roundToInstance(instancia, options, config.montos_enteros);
        },
      },
    },
  });
  console.log(`<==================> DB CONNECTED ${ConfigDB.nombre} DB CONNECTED  <====================>`);
  return sequelize;
};

const createNewConnection = (configuracion_db) => {
  let connection_seq = checkPool(configuracion_db);
  if (!connection_seq) {
    let new_connection_seq = init(configuracion_db);
    array_connections = array_connections.concat(new_connection_seq);
    return new_connection_seq;
  } else {
    return connection_seq;
  }
};

const checkPool = (configuracion_db) => {
  if (!configuracion_db) {
    return false;
  }
  let result = array_connections.find((connection, i) => {
    if (!connection) {
      return false;
    }

    if (!connection.config) {
     return false;
   }

    let {nombre, usuario, puerto, host_escritura, host_lectura, montos_enteros} = configuracion_db;

    let contrasenna = configuracion_db.password;

    let {database, password, username, port, replication, montos_enteros: integer} = connection.config;

    let host_write = replication.write.host;
    let host_read = (Array.isArray(replication.read)) ? replication.read[0].host : replication.read.host;

    /*
        if ((
            nombre == database &&
            contrasenna == password &&
            usuario == username &&
            puerto == port &&
            host_write == host_escritura &&
            host_read == host_lectura) && integer != montos_enteros ) { //
             array_connections.splice(i, 1); // esto es para eliminar la instancia que no tenga montos_enteros y se meta la nueva y no quede duplicada la conexion
        }
        */

    if (nombre == database &&
      contrasenna == password &&
      usuario == username &&
      puerto == port &&
      host_write == host_escritura &&
      host_read == host_lectura) {
      return true;
    }

    return false;
  });
  return result;
};

const roundToInstance = (instancia, options, isMontosEnteros = false) => { // solo pasa a integer las columnas que sean floats y se salta todas las demas como ids , foreigns key etc, created_at updated_at
  try {
    if (!isMontosEnteros) {
      return true;
    } else {
      options.fields.forEach((element, i) => {
        if (/^id$|^id_\w+$/.test(element)) {
          return true;
        }
        if (/^fecha$|^fecha_\w+$/.test(element)) {
          return true;
        }
        if (element == 'created_at' || element == 'updated_at' || element == 'vencimiento' || element == 'fecha') {
          return true;
        }
        if (typeof instancia[element] === 'number') {
          instancia[element] = Math.round(instancia[element]);
          return true;
        }
        return true;
      });
      return true;
    }
  } catch (error) {
    console.log(`roundToInstance ==> ${error}`);
    return true;
  }
};

module.exports.master = sequelizeMaster;
module.exports.init = createNewConnection; // = (ConfigDatabase) => init(ConfigDatabase)
module.exports.initInstance = createNewConnection; // = (ConfigDatabase) => init(ConfigDatabase)
