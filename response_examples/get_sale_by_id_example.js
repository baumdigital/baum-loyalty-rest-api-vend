{
    "includes": null,
    "data": {
        "id": "6b37644f-a276-8826-11e9-5186e17a3570",
        "outlet_id": "0af7b240-ab1a-11e9-eddc-3a6497652b02",
        "register_id": "0af7b240-ab1a-11e9-eddc-3a64a2f512b8",
        "user_id": "0af7b240-ab1a-11e9-eddc-3a65124eee95",
        "customer_id": "0af7b240-ab1a-11e9-eddc-3f6bd2902465",
        "invoice_number": "19",
        "source": "USER",
        "source_id": null,
        "status": "CLOSED",
        "note": "",
        "short_code": "3qgxyg",
        "return_for": null,
        "total_price": 4800,
        "total_tax": 0,
        "total_loyalty": 0,
        "created_at": "2019-03-28T18:28:21+00:00",
        "updated_at": "2019-03-28T18:28:21+00:00",
        "sale_date": "2019-03-28T18:28:14+00:00",
        "deleted_at": null,
        "line_items": [
            {
                "id": "6b37644f-a276-bd4d-11e9-51872a602de8",
                "product_id": "dc85058a-a61a-11e4-ef46-ff870e26fe4e",
                "tax_id": "a0369f1f-9055-11e4-f68e-d1df2e6738d4",
                "discount_total": 1200,
                "discount": 300,
                "price_total": 4800,
                "price": 1200,
                "cost_total": 0,
                "cost": 0,
                "tax_total": 0,
                "tax": 0,
                "quantity": 4,
                "loyalty_value": 0,
                "note": null,
                "price_set": false,
                "status": "CONFIRMED",
                "sequence": 0,
                "gift_card_number": null,
                "tax_components": [
                    {
                        "rate_id": "2e67ce4e-d1df-11e4-968e-a0369f1f9055",
                        "total_tax": 0
                    }
                ],
                "promotions": [
                    {
                        "amount": -300,
                        "name": "20% DESCUENTO MINI DONAS",
                        "promotion_id": "1110680317053632512",
                        "promo_code": "APP1",
                        "promo_code_id": "1110680317053632513"
                    }
                ],
                "total_tax": 0,
                "unit_cost": 0,
                "unit_discount": 300,
                "unit_loyalty_value": 0,
                "unit_price": 1200,
                "unit_tax": 0,
                "total_cost": 0,
                "total_discount": 1200,
                "total_loyalty_value": 0,
                "total_price": 4800,
                "is_return": false
            }
        ],
        "payments": [
            {
                "id": "6b37644f-a276-bd4d-11e9-51873f91711e",
                "register_id": "0af7b240-ab1a-11e9-eddc-3a64a2f512b8",
                "outlet_id": "0af7b240-ab1a-11e9-eddc-3a6497652b02",
                "retailer_payment_type_id": "a0369f1f-9055-11e4-f68e-d1df2e8447f8",
                "payment_type_id": "1",
                "name": "Efectivo",
                "amount": 4800,
                "payment_date": "2019-03-28T18:28:14+00:00",
                "deleted_at": null,
                "external_attributes": [],
                "source_id": null
            }
        ],
        "adjustments": [],
        "version": 10140470878,
        "receipt_number": "19",
        "total_price_incl": 4800,
        "taxes": [
            {
                "amount": 0,
                "id": "2e67ce4e-d1df-11e4-968e-a0369f1f9055"
            }
        ]
    }
}