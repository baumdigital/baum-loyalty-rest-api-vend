{
    body:
    {
        domain_prefix: 'baumdev',
        environment: 'prod',
        payload:
        {
            "created_at": "2019-01-22 18:43:52",
            "customer":
            {
                "balance": "0.00000",
                "company_name": "",
                "contact_first_name": "Daniel",
                "contact_last_name": "asdasda",
                "created_at": "2019-01-22 18:20:02",
                "custom_field_1": "",
                "custom_field_2": "",
                "custom_field_3": "",
                "custom_field_4": "",
                "customer_code": "Daniel-9293",
                "customer_group_id": "0af7b240-abc5-11e9-eddc-19a1098cb389",
                "date_of_birth": null,
                "deleted_at": null,
                "do_not_email": true,
                "email": null,
                "enable_loyalty": true,
                "fax": null,
                "first_name": "Daniel",
                "id": "0af7b240-abf7-11e9-eddc-1e7255b615dc",
                "last_name": "asdasda",
                "loyalty_balance": "58.66667",
                "mobile": null,
                "note": "",
                "phone": null,
                "points": 0,
                "sex": null,
                "updated_at": "2019-01-22 18:43:53",
                "year_to_date": "106.50000"
            },
            "customer_id": "0af7b240-abf7-11e9-eddc-1e7255b615dc",
            "deleted_at": null,
            "id": "db1f0e39-ba37-8642-11e9-1e754dcadb40",
            "invoice_number": "4",
            "note": "",
            "register_id": "0af7b240-abc5-11e9-eddc-19a10996ab83",
            "register_sale_payments": [
            {
                "amount": 1.86,
                "id": "db1f0e39-ba37-8642-11e9-1e756c942c75",
                "payment_date": "2019-01-22T18:42:09Z",
                "payment_type":
                {
                    "has_native_support": false,
                    "id": "106",
                    "name": "Vend Loyalty Internal Payments"
                },
                "payment_type_id": 106,
                "retailer_payment_type":
                {
                    "config": null,
                    "id": "0af7b240-abf7-11e9-eddc-1e741a401190",
                    "name": "Loyalty",
                    "payment_type_id": "106"
                },
                "retailer_payment_type_id": "0af7b240-abf7-11e9-eddc-1e741a401190"
            },
            {
                "amount": 2.64,
                "id": "db1f0e39-ba37-8642-11e9-1e75703c4ce6",
                "payment_date": "2019-01-22T18:42:15Z",
                "payment_type":
                {
                    "has_native_support": false,
                    "id": "1",
                    "name": "Cash"
                },
                "payment_type_id": 1,
                "retailer_payment_type":
                {
                    "config": null,
                    "id": "0af7b240-abc5-11e9-eddc-19a1099232ef",
                    "name": "Cash",
                    "payment_type_id": "1"
                },
                "retailer_payment_type_id": "0af7b240-abc5-11e9-eddc-19a1099232ef"
            }],
            "register_sale_products": [
            {
                "discount": "0.00000",
                "id": "db1f0e39-ba37-8642-11e9-1e75554698f2",
                "is_return": false,
                "loyalty_value": "100.00000",
                "note": null,
                "price": "4.50000",
                "price_set": false,
                "price_total": "4.50000",
                "product_id": "0af7b240-abf7-11e9-eddc-19a109de883b",
                "quantity": 1,
                "tax": "0.00000",
                "tax_id": "0af7b240-abc5-11e9-eddc-19a1098f57ce",
                "tax_total": "0.00000"
            }],
            "return_for": null,
            "sale_date": "2019-01-22T18:42:15Z",
            "short_code": "3tsjzc",
            "source": "USER",
            "source_id": null,
            "status": "CLOSED",
            "taxes": [
            {
                "id": "098fd0a7-19a1-11e9-addc-0af7b240abc5",
                "name": "No Tax",
                "rate": "0.00000",
                "tax": 0
            }],
            "totals":
            {
                "total_loyalty": "100.00000",
                "total_payment": "4.50000",
                "total_price": "4.50000",
                "total_tax": "0.00000",
                "total_to_pay": "0.00000"
            },
            "updated_at": "2019-01-22T18:43:52+00:00",
            "user":
            {
                "created_at": "2019-01-16 15:11:45",
                "display_name": "Alfredo Rojas",
                "email": "alfredo@baumdigital.com",
                "id": "0af7b240-abc5-11e9-eddc-19a10997155d",
                "name": "alfredo@baumdigital.com",
                "target_daily": null,
                "target_monthly": null,
                "target_weekly": null,
                "updated_at": "2019-01-16 15:15:00"
            },
            "user_id": "0af7b240-abc5-11e9-eddc-19a10997155d",
            "version": 9469204663
        },
        retailer_id: '0af7b240-abc5-11e9-eddc-19a1098a6c9f',
        type: 'sale.update'
    },
    headers:
    {
        host: 'baumprojects.com',
        'user-agent': 'Vend/2.0',
        connection: 'close',
        'content-length': '4157',
        'content-type': 'application/x-www-form-urlencoded',
        'x-vend-webhook-id': '0af7b240-abf7-11e9-eddc-1e733a356162',
        'x-vend-webhook-source': 'hook',
        'accept-encoding': 'gzip'
    }
}