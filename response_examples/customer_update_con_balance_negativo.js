{
    body:
    {
        environment: 'prod',
        retailer_id: '0af7b240-abc5-11e9-eddc-19a1098a6c9f',
        domain_prefix: 'baumdev',
        type: 'customer.update',
        payload:
        {
            "id": "0af7b240-abf7-11e9-eddc-1e7255b615dc",
            "retailer_id": "0af7b240-abc5-11e9-eddc-19a1098a6c9f",
            "customer_code": "Daniel-9293",
            "customer_group_id": "0af7b240-abc5-11e9-eddc-19a1098cb389",
            "first_name": "Daniel",
            "last_name": "asdasda",
            "company_name": "",
            "do_not_email": true,
            "email": null,
            "phone": null,
            "mobile": null,
            "fax": null,
            "balance": -11.700000000000003,
            "loyalty_balance": 58.66667,
            "enable_loyalty": true,
            "points": 0,
            "note": "",
            "year_to_date": 118.2,
            "sex": null,
            "date_of_birth": null,
            "custom_field_1": "",
            "custom_field_2": "",
            "custom_field_3": "",
            "custom_field_4": "",
            "updated_at": "2019-01-22 18:56:34",
            "created_at": "2019-01-22 18:20:02",
            "deleted_at": null,
            "contact":
            {
                "first_name": "Daniel",
                "last_name": "asdasda",
                "company_name": "",
                "phone": null,
                "mobile": null,
                "fax": null,
                "email": null,
                "twitter": "",
                "website": null,
                "physical_address1": "HHMN",
                "physical_address2": "",
                "physical_suburb": "Heredia",
                "physical_city": "Heredia",
                "physical_postcode": "1150300",
                "physical_state": "Heredia",
                "physical_country_id": "CR",
                "postal_address1": "",
                "postal_address2": "",
                "postal_suburb": "",
                "postal_city": "",
                "postal_postcode": "",
                "postal_state": "",
                "postal_country_id": "CR"
            },
            "contact_first_name": "Daniel",
            "contact_last_name": "asdasda"
        }
    },
    headers:
    {
        host: 'baumprojects.com',
        'user-agent': 'Vend/2.0',
        connection: 'close',
        'content-length': '1873',
        'content-type': 'application/x-www-form-urlencoded',
        'x-vend-webhook-id': '0af7b240-abf7-11e9-eddc-1e7230b76064',
        'x-vend-webhook-source': 'monoliph',
        'accept-encoding': 'gzip'
    }
}