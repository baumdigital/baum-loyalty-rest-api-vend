{
    body:
    {
        domain_prefix: 'baumdev',
        environment: 'prod',
        payload:
        {
            "created_at": "2019-01-22 18:30:25",
            "customer":
            {
                "balance": "0.00000",
                "company_name": "",
                "contact_first_name": "Daniel",
                "contact_last_name": "asdasda",
                "created_at": "2019-01-22 18:20:02",
                "custom_field_1": "",
                "custom_field_2": "",
                "custom_field_3": "",
                "custom_field_4": "",
                "customer_code": "Daniel-9293",
                "customer_group_id": "0af7b240-abc5-11e9-eddc-19a1098cb389",
                "date_of_birth": null,
                "deleted_at": null,
                "do_not_email": true,
                "email": null,
                "enable_loyalty": true,
                "fax": null,
                "first_name": "Daniel",
                "id": "0af7b240-abf7-11e9-eddc-1e7255b615dc",
                "last_name": "asdasda",
                "loyalty_balance": "0.00000",
                "mobile": null,
                "note": "",
                "phone": null,
                "points": 0,
                "sex": null,
                "updated_at": "2019-01-22 18:30:26",
                "year_to_date": "9.00000"
            },
            "customer_id": "0af7b240-abf7-11e9-eddc-1e7255b615dc",
            "deleted_at": null,
            "id": "db1f0e39-ba37-bbcd-11e9-1e73ab1f2517",
            "invoice_number": "1",
            "note": "",
            "register_id": "0af7b240-abc5-11e9-eddc-19a10996ab83",
            "register_sale_payments": [
            {
                "amount": 9,
                "id": "db1f0e39-ba37-bbcd-11e9-1e73c80a4071",
                "payment_date": "2019-01-22T18:30:23Z",
                "payment_type":
                {
                    "has_native_support": false,
                    "id": "1",
                    "name": "Cash"
                },
                "payment_type_id": 1,
                "retailer_payment_type":
                {
                    "config": null,
                    "id": "0af7b240-abc5-11e9-eddc-19a1099232ef",
                    "name": "Cash",
                    "payment_type_id": "1"
                },
                "retailer_payment_type_id": "0af7b240-abc5-11e9-eddc-19a1099232ef"
            }],
            "register_sale_products": [
            {
                "discount": "0.00000",
                "id": "db1f0e39-ba37-bbcd-11e9-1e73ad7aef69",
                "is_return": false,
                "loyalty_value": "0.00000",
                "note": null,
                "price": "5.50000",
                "price_set": false,
                "price_total": "5.50000",
                "product_id": "0af7b240-abf7-11e9-eddc-19a109c09316",
                "quantity": 1,
                "tax": "0.00000",
                "tax_id": "0af7b240-abc5-11e9-eddc-19a1098f57ce",
                "tax_total": "0.00000"
            },
            {
                "discount": "0.00000",
                "id": "db1f0e39-ba37-bbcd-11e9-1e73b0399a3c",
                "is_return": false,
                "loyalty_value": "0.00000",
                "note": null,
                "price": "3.50000",
                "price_set": false,
                "price_total": "3.50000",
                "product_id": "0af7b240-abf7-11e9-eddc-19a10a4ab060",
                "quantity": 1,
                "tax": "0.00000",
                "tax_id": "0af7b240-abc5-11e9-eddc-19a1098f57ce",
                "tax_total": "0.00000"
            }],
            "return_for": null,
            "sale_date": "2019-01-22T18:30:23Z",
            "short_code": "lzxqse",
            "source": "USER",
            "source_id": null,
            "status": "CLOSED",
            "taxes": [
            {
                "id": "098fd0a7-19a1-11e9-addc-0af7b240abc5",
                "name": "No Tax",
                "rate": "0.00000",
                "tax": 0
            }],
            "totals":
            {
                "total_loyalty": "0.00000",
                "total_payment": "9.00000",
                "total_price": "9.00000",
                "total_tax": "0.00000",
                "total_to_pay": "0.00000"
            },
            "updated_at": "2019-01-22T18:30:25+00:00",
            "user":
            {
                "created_at": "2019-01-16 15:11:45",
                "display_name": "Alfredo Rojas",
                "email": "alfredo@baumdigital.com",
                "id": "0af7b240-abc5-11e9-eddc-19a10997155d",
                "name": "alfredo@baumdigital.com",
                "target_daily": null,
                "target_monthly": null,
                "target_weekly": null,
                "updated_at": "2019-01-16 15:15:00"
            },
            "user_id": "0af7b240-abc5-11e9-eddc-19a10997155d",
            "version": 9469113850
        },
        retailer_id: '0af7b240-abc5-11e9-eddc-19a1098a6c9f',
        type: 'sale.update'
    }
}