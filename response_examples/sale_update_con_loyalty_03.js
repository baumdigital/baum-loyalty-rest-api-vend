{
    body:
    {
        domain_prefix: 'baumdev',
        environment: 'prod',
        payload:
        {
            "created_at": "2019-01-22 18:56:34",
            "customer":
            {
                "balance": "-11.70000",
                "company_name": "",
                "contact_first_name": "Daniel",
                "contact_last_name": "asdasda",
                "created_at": "2019-01-22 18:20:02",
                "custom_field_1": "",
                "custom_field_2": "",
                "custom_field_3": "",
                "custom_field_4": "",
                "customer_code": "Daniel-9293",
                "customer_group_id": "0af7b240-abc5-11e9-eddc-19a1098cb389",
                "date_of_birth": null,
                "deleted_at": null,
                "do_not_email": true,
                "email": null,
                "enable_loyalty": true,
                "fax": null,
                "first_name": "Daniel",
                "id": "0af7b240-abf7-11e9-eddc-1e7255b615dc",
                "last_name": "asdasda",
                "loyalty_balance": "58.66667",
                "mobile": null,
                "note": "",
                "phone": null,
                "points": 0,
                "sex": null,
                "updated_at": "2019-01-22 18:56:34",
                "year_to_date": "118.20000"
            },
            "customer_id": "0af7b240-abf7-11e9-eddc-1e7255b615dc",
            "deleted_at": null,
            "id": "db1f0e39-ba37-bd33-11e9-1e7684852e2a",
            "invoice_number": "5",
            "note": "",
            "register_id": "0af7b240-abc5-11e9-eddc-19a10996ab83",
            "register_sale_payments": [],
            "register_sale_products": [
            {
                "discount": "0.45000",
                "id": "db1f0e39-ba37-afae-11e9-1e7720157cef",
                "is_return": false,
                "loyalty_value": "100.00000",
                "note": null,
                "price": "4.05000",
                "price_set": false,
                "price_total": "4.05000",
                "product_id": "0af7b240-abf7-11e9-eddc-19a109de883b",
                "quantity": 1,
                "tax": "0.00000",
                "tax_id": "0af7b240-abc5-11e9-eddc-19a1098f57ce",
                "tax_total": "0.00000"
            },
            {
                "discount": "0.30000",
                "id": "db1f0e39-ba37-afae-11e9-1e7721646b23",
                "is_return": false,
                "loyalty_value": "0.05400",
                "note": null,
                "price": "2.70000",
                "price_set": false,
                "price_total": "2.70000",
                "product_id": "0af7b240-abf7-11e9-eddc-19a10a0ae674",
                "quantity": 1,
                "tax": "0.00000",
                "tax_id": "0af7b240-abc5-11e9-eddc-19a1098f57ce",
                "tax_total": "0.00000"
            },
            {
                "discount": "0.55000",
                "id": "db1f0e39-ba37-bd33-11e9-1e768884e47c",
                "is_return": false,
                "loyalty_value": "0.09900",
                "note": null,
                "price": "4.95000",
                "price_set": false,
                "price_total": "4.95000",
                "product_id": "0af7b240-abf7-11e9-eddc-19a109c09316",
                "quantity": 1,
                "tax": "0.00000",
                "tax_id": "0af7b240-abc5-11e9-eddc-19a1098f57ce",
                "tax_total": "0.00000"
            }],
            "return_for": null,
            "sale_date": "2019-01-22T18:56:07Z",
            "short_code": "cibshd",
            "source": "USER",
            "source_id": null,
            "status": "LAYBY",
            "taxes": [
            {
                "id": "098fd0a7-19a1-11e9-addc-0af7b240abc5",
                "name": "No Tax",
                "rate": "0.00000",
                "tax": 0
            }],
            "totals":
            {
                "total_loyalty": "100.15300",
                "total_payment": "0.00000",
                "total_price": "11.70000",
                "total_tax": "0.00000",
                "total_to_pay": "11.70000"
            },
            "updated_at": "2019-01-22T18:56:34+00:00",
            "user":
            {
                "created_at": "2019-01-16 15:11:45",
                "display_name": "Alfredo Rojas",
                "email": "alfredo@baumdigital.com",
                "id": "0af7b240-abc5-11e9-eddc-19a10997155d",
                "name": "alfredo@baumdigital.com",
                "target_daily": null,
                "target_monthly": null,
                "target_weekly": null,
                "updated_at": "2019-01-16 15:15:00"
            },
            "user_id": "0af7b240-abc5-11e9-eddc-19a10997155d",
            "version": 9469293039
        },
        retailer_id: '0af7b240-abc5-11e9-eddc-19a1098a6c9f',
        type: 'sale.update'
    },
    headers:
    {
        host: 'baumprojects.com',
        'user-agent': 'Vend/2.0',
        connection: 'close',
        'content-length': '4030',
        'content-type': 'application/x-www-form-urlencoded',
        'x-vend-webhook-id': '0af7b240-abf7-11e9-eddc-1e733a356162',
        'x-vend-webhook-source': 'hook',
        'accept-encoding': 'gzip'
    }
}