{
  body: {
    environment: 'prod',
    retailer_id: '0af7b240-abc5-11e9-eddc-19a1098a6c9f',
    domain_prefix: 'baumdev',
    type: 'customer.update',
    payload: { "id": "0af7b240-abf7-11e9-eddc-1f25ea941203", "retailer_id": "0af7b240-abc5-11e9-eddc-19a1098a6c9f", "customer_code": "daniel-WVB9", "customer_group_id": "0af7b240-abc5-11e9-eddc-19a1098cb389", "first_name": "daniel", "last_name": "montealegre", "company_name": null, "do_not_email": true, "email": "daniel@mailinator.com", "phone": null, "mobile": null, "fax": null, "balance": 0, "loyalty_balance": 0.64546, "enable_loyalty": true, "points": 0, "note": null, "year_to_date": 174, "sex": null, "date_of_birth": null, "custom_field_1": null, "custom_field_2": null, "custom_field_3": null, "custom_field_4": null, "updated_at": "2019-01-25 17:42:22", "created_at": "2019-01-23 15:45:32", "deleted_at": null, "contact": { "first_name": "daniel", "last_name": "montealegre", "company_name": null, "phone": null, "mobile": null, "fax": null, "email": "daniel@mailinator.com", "twitter": null, "website": null, "physical_address1": null, "physical_address2": null, "physical_suburb": null, "physical_city": null, "physical_postcode": null, "physical_state": null, "physical_country_id": null, "postal_address1": null, "postal_address2": null, "postal_suburb": null, "postal_city": null, "postal_postcode": null, "postal_state": null, "postal_country_id": null }, "contact_first_name": "daniel", "contact_last_name": "montealegre" }
  },
  headers: {
    host: 'baumprojects.com',
    'user-agent': 'Vend/2.0',
    connection: 'close',
    'content-length': '1834',
    'content-type': 'application/x-www-form-urlencoded',
    'x-signature': 'signature=95ad4e30d870d82e42d3eba75ce6d2e9e965d242819017fff20be91237f897a7, algorithm=HMAC-SHA256',
    'x-vend-webhook-id': '0af7b240-abf7-11e9-eddc-20c7be8d76e7',
    'x-vend-webhook-source': 'monoliph',
    'accept-encoding': 'gzip'
  }
}
