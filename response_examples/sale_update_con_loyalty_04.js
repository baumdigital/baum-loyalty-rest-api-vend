{
    body:
    {
        domain_prefix: 'baumdev',
        environment: 'prod',
        payload:
        {
            "created_at": "2019-01-23 17:56:59",
            "customer":
            {
                "balance": "0.00000",
                "company_name": "",
                "contact_first_name": "Daniel",
                "contact_last_name": "asdasda",
                "created_at": "2019-01-22 18:20:02",
                "custom_field_1": "",
                "custom_field_2": "",
                "custom_field_3": "",
                "custom_field_4": "",
                "customer_code": "Daniel-9293",
                "customer_group_id": "0af7b240-abc5-11e9-eddc-19a1098cb389",
                "date_of_birth": null,
                "deleted_at": null,
                "do_not_email": true,
                "email": "daniel@mailinator.com",
                "enable_loyalty": true,
                "fax": null,
                "first_name": "Daniel",
                "id": "0af7b240-abf7-11e9-eddc-1e7255b615dc",
                "last_name": "asdasda",
                "loyalty_balance": "251.92967",
                "mobile": null,
                "note": "",
                "phone": null,
                "points": 0,
                "sex": "M",
                "updated_at": "2019-01-23 17:56:59",
                "year_to_date": "135.20000"
            },
            "customer_id": "0af7b240-abf7-11e9-eddc-1e7255b615dc",
            "deleted_at": null,
            "id": "db1f0e39-ba37-958b-11e9-1f235d04598f",
            "invoice_number": "9",
            "note": "",
            "register_id": "0af7b240-abc5-11e9-eddc-19a10996ab83",
            "register_sale_payments": [
            {
                "amount": 3.5,
                "id": "db1f0e39-ba37-b0ae-11e9-1f38d221e970",
                "payment_date": "2019-01-23T18:00:51Z",
                "payment_type":
                {
                    "has_native_support": false,
                    "id": "106",
                    "name": "Vend Loyalty Internal Payments"
                },
                "payment_type_id": 106,
                "retailer_payment_type":
                {
                    "config": null,
                    "id": "0af7b240-abf7-11e9-eddc-1e741a401190",
                    "name": "Loyalty",
                    "payment_type_id": "106"
                },
                "retailer_payment_type_id": "0af7b240-abf7-11e9-eddc-1e741a401190"
            }],
            "register_sale_products": [
            {
                "discount": "0.00000",
                "id": "db1f0e39-ba37-a7d0-11e9-1f2868b10b61",
                "is_return": false,
                "loyalty_value": "0.07000",
                "note": null,
                "price": "3.50000",
                "price_set": false,
                "price_total": "3.50000",
                "product_id": "0af7b240-abf7-11e9-eddc-19a10aa59494",
                "quantity": 1,
                "tax": "0.00000",
                "tax_id": "0af7b240-abc5-11e9-eddc-19a1098f57ce",
                "tax_total": "0.00000"
            }],
            "return_for": null,
            "sale_date": "2019-01-23T18:00:51Z",
            "short_code": "mpd6jk",
            "source": "USER",
            "source_id": null,
            "status": "CLOSED",
            "taxes": [
            {
                "id": "098fd0a7-19a1-11e9-addc-0af7b240abc5",
                "name": "No Tax",
                "rate": "0.00000",
                "tax": 0
            }],
            "totals":
            {
                "total_loyalty": "0.07000",
                "total_payment": "3.50000",
                "total_price": "3.50000",
                "total_tax": "0.00000",
                "total_to_pay": "0.00000"
            },
            "updated_at": "2019-01-23T17:56:59+00:00",
            "user":
            {
                "created_at": "2019-01-16 15:11:45",
                "display_name": "Alfredo Rojas",
                "email": "alfredo@baumdigital.com",
                "id": "0af7b240-abc5-11e9-eddc-19a10997155d",
                "name": "alfredo@baumdigital.com",
                "target_daily": null,
                "target_monthly": null,
                "target_weekly": null,
                "updated_at": "2019-01-16 15:15:00"
            },
            "user_id": "0af7b240-abc5-11e9-eddc-19a10997155d",
            "version": 9478869891
        },
        retailer_id: '0af7b240-abc5-11e9-eddc-19a1098a6c9f',
        type: 'sale.update'
    },
    headers:
    {
        host: 'baumprojects.com',
        'user-agent': 'Vend/2.0',
        connection: 'close',
        'content-length': '3654',
        'content-type': 'application/x-www-form-urlencoded',
        'x-vend-webhook-id': '0af7b240-abf7-11e9-eddc-1e733a356162',
        'x-vend-webhook-source': 'hook',
        'accept-encoding': 'gzip'
    }
}