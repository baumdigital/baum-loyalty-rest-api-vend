{
  body: {
    domain_prefix: 'baumdev',
    environment: 'prod',
    payload: {
      "created_at": "2019-01-23 20:33:25",
      "customer": {
        "balance": "-10.38000",
        "company_name": null,
        "contact_first_name": "daniel",
        "contact_last_name": "montealegre",
        "created_at": "2019-01-23 15:45:32",
        "custom_field_1": null,
        "custom_field_2": null,
        "custom_field_3": null,
        "custom_field_4": null,
        "customer_code": "daniel-WVB9",
        "customer_group_id": "0af7b240-abc5-11e9-eddc-19a1098cb389",
        "date_of_birth": null,
        "deleted_at": null,
        "do_not_email": true,
        "email": "daniel@mailinator.com",
        "enable_loyalty": true,
        "fax": null,
        "first_name": "daniel",
        "id": "0af7b240-abf7-11e9-eddc-1f25ea941203",
        "last_name": "montealegre",
        "loyalty_balance": "-0.00214",
        "mobile": null,
        "note": null,
        "phone": null,
        "points": 0,
        "sex": null,
        "updated_at": "2019-01-23 20:33:25",
        "year_to_date": "152.00000"
      },
      "customer_id": "0af7b240-abf7-11e9-eddc-1f25ea941203",
      "deleted_at": null,
      "id": "db1f0e39-ba37-a3aa-11e9-1f4e9ad40a60",
      "invoice_number": "13",
      "note": "",
      "register_id": "0af7b240-abc5-11e9-eddc-19a10996ab83",
      "register_sale_payments": [{
        "amount": 0.62,
        "id": "db1f0e39-ba37-a3aa-11e9-1f4ea69f7328",
        "payment_date": "2019-01-23T20:37:07Z",
        "payment_type": {
          "has_native_support": false,
          "id": "106",
          "name": "Vend Loyalty Internal Payments"
        },
        "payment_type_id": 106,
        "retailer_payment_type": {
          "config": null,
          "id": "0af7b240-abf7-11e9-eddc-1e741a401190",
          "name": "Loyalty",
          "payment_type_id": "106"
        },
        "retailer_payment_type_id": "0af7b240-abf7-11e9-eddc-1e741a401190"
      }],
      "register_sale_products": [{
          "discount": "0.00000",
          "id": "db1f0e39-ba37-a3aa-11e9-1f4ea14e84b2",
          "is_return": false,
          "loyalty_value": "0.11000",
          "note": null,
          "price": "5.50000",
          "price_set": false,
          "price_total": "5.50000",
          "product_id": "0af7b240-abf7-11e9-eddc-19a109c09316",
          "quantity": 1,
          "tax": "0.00000",
          "tax_id": "0af7b240-abc5-11e9-eddc-19a1098f57ce",
          "tax_total": "0.00000"
        },
        {
          "discount": "0.00000",
          "id": "db1f0e39-ba37-a3aa-11e9-1f4ea261f085",
          "is_return": false,
          "loyalty_value": "0.11000",
          "note": null,
          "price": "5.50000",
          "price_set": false,
          "price_total": "5.50000",
          "product_id": "0af7b240-abf7-11e9-eddc-19a109c09316",
          "quantity": 1,
          "tax": "0.00000",
          "tax_id": "0af7b240-abc5-11e9-eddc-19a1098f57ce",
          "tax_total": "0.00000"
        }
      ],
      "return_for": null,
      "sale_date": "2019-01-23T20:37:09Z",
      "short_code": "lvddws",
      "source": "USER",
      "source_id": null,
      "status": "LAYBY",
      "taxes": [{
        "id": "098fd0a7-19a1-11e9-addc-0af7b240abc5",
        "name": "No Tax",
        "rate": "0.00000",
        "tax": 0
      }],
      "totals": {
        "total_loyalty": "0.22000",
        "total_payment": "0.62000",
        "total_price": "11.00000",
        "total_tax": "0.00000",
        "total_to_pay": "10.38000"
      },
      "updated_at": "2019-01-23T20:33:25+00:00",
      "user": {
        "created_at": "2019-01-16 15:11:45",
        "display_name": "Alfredo Rojas",
        "email": "alfredo@baumdigital.com",
        "id": "0af7b240-abc5-11e9-eddc-19a10997155d",
        "name": "alfredo@baumdigital.com",
        "target_daily": null,
        "target_monthly": null,
        "target_weekly": null,
        "updated_at": "2019-01-16 15:15:00"
      },
      "user_id": "0af7b240-abc5-11e9-eddc-19a10997155d",
      "version": 9480178654
    },
    retailer_id: '0af7b240-abc5-11e9-eddc-19a1098a6c9f',
    type: 'sale.update'
  },
  headers: {
    host: 'baumprojects.com',
    'user-agent': 'Vend/2.0',
    connection: 'close',
    'content-length': '4129',
    'content-type': 'application/x-www-form-urlencoded',
    'x-vend-webhook-id': '0af7b240-abf7-11e9-eddc-1e733a356162',
    'x-vend-webhook-source': 'hook',
    'accept-encoding': 'gzip'
  }
}
