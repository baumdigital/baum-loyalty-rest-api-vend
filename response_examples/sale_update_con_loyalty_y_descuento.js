{
    "created_at": "2019-03-28 18:28:21",
    "customer":
    {
        "balance": "0.00000",
        "company_name": null,
        "contact_first_name": "Danielnn",
        "contact_last_name": "Montealegre",
        "created_at": "2019-03-05 17:26:33",
        "custom_field_1": "",
        "custom_field_2": "nacional",
        "custom_field_3": null,
        "custom_field_4": null,
        "customer_code": "JDWJ4M",
        "customer_group_id": "0af7b240-ab1a-11e9-eddc-3f51e4603ce6",
        "date_of_birth": null,
        "deleted_at": null,
        "do_not_email": false,
        "email": "daniel-test@mailinator.com",
        "enable_loyalty": true,
        "fax": null,
        "first_name": "Danielnn",
        "id": "0af7b240-ab1a-11e9-eddc-3f6bd2902465",
        "last_name": "Montealegre",
        "loyalty_balance": "184.33000",
        "mobile": "",
        "note": null,
        "phone": "",
        "points": 0,
        "sex": null,
        "updated_at": "2019-03-28 18:28:22",
        "year_to_date": "19050.00000"
    },
    "customer_id": "0af7b240-ab1a-11e9-eddc-3f6bd2902465",
    "deleted_at": null,
    "id": "6b37644f-a276-8826-11e9-5186e17a3570",
    "invoice_number": "19",
    "note": "",
    "register_id": "0af7b240-ab1a-11e9-eddc-3a64a2f512b8",
    "register_sale_payments": [
    {
        "amount": 4800,
        "id": "6b37644f-a276-bd4d-11e9-51873f91711e",
        "payment_date": "2019-03-28T18:28:14Z",
        "payment_type":
        {
            "has_native_support": false,
            "id": "1",
            "name": "Cash"
        },
        "payment_type_id": 1,
        "retailer_payment_type":
        {
            "config": null,
            "id": "a0369f1f-9055-11e4-f68e-d1df2e8447f8",
            "name": "Efectivo",
            "payment_type_id": "1"
        },
        "retailer_payment_type_id": "a0369f1f-9055-11e4-f68e-d1df2e8447f8"
    }],
    "register_sale_products": [
    {
        "discount": "300.00000",
        "id": "6b37644f-a276-bd4d-11e9-51872a602de8",
        "is_return": false,
        "loyalty_value": "0.00000",
        "note": null,
        "price": "1200.00000",
        "price_set": false,
        "price_total": "4800.00000",
        "product_id": "dc85058a-a61a-11e4-ef46-ff870e26fe4e",
        "quantity": 4,
        "tax": "0.00000",
        "tax_id": "a0369f1f-9055-11e4-f68e-d1df2e6738d4",
        "tax_total": "0.00000"
    }],
    "return_for": null,
    "sale_date": "2019-03-28T18:28:14Z",
    "short_code": "3qgxyg",
    "source": "USER",
    "source_id": null,
    "status": "CLOSED",
    "taxes": [
    {
        "id": "2e67ce4e-d1df-11e4-968e-a0369f1f9055",
        "name": "No Tax",
        "rate": "0.00000",
        "tax": 0
    }],
    "totals":
    {
        "total_loyalty": "0.00000",
        "total_payment": "4800.00000",
        "total_price": "4800.00000",
        "total_tax": "0.00000",
        "total_to_pay": "0.00000"
    },
    "updated_at": "2019-03-28T18:28:21+00:00",
    "user":
    {
        "created_at": "2019-02-27 07:55:38",
        "display_name": "APP-cajero",
        "email": null,
        "id": "0af7b240-ab1a-11e9-eddc-3a65124eee95",
        "name": "APP-cajero",
        "target_daily": null,
        "target_monthly": null,
        "target_weekly": null,
        "updated_at": "2019-02-27 07:55:38"
    },
    "user_id": "0af7b240-ab1a-11e9-eddc-3a65124eee95",
    "version": 10140470878
}