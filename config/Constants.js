/**
 *Configuración de las constantes del sistema
 */

module.exports = {
  port: process.env.port, //
  url: process.env.NODE_URL,
  root: __dirname.replace(/\\/g, '/').replace('config', ''),
  url_admin: process.env.ADMIN_URL,
  url_images: process.env.IMAGES_URL,
  URL_API_LEALTO: process.env.URL_API_LEALTO,
  CRONJOBS: {
    syncCustomers: process.env.cj_sync_customers ? process.env.cj_sync_customers :  '00 00 * * *',
    syncTransactions: process.env.cj_sync_transaccions ? process.env.cj_sync_customers : '00 00 * * *',
    syncPendientes: process.env.cj_sync_pendientes? process.env.cj_sync_pendientes : '* * * * *',

  },
  LEALTO_SERVICES: {
    loginMaster: {
      method: 'POST',
      url: '/clientes/login-admin',
    },
    acumular: {
      method: 'POST',
      url: '/plan-lealtad/acumulacion-especifica',
    },
    canjear: {
      method: 'POST',
      url: '/plan-lealtad/cashback-redimir',
    },
    createUser: {
      method: 'POST',
      url: '/usuarios',
    },
    updateUser: {
      method: 'PUT',
      url: '/usuarios',
    },
    exportDoc: {
      method: 'POST',
      url: '/export-docs-apigateway',
    },
  },
  tipoRegalia: {
    bievenida: 'regalia-bienvenida',
    codigo: 'regalia-codigo',
    invitacion: 'regalia-invitacion',
    encuesta: 'regalia-encuesta',
  },
  tipoWebhookDescripcion: {
    usuario: {
      creado: 'user.created',
      actualizado: 'user.updated',
      sincronizado: 'user.synchronized',
      login: 'user.login',
      loginFacebook: 'user.loginFacebook',
      actulizacionNotificaciones: 'user.updateNotificaciones',
      codigoUsuarioActualizado: 'user.codeUpdated',
    },
    acumulacion: {
      regalia: 'acum.regalia',
      normal: 'acum.normal',
      especifica: 'acum.especifica',
      manual: 'acum.manual',
    },
  },
  estadoPreRegistro: {
    deshabilitado: 0,
    activo: 1,
    pendiente: 2,
    asociado: 3,
  },
  tipoDePlan: {
    sellos: 1,
    cashback: 2,
    premios: 3,
    sellosNiveles: 4,
    cashbackNiveles: 5,
    premiosNiveles: 6,
  },
  API_SERVICES: {
    vend: { // esta key es el nombre del primer route del service
      customerUpdate: '/customer-update',
      saleUpdate: '/sale-update',
      oauthAuthorize: '/oauth-authorize',
      authorizationLink: '/link-authorize',
      createWebhook: '/webhook',
      updateWebhook: '/webhook',
      deleteWebhook: '/webhook',
      listWebhook: '/webhook',
      synchronizeCustomers: '/synchronize-customers',
      synchronizeCustomersState: '/synchronize-customers-state',
      synchronizeSales: '/synchronize-sales',
      synchronizeSalesState: '/synchronize-sales-state',

    },
    lealto: {
      customerCreateOrUpdate: '/customer-update-or-create',
      saleCreateOrUpdate: '/sale-update-or-create',
      exportSwaggerConfig: '/export-swagger-api'
    },
  },
  VEND_WEBHOOK_TYPES: {
    customerUpdate: 'customer.update',
    saleUpdate: 'sale.update',
  },
  PAYMENTS_IDS: {
    cash: '1',
    creditCard: '',
    loyalty: '106',
    giftCard: '118,',
    storeCredit: '121',
  },
  PAYMENTS_STATUS: {
    ONACCOUNT: 'ONACCOUNT',
    LAYBY: 'LAYBY',
    ONACCOUNT_CLOSED: 'ONACCOUNT_CLOSED', // acumula loyalty
    LAYBY_CLOSED: 'LAYBY_CLOSED', // acumula loyalty
    CLOSED: 'CLOSED', // acumula loyalty,
    VOIDED: 'VOIDED',
    SAVED: 'SAVED',
  },
  tipoTransaccion: {
    Acreditacion: 1,
    AcreditacionManual: 2,
    Redencion: 3,
    Vencimiento: 4,
    AcreditacionBonus: 5,
    AcreditacionCupon: 6,
    Donacion: 7,
    AcreditacionEncuesta: 8,
    RedencionRegalia: 9,

    Acreditacion_id_label_titulo: 1,
    AcreditacionManual_id_label_titulo: 2,
    Redencion_id_label_titulo: 3,
    Vencimiento_id_label_titulo: 4,
    AcreditacionBonus_id_label_titulo: 5,
    AcreditacionCupon_id_label_titulo: 6,
    RedencionRegalia_id_label_titulo: 22,
    Donacion_id_label_titulo: 7,
    AcreditacionEncuesta_id_label_titulo: 20,

    Acreditacion_id_label_descripcion: 8,
    AcreditacionManual_id_label_descripcion: 9,
    Redencion_id_label_descripcion: 10,
    Vencimiento_id_label_descripcion: 11,
    AcreditacionBonus_id_label_descripcion: 12,
    AcreditacionCupon_id_label_descripcion: 13,
    RedencionRegalia_id_label_descripcion: 23,
    Donacion_id_label_descripcion: 14,
    AcreditacionEncuesta_id_label_descripcion: 21,
  },
  tipoTransaccionObj: {
    Acreditacion: {
      tipo: 1,
      id_label_titulo: 1,
      id_label_descripcion: 8,
    },
    AcreditacionManual: {
      tipo: 2,
      id_label_titulo: 2,
      id_label_descripcion: 9,

    },
    Redencion: {
      tipo: 3,
      id_label_titulo: 3,
      id_label_descripcion: 10,

    },
    Vencimiento: {
      tipo: 4,
      id_label_titulo: 4,
      id_label_descripcion: 11,

    },
    AcreditacionBonus: {
      tipo: 5,
      id_label_titulo: 5,
      id_label_descripcion: 12,

    },
    AcreditacionCupon: {
      tipo: 6,
      id_label_titulo: 6,
      id_label_descripcion: 13,

    },
    Donacion: {
      tipo: 7,
      id_label_titulo: 7,
      id_label_descripcion: 14,

    },
    AcreditacionEncuesta: {
      tipo: 8,
      id_label_titulo: 8,
      id_label_descripcion: 15,
    },
  },
};
