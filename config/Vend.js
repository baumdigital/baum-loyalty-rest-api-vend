
module.exports = {
	vendClientSecret: process.env.VEND_CLIENT_SECRET,
	vendClientId: process.env.VEND_CLIENT_ID,
	vendRedirectUri: process.env.VEND_REDIRECT_URI
}