
//Retry policy


const config = {
	//Retry policy
	//const RETRY_DELAY = ( process.env.NODE_ENV == 'production')? 30000 : 5000; // ms
	retryPolicy:{
		max_retries_send: (process.env.NODE_ENV == 'production') ? 6 : 3, // cantidad de intentos al request
		max_intentos_accion_cola: 5 // /Maximo de intentos para rearlizar una accion
	},
	tipoWebhooks:{
    	usuario: 'USUARIOS_WEBHOOKS',
    	acumular: 'ACUMULAR_WEBHOOKS',
    	canjear:'CANJEAR_WEBHOOKS',
    	cupones:'CUPONES_WEBHOOKS',
    	solicitudes:'SOLICITUDES_WEBHOOKS',		
	},
	tipoAccionesCola:{
		envioPrimeraVez: 'send-first-time',
		reIntentarEnvio: 'send-retry'
	},
	tablaConfiguracionesWebhooks:{
		id: 1
	},
	estadoAccionesCola:{
	    done: "DONE",
    	pending: "PENDING",
    	executing: "EXECUTING",
    	error: "ERROR",
    	errorMax: 'ERROR-MAX-RETRIES'	
	},
	tipoReEnvios:{
		interno: 'interno',
		cliente: 'cliente'	
	},
	tipoWebhookDescripcion:{
      usuario:{
        creado: 'user.created',
        actualizado: 'user.updated',
        sincronizado: 'user.synchronized',
        login: 'user.login',
        loginFacebook: 'user.loginFacebook',
        actulizacionNotificaciones: 'user.updateNotificaciones',
        codigoUsuarioActualizado: 'user.codeUpdated',
      },
      acumulacion:{
        regalia: 'acum.regalia',
        normal: 'acum.normal',
        especifica: 'acum.especifica',
        manual: 'acum.manual'
      }
  }
}




module.exports = config;






