/**
 *Configuración general del sistema
 */
module.exports = {
    timezone_js: 'GMT-0600',
    timezone_sequelize: '-06:00',
    debug: false
};
