

/*
 * Files
 */
const path = require('path');

const env_path = path.resolve(__dirname, '.env');
require('dotenv').config({ path: env_path });

//config
const Config = require('./config.js');
const Constants = Config.Constants;
const API_SERVICES = Constants.API_SERVICES;
/**
 * Express
 */
const express = require('express');
const app = express();
/**
 * Paquetes de terceros
 */
const compression = require('compression');
const logger = require('morgan');
const bodyParser = require('body-parser');


app.get('/',(req,res) => {
  res.status(200);
  res.json({ valid: true });
  return;
})


/**
 * Logger de middleware
 */
app.use(logger('short'));


const validUrls = [
  `/vend${API_SERVICES.vend.customerUpdate}`,
  `/vend${API_SERVICES.vend.saleUpdate}`,
  `/lealto${API_SERVICES.lealto.customerCreateOrUpdate}`,
  `/lealto${API_SERVICES.lealto.saleCreateOrUpdate}`,
  //`/vend${API_SERVICES.oauthAuthorize}`,
]

/**
 * Limite de tamaño de upload
 * @type {String}
 */
app.use(bodyParser.json({
  limit: '10000mb',
  verify: function(req, res, buf) {
    const orignalUlr = req.originalUrl;
    if (validUrls.some((url, i) => url == orignalUlr)) {
      req.rawBody = buf
    }
  }
}));



/**
 * Límite de tamaño del request
 * @type {String}
 */
app.use(bodyParser.urlencoded({
  limit: '10000mb',
  extended: true,
  verify: function(req, res, buf) {
    const orignalUlr = req.originalUrl;
    if (validUrls.some((url, i) => url == orignalUlr)) {
      req.rawBody = buf
    }
  }
}));

app.use(compression());

/**
 * Swagger documentación del RESTful Api
 */
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerDefinition = require(path.join(__dirname, 'api/swagger/swagger.js'));
/**
 * Configuración de Swagger
 * @type {Object}
 */
const options = {
    // import swaggerDefinitions
    swaggerDefinition: swaggerDefinition,
    // path to the API docs
    apis: [path.join(__dirname, 'api/routes/*.js')],
};
/**
 * Documentación con Swagger
 *
 */
const swaggerSpec = swaggerJSDoc(options);
global.swaggerSpec = swaggerSpec; // siempre sacar un clon de este objeto cuando se vaya a manipular

/**
 * Documento de configuración de Swagger
 */

app.get('/json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
});
/**
 * HTML de documentación
 */
app.use('/docs', express.static(path.join(__dirname, 'docs')));


const vendRoute = require('./api/routes/Vend.route.js');
const lealtoRoute = require('./api/routes/Lealto.route.js');

//Route vend
app.use('/vend', vendRoute.App);
app.use('/lealto', lealtoRoute.App);


/**
 * Error 404 si no encuentra ningún servicio
 */
app.use((req, res) => {
  res.status(404);
  res.send('The resource URL: "' + req.url + '" AND Method: "' + req.method + '" does no exist...');
});

const port = (process.env.port)? process.env.port: 8081 ;

app.listen(port, function() {
    console.log('App running on port ' + port);    
});
