//Carga la configuración del sitio
const Config = {
    Constants: require('./config/Constants.js'),
    System: require('./config/System.js'),
    Vend: require('./config/Vend.js'),
    DatabaseMaster: require('./config/Database.js'),
    ConfigWebhooks: require('./config/Webhooks.js')
};
module.exports = Config;
